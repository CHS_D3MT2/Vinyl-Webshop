-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: dd6708
-- Erstellungszeit: 11. Apr 2018 um 00:15
-- Server-Version: 5.7.21-nmm1-log
-- PHP-Version: 7.0.25-nmm1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `d02985cf`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(255) NOT NULL,
  `plz` varchar(10) NOT NULL,
  `street` varchar(255) NOT NULL,
  `housenumber` varchar(10) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `is_main_shipping` tinyint(1) DEFAULT '0',
  `is_main_payment` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `company`, `first_name`, `last_name`, `country`, `city`, `plz`, `street`, `housenumber`, `phone`, `is_main_shipping`, `is_main_payment`, `created_at`) VALUES
(10, 1, 'asdasd', 'asdasd', 'asd', 'asd', 'asda', 'asd', 'asdasd', 'asd', 'asdasda', 1, 0, '2018-04-04 22:36:05'),
(11, 1, 'aasd', 'asdasd', 'asdsd', 'asd', 'asd', 'asda', 'asd', 'asdasd', 'asdasda', 0, 0, '2018-04-04 23:50:46'),
(17, 5, 'sadasdfsfb', 'Artur', 'Vazigin', 'Deutschland', 'Karlsruhe', '76131', 'Schützenstraße', '42', '1794137397', 1, 1, '2018-04-09 22:47:57'),
(18, 1, 'Daimler AG', 'Eva', 'Ngo', 'Deutschland', 'Stuttgart', '70178', 'Hasenbergstraße', '20', '017630143788', 1, 0, '2018-04-09 23:44:10');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `auth`
--

CREATE TABLE `auth` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `auth`
--

INSERT INTO `auth` (`user_id`, `email`, `password`, `role`) VALUES
(1, 'pascal.buerkle@web.de', '$2y$10$Yi8ro3geWXcnKAfCvWWe8.rXeYZa0i73asCu2ERcK0.3swKnApszy', 'admin'),
(5, 'artur.vazigin@gmail.com', '$2y$10$iEBA78QFRGVdlh8m1h13I.U5Xz04nmDBJTap91WFYX24G1d59rxPG', 'admin'),
(6, 'andyjet90@googlemail.com', '$2y$10$okhldZx2cctUOxiqBxciCe6/wvNum8PGGAoT.ziGQoy5wQdTGiLme', 'admin'),
(7, 'eva.ngo@web.de', '$2y$10$SY.xAG7/NmVLoEmzC8gCM.rHj8qu244xTckjac0FB/e1/D0q7ZIT.', 'admin'),
(8, 'eva.ngo19@gmail.com', '$2y$10$s9tKU75vaQ8HXC3WEMHMFe3dsD3xswetXS21MT5dro1cuI/iXIdje', 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bank_transfer`
--

CREATE TABLE `bank_transfer` (
  `bank_transfer_id` int(11) NOT NULL,
  `institute` varchar(50) NOT NULL,
  `iban` varchar(33) NOT NULL,
  `bic` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `bank_transfer`
--

INSERT INTO `bank_transfer` (`bank_transfer_id`, `institute`, `iban`, `bic`) VALUES
(1, '', '1231412319238012', '123123123'),
(3, '', '123141231923801212', '123123123'),
(4, '', '567657567567', '567567567'),
(5, '', 'DE8209389103', ''),
(6, '', 'DE273904ß73', ''),
(7, '', '213475683457658967', '32154325123');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `credit_card`
--

CREATE TABLE `credit_card` (
  `credit_card_id` int(11) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `card_number` varchar(255) NOT NULL,
  `security_code` varchar(255) NOT NULL,
  `expiration_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `credit_card`
--

INSERT INTO `credit_card` (`credit_card_id`, `provider`, `card_number`, `security_code`, `expiration_date`) VALUES
(1, 'Mastercard', '12312312312', '123', '0000-00-00'),
(2, 'Visa', '12312312312', '123', '2019-06-01'),
(3, 'jasd', '123123123', '123', '2021-12-01'),
(4, 'Sparkasse Pforzheim Calw', '387290473057630747', '937', '1970-01-01'),
(5, 'visa', '2312313123123', '123', '2018-05-01'),
(6, 'Mastercard', '2456567653', '345', '2023-05-01'),
(7, 'Mastercard', '328494534', '432', '2024-03-01'),
(8, 'Mastercard', '535586XXXXXX3265', '456', '2021-11-01'),
(9, 'Mastercard', '1239083908402', '493', '1970-01-01');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

CREATE TABLE `orders` (
  `order_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `payment_address_id` int(11) NOT NULL,
  `shipping_address_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `shipping_code` varchar(50) DEFAULT 'dhl_0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `is_paid`, `payment_address_id`, `shipping_address_id`, `payment_id`, `total_price`, `shipping_code`, `created_at`) VALUES
('1867321630924553641', 1, 1, 10, 18, 6, NULL, 'hermes_0', '2018-04-10 00:00:59'),
('4465756355360239713', 1, 1, 10, 18, 6, '303.00', 'hermes_0', '2018-04-10 00:01:58'),
('5518318283130835062', 1, 1, 10, 18, 2, '328.00', 'dhl_0', '2018-04-10 17:12:53'),
('7057058869593262632', 1, 1, 11, 11, 2, '169.00', 'dhl_0', '2018-04-08 01:27:18'),
('7668301379204730294', 1, 1, 10, 10, 8, '42.00', 'dhl_0', '2018-04-09 11:38:24'),
('8580012896575771211', 1, 1, 10, 18, 6, NULL, 'hermes_0', '2018-04-10 00:01:40');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `order_content`
--

CREATE TABLE `order_content` (
  `content_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `amount` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `order_content`
--

INSERT INTO `order_content` (`content_id`, `order_id`, `product_id`, `price`, `amount`) VALUES
(39, '7057058869593262632', 5, '0.00', 15),
(40, '7057058869593262632', 3, '0.00', 1),
(41, '7057058869593262632', 4, '0.00', 1),
(42, '7057058869593262632', 9, '0.00', 1),
(43, '7668301379204730294', 10, '0.00', 2),
(44, '1867321630924553641', 7, '0.00', 1),
(45, '8580012896575771211', 7, '0.00', 1),
(46, '4465756355360239713', 7, '0.00', 1),
(47, '4465756355360239713', 21, '0.00', 15),
(48, '5518318283130835062', 5, '3.00', 16),
(49, '5518318283130835062', 3, '70.00', 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `bank_transfer_id` int(11) DEFAULT NULL,
  `credit_card_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `payment`
--

INSERT INTO `payment` (`payment_id`, `bank_transfer_id`, `credit_card_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 3, NULL, 1, '2018-04-05', NULL),
(6, NULL, 4, 1, '2018-04-06', NULL),
(8, 4, NULL, 1, '2018-04-08', NULL),
(9, 5, NULL, 8, '2018-04-08', NULL),
(10, NULL, 6, 8, '2018-04-08', NULL),
(11, 6, NULL, 7, '2018-04-08', NULL),
(12, NULL, 7, 7, '2018-04-08', NULL),
(13, NULL, 8, 5, '2018-04-09', NULL),
(14, 7, NULL, 6, '2018-04-09', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `rating` int(11) DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `view_amount` int(11) DEFAULT '0',
  `vote_amount` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `artist_id` int(11) NOT NULL,
  `release_date` date NOT NULL,
  `amount_sold` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product`
--

INSERT INTO `product` (`product_id`, `category_id`, `name`, `description`, `slug`, `rating`, `price`, `stock`, `discount`, `view_amount`, `vote_amount`, `created_at`, `updated_at`, `visible`, `artist_id`, `release_date`, `amount_sold`) VALUES
(3, 6, 'No Mercy', '', 'gunz-for-hire-no-mercy', 3, '1.30', 50, '0.00', 186, 3, '2018-02-27 14:51:33', '2018-04-10 21:45:59', 1, 0, '2016-11-23', 35),
(4, 6, 'Armed & Dangerous', '', 'gunz-for-hire-armed-and-dangerous', 0, '1.30', 999, '0.00', 14, 0, '2018-02-27 15:16:21', '2018-04-10 21:47:51', 1, 0, '2017-10-16', 3),
(5, 23, 'Dance Machine', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata', 'dance-machine', 3, '3.00', 9984, '5.00', 142, 4, '2018-03-05 10:54:38', '2018-04-10 22:41:23', 1, 0, '0000-00-00', 38),
(6, 6, 'We Are Nexus', 'bla', 'sub-sonik-we-are-nexus', 0, '2.56', 5, '0.00', 0, 0, '2018-03-05 18:05:57', '2018-04-10 23:53:07', 1, 15, '0000-00-00', 1),
(7, 15, 'Euphoria', 'Bla', 'wasted-penguinz-euphoria', 0, '3.00', 6, '0.00', 5, 0, '2018-03-05 18:08:27', '2018-04-11 00:02:44', 1, 16, '0000-00-00', 4),
(8, 14, 'Euphoria', 'Beschreibuuung', 'code-black-euphoria', 0, '5.00', 124, '0.00', 4, 0, '2018-03-05 18:11:04', '2018-04-06 18:51:21', 1, 17, '0000-00-00', 0),
(9, 6, 'Eclipse', 'feat. Nolz, Jonjo, Phuture Noize', 'b-freqz-eclipse', 0, '13.79', 100, '0.00', 5, 0, '2018-03-05 18:11:58', '2018-04-10 20:52:49', 1, 9, '2018-04-09', 2),
(10, 14, 'Nothing (Re-Release)', 'Erscheinungsjahr: 2002\r\n\r\n10 Songs, ~ 58 Min\r\n\r\n1 - Stengah\r\n2 - Rational Gaze\r\n3 - Perpetual Black Second\r\n4 - Closed Eye Visuals\r\n5 - Glints Collide\r\n6 - Organic Shadows\r\n7 - Straws Pulled At Random\r\n8 - Spasm\r\n9 - Nebulous\r\n10 - Obsidian', 'meshuggah-nothing-re-release', 0, '21.00', 50, '0.00', 3, 0, '2018-03-08 23:12:13', '2018-04-10 21:55:24', 1, 26, '0000-00-00', 2),
(12, 6, 'Trackprodukt', 'test', 'trackprodukt', 4, '123.00', 123, '12.00', 52, 1, '2018-03-15 15:09:46', '2018-03-23 23:08:37', 1, 0, '0000-00-00', 0),
(20, 6, 'Tempest', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata', 'tempest', 0, '5.00', 100, '5.00', 4, 0, '2018-03-23 23:15:25', '0000-00-00 00:00:00', 1, 0, '0000-00-00', 0),
(21, 15, 'That\'s so me', 'An unique album about me, myself and I.', 'thats-so-me', 3, '20.00', 4, '5.00', 12, 2, '2018-03-24 22:52:25', '2018-03-25 12:27:52', 1, 1, '0000-00-00', 16),
(22, 6, 'Won’t Hold Me Down (Gravity)', 'feat. Trevor Guthrie', 'brennan-heart-wont-hold-me-down-gravity', 4, '0.99', 100, '0.00', 8, 2, '2018-03-30 13:15:53', '2018-04-10 20:16:50', 1, 5, '0000-00-00', 0),
(23, 20, 'Erase Me', '', 'underoath-erase-me', 4, '18.00', 500, '0.00', 18, 1, '2018-04-09 22:15:34', '0000-00-00 00:00:00', 1, 25, '2018-04-06', 0),
(24, 6, 'EP I', 'feat. Headhunterz, Wildstylez, Sound Rush', 'project-one-ep-i', 0, '5.20', 100, '0.00', 2, 0, '2018-04-10 21:04:55', '2018-04-10 21:41:18', 1, 28, '2017-11-10', 0),
(25, 6, 'Anthem of Freedom (QAPITAL Anthem 2018)', '', 'phuture-noize-anthem-of-freedom-qapital-anthem-2018', 0, '1.56', 20, '0.00', 0, 0, '2018-04-10 21:12:26', '0000-00-00 00:00:00', 1, 12, '2018-03-13', 0),
(26, 20, 'Beloved (Deluxe Edition)', 'Beloved is the third full-length studio album by Australian metalcore band I Killed the Prom Queen. It was released in Australia on 14 February 2014 and in US on 18 February 2014. The album was streamed on Alternative Press\'s website on 5 February 2014.', 'i-killed-the-prom-queen-beloved-deluxe-edition', 0, '18.00', 500, '0.00', 1, 0, '2018-04-10 21:17:57', '0000-00-00 00:00:00', 1, 29, '2014-02-14', 0),
(27, 6, 'No Guts No Glory (Defqon.1 Anthem 2015)', 'feat. Skits Vicious', 'ran-d-no-guts-no-glory-defqon-1-anthem-2015', 0, '2.60', 20, '0.00', 1, 0, '2018-04-10 21:18:59', '2018-04-10 21:44:51', 1, 30, '2015-05-18', 0),
(28, 14, 'Malina', 'Malina is the fifth studio album by Norwegian progressive music band Leprous, released on August 25, 2017, through Inside Out Music. Malina is the first album to feature Robin Ognedal on guitar, and the first not to feature former guitarist Øystein Landsv', 'leprous-malina', 0, '18.00', 500, '0.00', 1, 0, '2018-04-10 21:22:28', '0000-00-00 00:00:00', 1, 31, '2017-08-25', 0),
(29, 21, 'Pleiades\' Dust', 'Pleiades\' Dust is the first EP by the Canadian technical death metal band Gorguts, released on May 13, 2016 through Season of Mist. It consists of a single track which runs for thirty-three minutes and is roughly divided into seven movements. It is also t', 'gorguts-pleiades-dust', 0, '16.00', 500, '0.00', 6, 0, '2018-04-10 21:26:55', '2018-04-10 21:36:50', 1, 32, '2016-05-13', 0),
(32, 22, 'A Fever You Can\'t Sweat Out', 'A Fever You Can\'t Sweat Out is the debut studio album by American rock band Panic! at the Disco. Produced by Matt Squire, the album was released on September 27, 2005, on Decaydance and Fueled by Ramen. The group formed in Las Vegas in 2004 and began post', 'panic-at-the-disco-a-fever-you-cant-sweat-out', 0, '15.00', 500, '0.00', 1, 0, '2018-04-10 21:35:02', '0000-00-00 00:00:00', 1, 33, '2005-09-27', 0),
(33, 23, 'Gemini', 'Gemini was released as a free download on 18 December 2015, which featured collaborations with artists Dillon Francis, Tunji Ige and KLP. The EP was downloaded over 800,000 times, charted globally on iTunes Electronic EP chart and was named to var', 'what-so-not-gemini', 0, '12.00', 500, '0.00', 2, 0, '2018-04-10 21:40:54', '2018-04-10 21:43:22', 1, 34, '2015-12-18', 0),
(34, 24, 'Untrue', 'Untrue is the second studio album by British electronic music producer Burial. Released on 5 November 2007 by Hyperdub, the album was produced by Burial from 2006 to 2007 using the digital audio editing software Sound Forge. Untrue builds on the general a', 'burial-untrue', 0, '21.00', 500, '0.00', 0, 0, '2018-04-10 21:47:01', '2018-04-10 21:48:19', 1, 35, '2007-11-05', 0),
(35, 24, 'Burial', 'Burial is the debut studio album by London electronic producer Burial, released in 2006 on Kode9\'s Hyperdub label. Considered a landmark of the mid-2000s dubstep scene, album\'s sound features a dark, emotive take on the UK rave music that preoccupied Buri', 'burial-burial', 0, '21.00', 500, '0.00', 1, 0, '2018-04-10 21:47:10', '2018-04-10 21:52:43', 1, 35, '2007-11-05', 0),
(36, 14, 'Lateralus', 'Lateralus (/ˌlætəˈræləs/) is the third studio album by American rock band Tool. It was released on May 15, 2001 through Volcano Entertainment. The album was recorded at Cello Studios in Hollywood and The Hook, Big Empty Space, and The Lodge, in North Holl', 'tool-lateralus', 0, '21.00', 500, '0.00', 0, 0, '2018-04-10 22:00:41', '0000-00-00 00:00:00', 1, 36, '2001-05-15', 0),
(37, 21, 'Everything is Fire', 'Everything Is Fire is the second full-length album by New Zealand death metal band Ulcerate. It was released on April 7, 2009 through Willowtip Records to very positive reviews, and has been described by Roadburn as \"genre-defining\". Lyrically, the album ', 'ulcerate-everything-is-fire', 0, '21.00', 500, '0.00', 0, 0, '2018-04-10 22:04:24', '0000-00-00 00:00:00', 1, 37, '2009-04-07', 0),
(38, 23, 'LSD', '-', 'ghastly-lsd', 0, '3.00', 500, '0.00', 0, 0, '2018-04-10 22:06:26', '0000-00-00 00:00:00', 1, 38, '0000-00-00', 0),
(39, 25, '2 2 Karma', '', 'foreign-beggars-2-2-karma', 0, '0.00', 500, '0.00', 0, 0, '2018-04-10 22:12:51', '2018-04-10 22:13:28', 1, 39, '0000-00-00', 0),
(40, 9, 'Nevermind', '', 'nirvana-nevermind', 0, '5.99', 1000, '0.00', 1, 0, '2018-04-10 22:18:48', '0000-00-00 00:00:00', 1, 40, '2011-09-23', 0),
(41, 25, 'Godfather', '', 'wiley-godfather', 0, '18.00', 500, '0.00', 0, 0, '2018-04-10 22:19:57', '0000-00-00 00:00:00', 1, 41, '2017-01-13', 0),
(42, 9, 'American Idiot', '', 'green-day-american-idiot', 0, '5.99', 1000, '0.00', 0, 0, '2018-04-10 22:50:37', '0000-00-00 00:00:00', 1, 42, '2004-09-27', 0),
(43, 9, 'Hybrid Theory', '', 'linkin-park-hybrid-theory', 5, '5.99', 1000, '0.00', 4, 1, '2018-04-10 23:02:59', '0000-00-00 00:00:00', 1, 43, '2001-02-23', 0),
(44, 9, 'Meteora', '', 'linkin-park-meteora', 0, '16.30', 1000, '0.00', 1, 0, '2018-04-10 23:09:10', '2018-04-10 23:11:31', 1, 43, '2003-03-24', 0),
(45, 9, 'Californication', '', 'red-hot-chilli-peppers-californication', 0, '6.99', 1000, '0.00', 0, 0, '2018-04-10 23:18:58', '0000-00-00 00:00:00', 1, 44, '1999-06-07', 0),
(46, 20, 'Deadweight', 'Lyrically, Deadweight sees the band exploring both personal to political issues in a way that’s as raw and honest as the music that supports them. “This record is very much a snapshot of the past year; we are all very positive people but in 2016 we went t', 'wage-war-deadweight', 0, '16.00', 500, '0.00', 0, 0, '2018-04-10 23:26:38', '0000-00-00 00:00:00', 1, 46, '2017-08-04', 0),
(47, 24, 'Master of Shadows', 'feat. Virtual Riot, Ganja White Night, T.Rabb, JPhelpz, Excision, Dion Timmer', 'datsik-master-of-shadows', 0, '8.92', 1000, '0.00', 0, 0, '2018-04-10 23:28:54', '0000-00-00 00:00:00', 1, 45, '2018-01-12', 0),
(48, 24, 'LEVELED', '', 'jphelpz-leveled', 0, '0.00', 1000, '0.00', 0, 0, '2018-04-10 23:30:41', '0000-00-00 00:00:00', 1, 48, '2017-02-24', 0),
(49, 20, 'Deathgrip', 'Deathgrip is the third album by the metalcore band Fit for a King.', 'fit-for-a-king-deathgrip', 0, '18.00', 500, '0.00', 0, 0, '2018-04-10 23:31:00', '2018-04-10 23:31:36', 1, 47, '2016-10-07', 0),
(50, 24, 'Alien Theory', 'feat. Bawdy, BARENHVRD', 'extra-terra-alien-theory', 0, '4.86', 1000, '0.00', 1, 0, '2018-04-10 23:33:25', '0000-00-00 00:00:00', 1, 49, '2017-12-08', 0),
(51, 15, 'Fenech-Soler', 'Fenech-Soler is the eponymous debut studio album by British electropop band Fenech-Soler. It was released on 27 September 2010 by B-Unique Records.', 'fenech-soler-fenech-soler', 0, '18.00', 500, '0.00', 0, 0, '2018-04-10 23:34:59', '0000-00-00 00:00:00', 1, 50, '2010-09-27', 0),
(52, 24, 'Feel The Fire', '', 'jarvis-feel-the-fire', 0, '6.48', 1000, '0.00', 0, 0, '2018-04-10 23:35:23', '0000-00-00 00:00:00', 1, 51, '2018-04-06', 0),
(53, 24, 'The Harvest', '', 'rettchit-the-harvest', 0, '4.86', 100, '0.00', 0, 0, '2018-04-10 23:37:40', '0000-00-00 00:00:00', 1, 53, '2018-03-23', 0),
(54, 24, 'Murk Em', '', 'jphelpz-murk-em', 0, '6.24', 100, '0.00', 0, 0, '2018-04-10 23:39:23', '0000-00-00 00:00:00', 1, 48, '2018-02-16', 0),
(55, 15, 'Alligator', 'Alligator is the third studio album by American indie rock band The National, released on April 12, 2005 on Beggars Banquet. Recorded and produced by Peter Katis and Paul Mahajan, the album brought The National critical acclaim and increased their fanbase', 'the-national-alligator', 0, '21.00', 500, '0.00', 0, 0, '2018-04-10 23:39:53', '0000-00-00 00:00:00', 1, 52, '2005-04-12', 0),
(56, 22, 'After Laughter', 'After Laughter is the fifth studio album by American rock band Paramore. It was released on May 12, 2017, through Fueled by Ramen as a follow-up to Paramore, their 2013 self-titled album.', 'paramore-after-laughter', 0, '18.00', 500, '0.00', 0, 0, '2018-04-10 23:45:39', '0000-00-00 00:00:00', 1, 54, '2017-05-12', 0),
(57, 21, 'Nocturnal', '', 'the-black-dahlia-murder-nocturnal', 0, '21.00', 500, '0.00', 0, 0, '2018-04-10 23:52:45', '0000-00-00 00:00:00', 1, 56, '2007-09-18', 0),
(58, 23, 'The Money Store', '', 'death-grips-the-money-store', 0, '18.00', 500, '0.00', 0, 0, '2018-04-10 23:57:29', '0000-00-00 00:00:00', 1, 57, '2012-04-24', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_artist`
--

CREATE TABLE `product_artist` (
  `artist_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_artist`
--

INSERT INTO `product_artist` (`artist_id`, `name`, `first_name`, `last_name`, `slug`) VALUES
(0, 'Gunz for Hire', 'Ran-D', 'Adaro', 'gunz-for-hire'),
(1, 'Schwesta Ewa', 'Eva', 'Ngo', 'schwesta-ewa'),
(2, 'D-Sturb', '', '', NULL),
(3, 'Sub Zero Project', '', '', NULL),
(4, 'D-Block & S-te-Fan', '', '', NULL),
(5, 'Brennan Heart', '', '', 'brennan-heart'),
(6, 'Warface & Killshot', '', '', NULL),
(7, 'Brennan Heart & Toneshifterz', '', '', NULL),
(8, 'Hard Driver', '', '', NULL),
(9, 'B-Freqz', 'B-Front', 'Frequencerz', 'b-freqz'),
(10, 'Digital Punk', '', '', NULL),
(11, 'Frontliner', '', '', NULL),
(12, 'Phuture Noize', '', '', NULL),
(13, 'The Prophet', '', '', 'the-prophet'),
(14, 'Psyko Punkz', '', '', NULL),
(15, 'Sub Sonik', '', '', NULL),
(16, 'Wasted Penguinz', '', '', NULL),
(17, 'Code Black', '', '', NULL),
(18, 'Headhunterz', '', '', NULL),
(22, 'sfsdfsdf', 'sdfsdf', 'sdfsdf', 'sfsdfsdf'),
(23, 'sdfsdfsdf', 'sdfsdfsdf', 'sdfsdfsdf', 'sdfsdfsdf'),
(24, 'asdasd', 'asdasd', 'asdasdad', 'asdasd'),
(25, 'Underoath', '', '', 'underoath'),
(26, 'Meshuggah', '', '', 'meshuggah'),
(27, 'B-Freqz', '', '', 'b-freqz'),
(28, 'Project One', 'Headhunterz', 'Wildstylez', 'project-one'),
(29, 'I Killed the Prom Queen', '', '', 'i-killed-the-prom-queen'),
(30, 'Ran-D', '', '', 'ran-d'),
(31, 'Leprous', '', '', 'leprous'),
(32, 'Gorguts', '', '', 'gorguts'),
(33, 'Panic! at the Disco', '', '', 'panic-at-the-disco'),
(34, 'What So Not', '', '', 'what-so-not'),
(35, 'Burial', '', '', 'burial'),
(36, 'Tool', '', '', 'tool'),
(37, 'Ulcerate', '', '', 'ulcerate'),
(38, 'Ghastly', '', '', 'ghastly'),
(39, 'Foreign Beggars', '', '', 'foreign-beggars'),
(40, 'Nirvana', '', '', 'nirvana'),
(41, 'Wiley', '', '', 'wiley'),
(42, 'Green Day', '', '', 'green-day'),
(43, 'Linkin Park', '', '', 'linkin-park'),
(44, 'Red Hot Chilli Peppers', '', '', 'red-hot-chilli-peppers'),
(45, 'Datsik', '', '', 'datsik'),
(46, 'Wage War', '', '', 'wage-war'),
(47, 'Fit For A King', '', '', 'fit-for-a-king'),
(48, 'JPhelpz', '', '', 'jphelpz'),
(49, 'Extra Terra', '', '', 'extra-terra'),
(50, 'Fenech-Soler', '', '', 'fenech-soler'),
(51, 'Jarvis', '', '', 'jarvis'),
(52, 'The National', '', '', 'the-national'),
(53, 'Rettchit', '', '', 'rettchit'),
(54, 'Paramore', '', '', 'paramore'),
(56, 'The Black Dahlia Murder', '', '', 'the-black-dahlia-murder'),
(57, 'Death Grips', '', '', 'death-grips');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_artist_image`
--

CREATE TABLE `product_artist_image` (
  `image_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_artist_image`
--

INSERT INTO `product_artist_image` (`image_id`, `url`, `artist_id`, `is_active`, `created_at`) VALUES
(1, '7cnNilcuaTauF.jpg', 22, 0, '2018-04-09 21:57:37'),
(2, '04B8YcDb5Qakb.jpg', 23, 0, '2018-04-09 22:00:50'),
(3, 'BijuiNr1gSZp0.jpg', 24, 0, '2018-04-09 22:01:52'),
(4, 'srij1aBGQ0b57.jpg', 26, 0, '2018-04-09 22:35:29'),
(5, 'mpTAusoj4wwzj.jpg', 13, 0, '2018-04-10 12:45:29'),
(6, 'giSA2NlkmHco0.jpg', 27, 0, '2018-04-10 20:48:31'),
(7, 'lLANAZPhvC0O8.jpg', 9, 0, '2018-04-10 20:55:09');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_category`
--

CREATE TABLE `product_category` (
  `category_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_category`
--

INSERT INTO `product_category` (`category_id`, `slug`, `category_name`, `created_at`, `updated_at`, `visible`) VALUES
(6, 'hardstyle', 'Hardstyle', '2018-02-28 11:55:04', '2018-04-03 00:06:40', 1),
(9, 'rock', 'Rock', '2018-02-28 13:32:25', '2018-04-03 00:06:46', 1),
(14, 'progressive-metal', 'Progressive Metal', '2018-03-08 23:08:36', '2018-04-03 00:06:57', 1),
(15, 'indie', 'Indie', '2018-03-24 22:46:12', '2018-04-03 00:06:59', 1),
(16, 'metal', 'Metal', '2018-04-03 21:12:07', NULL, 0),
(17, 'pop', 'Pop', '2018-04-03 21:12:34', NULL, 0),
(18, 'dubstep', 'Dubstep', '2018-04-03 21:12:48', NULL, 0),
(19, 'house', 'House', '2018-04-03 21:13:08', NULL, 0),
(20, 'metalcore', 'Metalcore', '2018-04-09 22:10:00', NULL, 1),
(21, 'death-metal', 'Death Metal', '2018-04-10 21:25:21', NULL, 1),
(22, 'pop-punk', 'Pop-Punk', '2018-04-10 21:31:03', NULL, 1),
(23, 'electronic', 'Electronic', '2018-04-10 21:38:37', NULL, 1),
(24, 'dubstep', 'Dubstep', '2018-04-10 21:43:52', NULL, 1),
(25, 'grime', 'Grime', '2018-04-10 22:08:00', NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_image`
--

CREATE TABLE `product_image` (
  `image_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_image`
--

INSERT INTO `product_image` (`image_id`, `url`, `product_id`, `is_active`, `created_at`) VALUES
(2, '8m2YkI9DVHog1.jpg', 5, 0, '2018-03-23 21:57:43'),
(3, 'IC3Ygfq6ja8Yz.jpg', 6, 0, '2018-03-23 23:07:57'),
(4, 'A9QATgewfCgFx.jpg', 7, 0, '2018-03-23 23:08:09'),
(6, 'K2tfiRG3TkHjI.jpg', 12, 0, '2018-03-23 23:08:37'),
(7, 'NNcZshXsXM2dl.jpg', 8, 0, '2018-03-23 23:08:53'),
(12, 'nj4OZr4HSRHfN.jpg', 20, 0, '2018-03-23 23:15:25'),
(16, 'FIiT0CEuyTUcQ.jpg', 21, 0, '2018-03-25 12:27:52'),
(17, 'FO7EcPSsA0V4D.jpg', 22, 0, '2018-03-30 13:34:12'),
(18, 'BrWDVCRqIBcIS.jpg', 23, 0, '2018-04-09 22:15:34'),
(19, 'yFUjPZX33StvR.jpg', 9, 0, '2018-04-10 20:52:49'),
(20, 'FPWuspmuvv35K.jpg', 24, 0, '2018-04-10 21:04:55'),
(21, '8lKSJ5H6TalIM.jpg', 25, 0, '2018-04-10 21:12:26'),
(22, 'ZOSGGm2T7FXPv.jpg', 26, 0, '2018-04-10 21:17:57'),
(23, 'dzLwXxTHuIJg3.jpg', 27, 0, '2018-04-10 21:18:59'),
(24, 'nOUsmnhkVJQH9.jpg', 28, 0, '2018-04-10 21:22:28'),
(25, 'vlxifYG4HjM4J.jpg', 3, 0, '2018-04-10 21:26:51'),
(26, 'jTueGcAlqI40n.jpg', 29, 0, '2018-04-10 21:29:32'),
(27, 'G9ZGddFTdf1LQ.jpg', 4, 0, '2018-04-10 21:31:13'),
(28, 'VjpLABJgoNR7B.jpg', 32, 0, '2018-04-10 21:35:02'),
(29, 'xU3hrMZNUnwjy.jpg', 33, 0, '2018-04-10 21:40:54'),
(30, 'wM94ok0ArDiQM.jpg', 34, 0, '2018-04-10 21:47:01'),
(32, '0FSC5DdRUsfmM.jpg', 35, 0, '2018-04-10 21:49:56'),
(33, 'C6gdYn6HqQihx.jpg', 10, 0, '2018-04-10 21:55:24'),
(34, 'JYUV9WEbSIVTk.jpg', 36, 0, '2018-04-10 22:00:41'),
(35, '0ZAvOAmRonOpq.jpg', 37, 0, '2018-04-10 22:04:24'),
(36, 'Ls2FN7RXypGbD.jpg', 38, 0, '2018-04-10 22:06:26'),
(37, 'yCBziAhbQiWR7.jpg', 39, 0, '2018-04-10 22:12:51'),
(38, 'zET5keZBUpcbt.jpg', 40, 0, '2018-04-10 22:18:48'),
(39, '0ZhKq1XCsgraY.jpg', 41, 0, '2018-04-10 22:19:57'),
(40, 'zJ3n4oe3kRY57.jpg', 42, 0, '2018-04-10 22:50:37'),
(41, '7KkecOv3mXM2W.jpg', 43, 0, '2018-04-10 23:02:59'),
(43, 'QDxvAOwhG5jHl.jpg', 44, 0, '2018-04-10 23:11:31'),
(44, '7UITbBwnW1aU9.jpg', 45, 0, '2018-04-10 23:18:58'),
(45, 'clHrakENRlRJB.jpg', 46, 0, '2018-04-10 23:26:38'),
(46, 'A3qYAmHuHRLeY.jpg', 47, 0, '2018-04-10 23:28:54'),
(47, 'EzHh5aFeYNNll.jpg', 48, 0, '2018-04-10 23:30:41'),
(48, 'GXH3JUKY1ACe1.jpg', 49, 0, '2018-04-10 23:31:00'),
(49, 'NotqE4rZnB732.jpg', 50, 0, '2018-04-10 23:33:25'),
(50, 'SO2ai4SDqQvBh.jpg', 51, 0, '2018-04-10 23:34:59'),
(51, 'Apg26HhVAw21V.jpg', 52, 0, '2018-04-10 23:35:23'),
(52, 'ebzQL9G8sGWEZ.jpg', 53, 0, '2018-04-10 23:37:40'),
(53, 'mE85Ga0cIz0GK.jpg', 54, 0, '2018-04-10 23:39:23'),
(54, 'wFRUqIKnC2GnG.gif', 55, 0, '2018-04-10 23:39:53'),
(55, 'HS1SVBOJLzrL4.jpg', 56, 0, '2018-04-10 23:45:39'),
(56, 'JMNHZTaxdm7QA.jpg', 57, 0, '2018-04-10 23:52:45'),
(57, 'bXebnhZvRJ9JL.jpg', 58, 0, '2018-04-10 23:57:29');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_review`
--

CREATE TABLE `product_review` (
  `review_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `rating` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_review`
--

INSERT INTO `product_review` (`review_id`, `title`, `content`, `rating`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Ziemlich gut.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 4, 3, 1, '2018-04-09', NULL),
(2, 'Ziemlich gut.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 4, 3, 1, '2018-04-09', NULL),
(4, 'Bullshit.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 1, 3, 1, '2018-04-09', NULL),
(7, 'Testüberschrift', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 4, 12, 1, '2018-04-09', NULL),
(8, 'Top album', 'Hatte viel spaß damit, mittlerweile mindestens 12 mal gehört, Underoath enttäuscht nicht!', 4, 23, 5, '2018-04-09', NULL),
(9, 'Supergeil', 'supergeile Künstlerin, ich hab mich einfach in sie verliebt!', 5, 21, 1, '2018-04-09', NULL),
(10, 'So lala', 'geht so, manchmal bissle nervig die Stimme...', 1, 21, 1, '2018-04-09', NULL),
(11, 'Hammer melodie', 'haha', 5, 22, 6, '2018-04-10', NULL),
(12, 'bla bla', 'asdgf', 3, 22, 6, '2018-04-10', NULL),
(15, 'Bestes LP album ever', 'Dazu muss man wohl nichts mehr sagen...', 5, 43, 5, '2018-04-10', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_track`
--

CREATE TABLE `product_track` (
  `track_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `length` time DEFAULT NULL,
  `youtube_id` varchar(33) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `product_track`
--

INSERT INTO `product_track` (`track_id`, `product_id`, `name`, `length`, `youtube_id`, `created_at`) VALUES
(36, 5, '', '00:00:00', NULL, NULL),
(42, 12, 'track 1', '00:01:23', NULL, NULL),
(43, 12, 'track 2', '00:02:34', NULL, NULL),
(44, 12, 'track 4', '00:01:23', NULL, NULL),
(48, 18, 'testasdasd', '00:01:23', NULL, NULL),
(49, 18, 'neuer track', '00:01:24', NULL, NULL),
(56, 20, 'Ein Traaaaack', '00:00:00', NULL, NULL),
(67, 21, 'That is me', '00:02:00', NULL, NULL),
(68, 21, 'This is me', '00:00:00', NULL, NULL),
(69, 21, 'This is the true me', '00:02:09', NULL, NULL),
(75, 19, 'sdasdasdasd', '00:01:23', NULL, NULL),
(93, 6, '', '00:00:00', NULL, NULL),
(96, 8, 'Track Eins', '00:01:23', NULL, NULL),
(102, 23, 'It Has To Start Somewhere', '00:03:11', '', NULL),
(103, 23, 'Rapture', '00:03:35', '', NULL),
(104, 23, 'On My Teeth', '00:03:11', '', NULL),
(105, 23, 'Wake Me', '00:03:40', '', NULL),
(106, 23, 'Bloodlust', '00:03:33', '', NULL),
(107, 23, 'Sink With You', '00:04:45', '', NULL),
(108, 23, 'ihateit', '00:03:28', '', NULL),
(109, 23, 'Hold Your Breath', '00:03:29', '', NULL),
(110, 23, 'No Frame', '00:03:46', '', NULL),
(111, 23, 'In Motion', '00:03:35', '', NULL),
(112, 23, 'I Gave Up', '00:04:03', '', NULL),
(120, 22, 'Won’t Hold Me Down (Gravity)', '00:03:17', 'https://www.youtube.com/embed/rFc', NULL),
(121, 9, ' 5 Years Ago', '00:01:10', '', NULL),
(122, 26, 'Beginning of the End', '00:01:47', '', NULL),
(123, 26, 'To the Wolves', '00:03:22', '', NULL),
(124, 26, 'Bright Enough', '00:03:49', '', NULL),
(125, 26, 'Melior', '00:03:40', '', NULL),
(126, 26, 'Thirty One & Sevens', '00:03:25', '', NULL),
(127, 26, 'Calvert Street', '00:03:31', '', NULL),
(128, 26, 'Kjælighet', '00:03:47', '', NULL),
(129, 26, 'The Beaten Path', '00:03:27', '', NULL),
(130, 26, 'Billions', '00:03:55', '', NULL),
(131, 26, 'Nightmares', '00:03:02', '', NULL),
(132, 26, 'No One Will Save Us', '00:02:59', '', NULL),
(133, 26, 'Memento Vivere', '00:03:52', '', NULL),
(134, 26, 'Sparrowhawk', '00:03:56', '', NULL),
(135, 26, 'Brevity', '00:05:08', '', NULL),
(136, 28, 'Bonneville', '00:05:29', '', NULL),
(137, 28, 'Stuck', '00:06:49', '', NULL),
(138, 28, 'From the Flame', '00:03:51', '', NULL),
(139, 28, 'Captive', '00:03:44', '', NULL),
(140, 28, 'Illuminate', '00:04:21', '', NULL),
(141, 28, 'Leashes', '00:04:10', '', NULL),
(142, 28, 'Mirage', '00:06:48', '', NULL),
(143, 28, 'Malina', '00:06:15', '', NULL),
(144, 28, 'Coma', '00:03:55', '', NULL),
(145, 28, 'The Weight of Disaster', '00:06:01', '', NULL),
(146, 28, 'The Last Milestone', '00:07:31', '', NULL),
(152, 32, 'Introduction', '00:00:37', '', NULL),
(153, 32, 'The Only Difference Between Martyrdom and Suicide Is Press Coverage', '00:02:54', '', NULL),
(154, 32, 'London Beckoned Songs About Money Written by Machines', '00:03:23', '', NULL),
(155, 32, 'Nails for Breakfast, Tacks for Snacks', '00:03:23', '', NULL),
(156, 32, 'Camisado', '00:03:11', '', NULL),
(157, 32, 'Time to Dance', '00:03:22', '', NULL),
(158, 32, 'Lying Is the Most Fun a Girl Can Have Without Taking Her Clothes Off', '00:03:20', '', NULL),
(159, 32, 'Intermission', '00:02:35', '', NULL),
(160, 32, 'But It\'s Better If You Do', '00:03:25', '', NULL),
(161, 32, 'I Write Sins Not Tragedies', '00:03:06', '', NULL),
(162, 32, 'I Constantly Thank God for Esteban', '00:03:30', '', NULL),
(163, 32, 'There\'s a Good Reason These Tables Are Numbered Honey, You Just Haven\'t Thought of It Yet', '00:03:16', '', NULL),
(164, 32, 'Build God, Then We\'ll Talk', '00:03:40', '', NULL),
(165, 29, 'Pleiades\' Dust', '00:32:59', '', NULL),
(171, 24, 'Luminosity', '00:04:29', 'VMR4LfQFKgM', NULL),
(172, 24, 'One without a Second', '00:05:18', 'EB38c8o7Q_M', NULL),
(173, 24, 'It\'s an edit', '00:06:06', 'avXwFhBT1ws', NULL),
(174, 24, 'Project 1 (Sound Rush Remix)', '00:05:40', 'zkdffoQcSOw', NULL),
(175, 33, 'Intro (feat. Tunji Ige)', '00:00:59', '', NULL),
(176, 33, 'Gemini (feat. George Maple)', '00:04:30', '', NULL),
(177, 33, 'Arrows (feat. Dawn Golden, Dillon Francis)', '00:04:27', '', NULL),
(178, 33, 'Death Drive (feat. KLP)', '00:04:32', '', NULL),
(179, 33, 'Oddity', '00:04:12', '', NULL),
(180, 27, 'No Guts No Glory (Defqon.1 Anthem 2015)', '00:05:58', 'F3nGALpB23Q', NULL),
(181, 3, 'No Mercy', '00:05:20', 'gfFOjnRLw8M', NULL),
(184, 4, 'Armed & Dangerous', '00:06:04', 'fVbSyXwszKM', NULL),
(185, 34, 'Untitled', '00:00:48', '', NULL),
(186, 34, 'Archangel', '00:03:58', '', NULL),
(187, 34, 'Near Dark', '00:03:54', '', NULL),
(202, 35, 'Untitled 1', '00:00:36', '', NULL),
(203, 35, 'Distant Lights', '00:05:27', '', NULL),
(204, 35, 'Spaceape', '00:04:02', '', NULL),
(205, 35, 'Wounder', '00:04:52', '', NULL),
(206, 35, 'Night Bus', '00:02:14', '', NULL),
(207, 35, 'Southern Comfort', '00:05:23', '', NULL),
(208, 35, 'U Hurt Me', '00:05:23', '', NULL),
(209, 35, 'Gutted', '00:04:43', '', NULL),
(210, 35, 'Forgive', '00:03:08', '', NULL),
(211, 35, 'Broken Home', '00:05:05', '', NULL),
(212, 35, 'Prayer', '00:03:45', '', NULL),
(213, 35, 'Pirates', '00:06:11', '', NULL),
(214, 35, 'Untitled 2', '00:00:55', '', NULL),
(215, 10, 'Stengah', '00:05:38', '', NULL),
(216, 10, 'Rational Gaze', '00:05:26', '', NULL),
(217, 10, 'Perpetual Black Second', '00:04:39', '', NULL),
(218, 10, 'Closed Eye Visuals', '00:07:25', '', NULL),
(219, 10, 'Giants Collide', '00:04:56', '', NULL),
(220, 10, 'Organic Shadows', '00:05:20', '', NULL),
(221, 10, 'Straws Pulled at Random', '00:05:16', '', NULL),
(222, 10, 'Spasm', '00:04:14', '', NULL),
(223, 10, 'Nebulous', '00:07:07', '', NULL),
(224, 10, 'Obsidian', '00:08:34', '', NULL),
(225, 36, 'The Grudge', '00:08:36', '', NULL),
(226, 36, 'Eon Blue Apocalypse', '00:01:04', '', NULL),
(227, 36, 'The Patient', '00:07:13', '', NULL),
(228, 36, 'Mantra', '00:01:12', '', NULL),
(229, 36, 'Schism', '00:06:47', '', NULL),
(230, 36, 'Parabol', '00:03:04', '', NULL),
(231, 36, 'Parabola', '00:06:03', '', NULL),
(232, 36, 'Ticks & Leeches', '00:08:10', '', NULL),
(233, 36, 'Lateralus', '00:09:24', '', NULL),
(234, 36, 'Disposition', '00:04:46', '', NULL),
(235, 36, 'Reflection', '00:11:07', '', NULL),
(236, 36, 'Triad', '00:08:46', '', NULL),
(237, 36, 'Faaip de Oiad', '00:02:39', '', NULL),
(238, 37, 'Drown Within', '00:06:42', '', NULL),
(239, 37, 'We Are Nil', '00:05:41', '', NULL),
(240, 37, 'Withered and Obsolete', '00:06:10', '', NULL),
(241, 37, 'Caecus', '00:06:26', '', NULL),
(242, 37, 'Tyranny', '00:05:22', '', NULL),
(243, 37, 'The Earth at its Knees', '00:05:45', '', NULL),
(244, 37, 'Soullessness Embraces', '00:06:36', '', NULL),
(245, 37, 'Everything is Fire', '00:07:52', '', NULL),
(246, 38, 'LSD (Original Mix)', '00:04:20', '', NULL),
(259, 39, 'Flashback (feat. Alix Perez)', '00:03:24', '', NULL),
(260, 39, '6 Million Stories (feat. Kate Tempest, Bangzy, Bionic, Scott Garcia)', '00:03:56', '', NULL),
(261, 39, 'The Mission (feat. Afronaut Zu, Bangzy)', '00:04:46', '', NULL),
(262, 39, 'Toast (feat. Alix Perez, Izzie Gibbs, Dizmack)', '00:03:15', '', NULL),
(263, 39, 'The Sauce (feat. Black Josh, Ivy Lab)', '00:04:05', '', NULL),
(264, 39, 'Crash and Burn (feat. Lord Laville, Bangzy)', '00:03:14', '', NULL),
(265, 39, 'Blood in the Sink (feat. Kojey Radical, Bangzy)', '00:03:19', '', NULL),
(266, 39, 'Bosh (feat. Marcello Spooks, Bangzy)', '00:03:19', '', NULL),
(267, 39, 'Waved (feat. Flux Pavilion, OG Maco, Black Josh)', '00:03:20', '', NULL),
(268, 39, '24-7 (feat. Feed Me)', '00:02:39', '', NULL),
(269, 39, 'Visitations (feat. Hyroglifics, Scorzayzee)', '00:04:17', '', NULL),
(270, 39, 'Vultures (feat. Josh Bevan, Dag Nabbit)', '00:04:42', '', NULL),
(271, 40, 'Smells Like Teen Spirit', '00:05:02', '', NULL),
(272, 40, 'In Bloom', '00:04:15', '', NULL),
(273, 40, 'Come As You Are', '00:03:39', '', NULL),
(274, 40, 'Breed', '00:03:04', '', NULL),
(275, 40, 'Lithium', '00:04:17', '', NULL),
(276, 40, 'Polly', '00:02:54', '', NULL),
(277, 40, 'Territorial Pissings', '00:02:23', '', NULL),
(278, 40, 'Drain You', '00:03:44', '', NULL),
(279, 40, 'Lounge Act', '00:02:36', '', NULL),
(280, 40, 'Stay Away', '00:03:31', '', NULL),
(281, 40, 'On A Plain', '00:03:14', '', NULL),
(282, 40, 'Something In The Way / Endless, Nameless', '00:03:52', '', NULL),
(283, 41, 'Birds n Bars', '00:06:30', '', NULL),
(284, 41, 'Bring Them All / Holy Grime (feat. Devlin)', '00:03:17', '', NULL),
(285, 41, 'Name Brand (feat. Jme, Frisco & J2K)', '00:03:42', '', NULL),
(286, 41, 'Speakerbox', '00:03:12', '', NULL),
(287, 41, 'Back with a Banger', '00:03:03', '', NULL),
(288, 41, 'Joe Bloggs (feat.Newham Generals & President T)', '00:03:27', '', NULL),
(289, 41, 'Pattern Up Properly (feat. Flowdan & Jamakabi)', '00:02:47', '', NULL),
(290, 41, 'Can\'t Go Wrong', '00:03:04', '', NULL),
(291, 41, 'Bang (feat. Ghetts)', '00:02:59', '', NULL),
(292, 41, 'U Were Always, Pt. 2 (feat. Skepta & Belly)', '00:03:36', '', NULL),
(293, 41, 'On This (feat. Chip, Ice Kid & Little D)', '00:03:40', '', NULL),
(294, 41, 'Bait Face (feat. Scratchy)', '00:02:47', '', NULL),
(295, 41, 'My Direction (feat. Lethal Bizzle)', '00:02:46', '', NULL),
(296, 41, 'Like It or Not (feat. Breeze)', '00:03:06', '', NULL),
(297, 41, 'Lucid', '00:03:40', '', NULL),
(298, 41, 'Laptop (feat. Manga)', '00:02:58', '', NULL),
(299, 41, 'P Money (Remix) (feat. P Money) (bonus track)', '00:03:01', '', NULL),
(300, 42, 'American Idiot', '00:02:56', '', NULL),
(301, 42, 'Holiday', '00:03:53', '', NULL),
(302, 42, 'Boulevard Of Broken Dreams', '00:04:21', '', NULL),
(303, 42, 'Are We The Waiting', '00:02:43', '', NULL),
(304, 42, 'St. Jimmy', '00:02:55', '', NULL),
(305, 42, 'Give Me Novacaine', '00:03:26', '', NULL),
(306, 42, 'She\'s A Rebel', '00:02:00', '', NULL),
(307, 42, 'Extraordinary Girl', '00:03:34', '', NULL),
(308, 42, 'Letterbomb', '00:04:06', '', NULL),
(309, 42, 'Wake Me Up When September Ends', '00:04:46', '', NULL),
(310, 42, 'Homecoming', '00:09:19', '', NULL),
(311, 43, 'Papercut', '00:03:04', '', NULL),
(312, 43, 'One Step Closer', '00:02:37', '', NULL),
(313, 43, 'With You', '00:03:23', '', NULL),
(314, 43, 'Points of Authority', '00:03:20', '', NULL),
(315, 43, 'Crawling', '00:03:29', '', NULL),
(316, 43, 'Runaway', '00:03:04', '', NULL),
(317, 43, 'By Myself', '00:03:09', '', NULL),
(318, 43, 'In the End', '00:03:36', '', NULL),
(319, 43, 'A Place for my Head', '00:03:04', '', NULL),
(320, 43, 'Forgotten', '00:03:14', '', NULL),
(321, 43, 'Cure for the Itch', '00:02:37', '', NULL),
(322, 43, 'Pushing me Away', '00:03:11', '', NULL),
(336, 44, 'Foreword', '00:00:13', '', NULL),
(337, 44, 'Don\'t stay', '00:03:08', '', NULL),
(338, 44, 'Somewhere I belong', '00:03:34', '', NULL),
(339, 44, 'Lying From You', '00:02:55', '', NULL),
(340, 44, 'Hit The Floor', '00:02:44', '', NULL),
(341, 44, 'Easier To Run', '00:03:24', '', NULL),
(342, 44, 'Faint', '00:02:43', '', NULL),
(343, 44, 'Figure 09', '00:03:18', '', NULL),
(344, 44, 'Breaking The Habit', '00:03:17', '', NULL),
(345, 44, 'From The Inside', '00:02:54', '', NULL),
(346, 44, 'Nobody\'s listening', '00:02:59', '', NULL),
(347, 44, 'Session', '00:02:24', '', NULL),
(348, 44, 'Numb', '00:03:06', '', NULL),
(349, 45, 'Around the World', '00:03:58', '', NULL),
(350, 45, 'Parallel Universe', '00:04:29', '', NULL),
(351, 45, 'Scar Tissue', '00:03:35', '', NULL),
(352, 45, 'Otherside', '00:04:15', '', NULL),
(353, 45, 'Get On Top', '00:03:18', '', NULL),
(354, 45, 'Californication', '00:05:29', '', NULL),
(355, 45, 'Easily', '00:03:51', '', NULL),
(356, 45, 'Porcelain', '00:02:43', '', NULL),
(357, 45, 'Emit Remmus', '00:04:00', '', NULL),
(358, 45, 'I Like Dirt', '00:02:37', '', NULL),
(359, 45, 'This Velvet Glove', '00:03:45', '', NULL),
(360, 45, 'Savior', '00:04:52', '', NULL),
(361, 45, 'Purple Stain', '00:04:13', '', NULL),
(362, 45, 'Right On Time', '00:01:52', '', NULL),
(363, 45, 'Road Trippin\'', '00:03:25', '', NULL),
(364, 46, 'Two Years', '00:01:15', '', NULL),
(365, 46, 'Southbound', '00:02:57', '', NULL),
(366, 46, 'Don\'t Let Me Fade Away', '00:04:05', '', NULL),
(367, 46, 'Stitch', '00:03:26', '', NULL),
(368, 46, 'Witness', '00:03:54', '', NULL),
(369, 46, 'Deadweight', '00:03:37', '', NULL),
(370, 46, 'Gravity', '00:03:52', '', NULL),
(371, 46, 'Never Enough', '00:03:11', '', NULL),
(372, 46, 'Indestructible', '00:04:13', '', NULL),
(373, 46, 'Disdain', '00:02:04', '', NULL),
(374, 46, 'My Grave Is Mine To Dig', '00:03:27', '', NULL),
(375, 46, 'Johnny Cash', '00:03:52', '', NULL),
(376, 47, 'Pressure Plates', '00:04:35', '', NULL),
(377, 47, 'Warriors of the Night', '00:03:23', '', NULL),
(378, 47, 'Bad Behavior', '00:03:58', '', NULL),
(379, 47, 'Ronin Riddim', '00:03:15', '', NULL),
(380, 47, 'You\'ve Changed', '00:03:21', '', NULL),
(381, 47, 'Freakuency', '00:03:18', '', NULL),
(382, 47, 'Find Me', '00:04:03', '', NULL),
(383, 48, 'LEVELED', '00:03:12', '', NULL),
(395, 49, 'The End\'s Beginning', '00:00:48', '', NULL),
(396, 49, 'Pissed Off', '00:03:31', '', NULL),
(397, 49, 'Dead Memory (feat. Jake Luhrs)', '00:04:13', '', NULL),
(398, 49, 'Cold Room', '00:02:52', '', NULL),
(399, 49, 'Disease', '00:03:32', '', NULL),
(400, 49, 'Shadows & Echoes', '00:03:47', '', NULL),
(401, 49, 'More Than Nameless', '00:03:14', '', NULL),
(402, 49, 'We Are All Lost', '00:04:07', '', NULL),
(403, 49, 'Unclaimed, Unloved', '00:03:01', '', NULL),
(404, 49, 'Stacking Bodies (feat. Levi Benton)', '00:03:14', '', NULL),
(405, 49, 'Deathgrip', '00:04:15', '', NULL),
(406, 50, 'Illuminacity', '00:03:51', '', NULL),
(407, 51, 'Battlefields', '00:04:02', '', NULL),
(408, 51, 'Lies', '00:03:17', '', NULL),
(409, 51, 'Golden Sun', '00:04:15', '', NULL),
(410, 51, 'Stop and Stare', '00:04:06', '', NULL),
(411, 51, 'The Great Unknown', '00:05:35', '', NULL),
(412, 51, 'Demons', '00:03:44', '', NULL),
(413, 51, 'LA Love', '00:04:19', '', NULL),
(414, 51, 'Stonebridge', '00:04:32', '', NULL),
(415, 51, 'Contender', '00:04:48', '', NULL),
(416, 52, 'Feel The Fire', '00:04:28', '', NULL),
(417, 53, 'Harvester', '00:03:21', '', NULL),
(418, 54, 'Murk Em', '00:03:59', '', NULL),
(419, 55, 'Secret Meeting', '00:03:44', '', NULL),
(420, 55, 'Karen', '00:03:59', '', NULL),
(421, 55, 'Lit Up', '00:02:55', '', NULL),
(422, 55, 'Looking for Astronauts', '00:03:23', '', NULL),
(423, 55, 'Daughters of the SoHo Riots', '00:03:59', '', NULL),
(424, 55, 'Baby, We\'ll Be Fine', '00:03:21', '', NULL),
(425, 55, 'Friend of Mine', '00:03:25', '', NULL),
(426, 56, 'Hard Times', '00:03:02', '', NULL),
(427, 56, 'Rose-Colored Boy', '00:03:32', '', NULL),
(428, 56, 'Told You So', '00:03:08', '', NULL),
(429, 56, 'Forgiveness', '00:03:39', '', NULL),
(430, 56, 'Fake Happy', '00:03:55', '', NULL),
(431, 56, '26', '00:03:41', '', NULL),
(432, 56, 'Pool', '00:03:52', '', NULL),
(433, 56, 'Grudges', '00:03:07', '', NULL),
(434, 56, 'Caught in the Middle', '00:03:34', '', NULL),
(435, 56, 'Idle Worship', '00:03:18', '', NULL),
(436, 56, 'No Friend', '00:03:23', '', NULL),
(437, 56, 'Tell Me How', '00:04:20', '', NULL),
(438, 57, 'Everything Went Black', '00:03:17', '', NULL),
(439, 57, 'What A Horrible Night To Have A Curse', '00:03:50', '', NULL),
(440, 57, 'Virally Yours', '00:03:05', '', NULL),
(441, 57, 'I Worship Only What You Bleed', '00:01:59', '', NULL),
(442, 57, 'Nocturnal', '00:03:13', '', NULL),
(443, 57, 'Deathmask Divine', '00:03:37', '', NULL),
(444, 57, 'Of Darkness Spawned', '00:03:22', '', NULL),
(445, 57, 'Climatic Degradation', '00:02:39', '', NULL),
(446, 57, 'To A Breathless Oblivion', '00:04:57', '', NULL),
(447, 57, 'Warborn', '00:04:40', '', NULL),
(448, 58, 'Get Got', '00:02:52', '', NULL),
(449, 58, 'The Fever (Aye Aye)', '00:03:07', '', NULL),
(450, 58, 'Lost Boys', '00:03:06', '', NULL),
(451, 58, 'Blackjack', '00:02:22', '', NULL),
(452, 58, 'Hustle Bones', '00:03:03', '', NULL),
(453, 58, 'I\'ve Seen Footage', '00:03:23', '', NULL),
(454, 58, 'Double Helix', '00:02:37', '', NULL),
(455, 58, 'System Blower', '00:03:44', '', NULL),
(456, 58, 'The Cage', '00:03:31', '', NULL),
(457, 58, 'Punk Weight', '00:03:25', '', NULL),
(458, 58, 'Fuck That', '00:02:25', '', NULL),
(459, 58, 'Bitch Please', '00:02:57', '', NULL),
(460, 58, 'Hacker', '00:04:36', '', NULL),
(462, 7, 'Runaway', '00:03:03', 't8YHNDhUuQE', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `code` varchar(50) NOT NULL,
  `is_express` tinyint(1) DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `visible` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `shipping`
--

INSERT INTO `shipping` (`id`, `name`, `company`, `code`, `is_express`, `price`, `visible`) VALUES
(1, 'Standard', 'DHL', 'dhl_0', 0, '5.99', 1),
(2, 'Express', 'DHL', 'dhl_1', 1, '9.99', 1),
(3, 'Basic', 'Hermes', 'hermes_0', 0, '3.99', 1),
(4, 'extreme', 'Hermes', 'hermes_1', 1, '6.99', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `gender`, `phone`, `created_at`, `last_login`) VALUES
(1, 'Pascal', 'Bürkle', 'male', NULL, '0000-00-00 00:00:00', '2018-04-10 18:19:35'),
(5, 'Artur', 'Vazigin', 'male', NULL, '2018-02-28 16:37:33', '2018-04-10 19:28:43'),
(6, 'Andreas', 'Jetter', 'male', NULL, '2018-03-05 10:05:13', '2018-04-08 21:44:34'),
(7, 'Eva', 'Ngo', 'female', NULL, '2018-03-11 12:59:19', '2018-04-10 00:07:41'),
(8, 'Eva', 'Nutella', 'female', NULL, '2018-04-08 18:21:31', '2018-04-08 18:21:31');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user_wishlist`
--

INSERT INTO `user_wishlist` (`wishlist_id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(33, 6, 22, '2018-04-03 21:05:55', NULL),
(81, 1, 5, '2018-04-09 12:06:38', NULL),
(84, 1, 21, '2018-04-09 23:17:33', NULL),
(87, 1, 35, '2018-04-10 22:51:16', NULL),
(88, 1, 33, '2018-04-10 22:51:43', NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `address_fk0` (`user_id`);

--
-- Indizes für die Tabelle `auth`
--
ALTER TABLE `auth`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `bank_transfer`
--
ALTER TABLE `bank_transfer`
  ADD PRIMARY KEY (`bank_transfer_id`),
  ADD UNIQUE KEY `iban` (`iban`);

--
-- Indizes für die Tabelle `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`credit_card_id`);

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_fk0` (`user_id`),
  ADD KEY `order_fk1` (`payment_address_id`),
  ADD KEY `order_fk2` (`shipping_address_id`),
  ADD KEY `order_fk3` (`payment_id`);

--
-- Indizes für die Tabelle `order_content`
--
ALTER TABLE `order_content`
  ADD PRIMARY KEY (`content_id`),
  ADD KEY `order_content_fk0` (`order_id`),
  ADD KEY `order_content_fk1` (`product_id`);

--
-- Indizes für die Tabelle `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `payment_fk0` (`bank_transfer_id`),
  ADD KEY `payment_fk1` (`credit_card_id`),
  ADD KEY `payment_fk2` (`user_id`);

--
-- Indizes für die Tabelle `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_fk0` (`category_id`),
  ADD KEY `artist_id` (`artist_id`),
  ADD KEY `artist_id_2` (`artist_id`);

--
-- Indizes für die Tabelle `product_artist`
--
ALTER TABLE `product_artist`
  ADD PRIMARY KEY (`artist_id`);

--
-- Indizes für die Tabelle `product_artist_image`
--
ALTER TABLE `product_artist_image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `artist_image_fk0` (`artist_id`);

--
-- Indizes für die Tabelle `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indizes für die Tabelle `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `product_image_fk0` (`product_id`);

--
-- Indizes für die Tabelle `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_review_fk0` (`product_id`),
  ADD KEY `product_review_fk1` (`user_id`);

--
-- Indizes für die Tabelle `product_track`
--
ALTER TABLE `product_track`
  ADD PRIMARY KEY (`track_id`),
  ADD KEY `tracks_fk1` (`product_id`);

--
-- Indizes für die Tabelle `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indizes für die Tabelle `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`wishlist_id`),
  ADD KEY `user_wishlist_fk0` (`user_id`),
  ADD KEY `user_wishlist_fk1` (`product_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT für Tabelle `bank_transfer`
--
ALTER TABLE `bank_transfer`
  MODIFY `bank_transfer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `credit_card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `order_content`
--
ALTER TABLE `order_content`
  MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT für Tabelle `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT für Tabelle `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT für Tabelle `product_artist`
--
ALTER TABLE `product_artist`
  MODIFY `artist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT für Tabelle `product_artist_image`
--
ALTER TABLE `product_artist_image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `product_category`
--
ALTER TABLE `product_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT für Tabelle `product_image`
--
ALTER TABLE `product_image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT für Tabelle `product_review`
--
ALTER TABLE `product_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT für Tabelle `product_track`
--
ALTER TABLE `product_track`
  MODIFY `track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;

--
-- AUTO_INCREMENT für Tabelle `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_fk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `auth`
--
ALTER TABLE `auth`
  ADD CONSTRAINT `auth_fk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_fk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `order_fk1` FOREIGN KEY (`payment_address_id`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `order_fk2` FOREIGN KEY (`shipping_address_id`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `order_fk3` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`payment_id`);

--
-- Constraints der Tabelle `order_content`
--
ALTER TABLE `order_content`
  ADD CONSTRAINT `order_content_fk1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `order_content_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_fk0` FOREIGN KEY (`bank_transfer_id`) REFERENCES `bank_transfer` (`bank_transfer_id`),
  ADD CONSTRAINT `payment_fk1` FOREIGN KEY (`credit_card_id`) REFERENCES `credit_card` (`credit_card_id`),
  ADD CONSTRAINT `payment_fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_fk0` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`category_id`),
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `product_artist` (`artist_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `product_artist_image`
--
ALTER TABLE `product_artist_image`
  ADD CONSTRAINT `artist_image_fk0` FOREIGN KEY (`artist_id`) REFERENCES `product_artist` (`artist_id`);

--
-- Constraints der Tabelle `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `product_image_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `product_review`
--
ALTER TABLE `product_review`
  ADD CONSTRAINT `product_review_fk0` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `product_review_fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD CONSTRAINT `user_wishlist_fk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `user_wishlist_fk1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
