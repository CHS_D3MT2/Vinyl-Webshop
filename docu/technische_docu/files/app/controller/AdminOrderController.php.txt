<?

/**
 * Diese Klasse bearbeitet alle Anfragen, die an Bestelldaten im Adminbereich gerichtet sind
 */
class AdminOrderController extends SubController
{
  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    $this->order = new Order;
  }

  /**
   * Zeigt die Seite welche alle vorhandenen Bestellungen darstellt
   * @return void
   */
  public function view_all()
  {
    $this->view->set_data([
      'orders' => $this->order->get_all(),
      'title' => 'Bestell-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('order_list');
  }

  /**
   * Zeigt die Seite welche eine Bestellung im Detail darstellt.
   * @return void
   */
  public function view_detail()
  {
    $order = $this->order->set_order_id($this->data['order_id'])->get_by_id();

    $payment_address = (new Address)->get_by_id($order['payment_address_id']);
    $shipping_address = (new Address)->get_by_id($order['payment_address_id']);
    $payment = (new Payment)->get_by_id($order['payment_id']);
    $shipping = (new Shipping)->get_by_code($order['shipping_code']);

    $this->view->set_data([
      'order' => $order,
      'payment_address' => $payment_address,
      'shipping_address' => $shipping_address,
      'payment' => $payment,
      'shipping' => $shipping,
      'title' => 'Detailansicht der Bestellung #'.$this->data['order_id']
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('order_detail');
  }

  /**
   * Löscht eine vorhandene Bestellung
   * @return void
   */
  public function delete()
  {
    $this->order->set_order_id($this->data['order_id']);
    $this->order->delete();
    $this->redirect->to('admin/order?view=all')->go();
  }

}

