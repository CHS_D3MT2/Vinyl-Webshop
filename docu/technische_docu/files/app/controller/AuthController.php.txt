<?

/**
 * Ist für die Authentifizierungsvorgänge wie Registrierung und Login zuständig.
 */
class AuthController extends BaseController
{

  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    $this->auth = new Auth();
  }

  // Nutzer will sich registrieren
  public function register()
  {
    $this->auth->set_data_to_process($this->data);

    if (!$this->auth->exists()) {

      if ($this->data['password'] === $this->data['password_repeat']) {

        $this->auth->create();
        $this->auth->login();

      } else {
        (new Notification)->set('password_not_same', 'error');
      }

    } else {
      (new Notification)->set('email_exists', 'error');
    }

    (new Redirect)->to_index()->go();
  }

  // Nutzer will sich anmelden
  public function login()
  {
    $this->auth->set_data_to_process($this->data);

    if ($this->auth->exists()) {

      if ($this->auth->verify_password()) {

        $this->auth->login();

      } else {
        (new Notification)->set('login', 'error');
      }

    } else {
      (new Notification)->set('login', 'error');
    }

    (new Redirect)->to_origin()->go();
  }

  public function logout()
  {
    $this->auth->logout();

    $this->redirect->to_index()->go();
  }

  public function update_role()
  {
    $this->auth->set_data_to_process($this->data);
    $this->auth->set_user_id($this->data['user_id']);
    $this->auth->update_role();

    if (Session::read('user_data.user_id') == $this->data['user_id']) {
      Session::write('user_data', [
        'role' => $this->auth->get_role()
      ]);
    }

    $this->redirect->to('admin/user?view=all')->go();
  }

  public function update_info()
  {
    $this->auth->set_data_to_process($this->data['auth_info']);
    $this->auth->update_email();

    if ($this->auth->password_change_allowed()) {
      $this->auth->update_password();
    }

    Session::write('user_data', [
      'email' => $this->auth->get_info()['email']
    ]);

    $this->redirect->to('/account')->go();
  }

}

