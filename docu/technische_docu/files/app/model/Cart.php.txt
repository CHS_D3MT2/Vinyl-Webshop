<?

/**
 * Beschreibt die Funktionalität eines Warenkorbs
 */
class Cart extends BaseModel
{
  /**
   * Die standardmäßig verwendete Menge beim Einfügen/Hinzufügen eines Produkts.
   * @var integer
   */
  protected $amount = 1;

  /**
   * Menge eines Produktes welches in der Session gespeichert ist.
   * @var integer;
   */
  protected $session_amount;

  /**
   * Gesamtanzahl der gespeicherten Produkte.
   * @var integer
   */
  protected $total_amount = 0;

  /**
   * Gesamtpreis der gespeicherten Produkte.
   * @var integer
   */
  protected $total_price = 0;

  /**
   * Setter für Produkt-ID.
   * @param [type] $id [description]
   */
  public function set_product_id($id)
  {
    $this->product_id = $id;
  }

  public function set_amount($amount)
  {
    $this->amount = $amount;
  }

  public function set_price($price)
  {
    $this->price = $price;
  }

  /**
   * Erhöht die Anzahl eines Produktes in der Session um die gesetzte Anzahl.
   * @return void
   */
  public function add_to_session()
  {
    $_SESSION['cart'][$this->product_id]['amount'] = $this->session_amount + $this->amount;
    $_SESSION['cart'][$this->product_id]['price'] = $this->price;
  }

  /**
   * Fügt ein Produkt zum Warenkorb hinzu.
   * @return void
   */
  public function create_in_session()
  {
    $_SESSION['cart'][$this->product_id]['amount'] = $this->amount;
    $_SESSION['cart'][$this->product_id]['price'] = $this->price;
  }

  /**
   * Überprüft, ob ein Produkt bereits im Warenkorb in der Session gespeichert ist.
   * @return boolean Produkt ist bereits vorhanden
   */
  public function exists_in_session()
  {
    if (isset($_SESSION['cart'][$this->product_id])) {
      $amount = $_SESSION['cart'][$this->product_id]['amount'];

      if (isset($amount)) {
        $this->session_amount = $amount;
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * [get_total_stats description]
   * @return [type] [description]
   */
  public function get_total_stats()
  {
    if (!isset($_SESSION['cart'])) {
      return false;
    }
    foreach ($_SESSION['cart'] as $item) {
      $this->total_price = floatval($this->total_price + ($item['amount'] * $item['price']));
      $this->total_amount = intval($this->total_amount + $item['amount']);
    }

    $cart_stats['price'] = $this->total_price;
    $cart_stats['amount'] = $this->total_amount;

    return $cart_stats;
  }

  /**
   * Gibt den gesamten Session-Warenkorb und zugehörige Produkt-Infos zurück.
   * @return array Warenkorb-Inhalt
   */
  public function get_all()
  {
    $this->product = new Product();

    $items = [];

    if (!isset($_SESSION['cart'])) {
      return false;
    }

    foreach ($_SESSION['cart'] as $key => $item) {

      $this->product->set_product_id($key);

      $items[] = $this->product->get_by_id();

    }

    return $items;
  }

  /**
   * Gibt den gesamten Session-Warenkorb zurück
   * @return array Warenkorb-Inhalt
   */
  public function get_session_cart()
  {
    return Session::read('cart');
  }

  /**
   * Löscht ein Produkt aus dem Session-Warenkorb.
   * @return void
   */
  public function delete()
  {
    Session::clear('cart', $this->product_id);
  }

  /**
   * Leert den gesamten Session-Warenkorb.
   * @return void
   */
  public function clear()
  {
    Session::clear('cart');
  }

  /**
   * Setzt die Anzahl eines Produktes im Warenkorb auf die festgesetzte Menge.
   * @return void
   */
  public function update_amount()
  {
    if (isset($_SESSION['cart'][$this->product_id])) {
      $_SESSION['cart'][$this->product_id]['amount'] = $this->amount;
    }
  }

}

