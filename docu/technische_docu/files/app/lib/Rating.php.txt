<?

/**
 * Diese Klasse ist für die Darstellung des Bewertungssystems zuständig.
 */
class Rating
{
  /**
   * Stellt nur einen gewissen Stand der Bewertung dar, welcher nicht bearbeitet werden kann.
   * @param  integer $score Rating-Wert, welcher dargestellt werden soll.
   * @return string
   */
  public static function display_only($score)
  {
    $score = intval($score);

    ?>
    <div class="rating d-inline-block display-only">
    <?

    for($i = 0; $i < $score; $i++) : ?>

      <i class="fas fa-star rating_star"></i>

    <? endfor;

    for($i = $score; $i < 5; $i++) : ?>

      <i class="far fa-star rating_star"></i>

    <? endfor; ?>
    </div>
    <?
  }

  /**
   * Stellt nur einen gewissen Stand der Bewertung dar, welcher bearbeitet werden kann.
   * @return string
   */
  public static function display_editable()
  {
    ?>
    <div class="rating d-inline-block editable">
      <i class="far fa-star rating_star"></i>
      <i class="far fa-star rating_star"></i>
      <i class="far fa-star rating_star"></i>
      <i class="far fa-star rating_star"></i>
      <i class="far fa-star rating_star"></i>
    </div>
    <?
  }

}

