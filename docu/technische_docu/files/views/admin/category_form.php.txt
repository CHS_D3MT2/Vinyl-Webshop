<form method="post" 
      action="/admin/category<?= (isset($form_params) ? '?'.$form_params : ''); ?>" 
      enctype="multipart/form-data" 
      class="w-100"
      id="content_form">

  <div class="col-md-12">
    
    <div class="row">

      <div class="form-group col-sm-12">
        
        <div class="form-group">
          
          <label for="product_name">Genre-Name</label>
          <input type="text" class="form-control" id="product_name" placeholder="Wie heißt das Genre?" 
                 name="category_name" 
                 value="<?= (isset($category) ? $category['category_name'] : ''); ?>">
          
        </div>
        
      </div>
      
    </div>
    
  </div>
    
</form>
