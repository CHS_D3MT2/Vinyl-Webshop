<h1><?= $category['category_name']; ?></h1>

<div class="form-row mt-4">

  <? foreach($products as $product) : ?>

  <div class="col-lg-2 col-md-3 col-6 pb-2 product-preview">

    <div class="card" data-role="base" data-product-id="<?= $product['product_id']; ?>">
      <form>
        <input type="hidden" name="product_id" value="<?= $product['product_id']; ?>"/>
        <input type="hidden" name="price" value="<?= $product['price']; ?>"/>
        <div class="card-img-container">
          <? if($product['stock'] < 5): ?>
            <span class="badge badge-warning">Nur noch wenig auf Lager.</span>
          <? endif; ?>
          <figure>
            <a class="product-title d-block" href="/product/<?= $product['slug']; ?>">
              <img class="card-img-top" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
            </a>
          </figure>
          <div class="card-actions">
            <div>
              <button
                class="btn"
                type="button"
                data-url="cart"
                data-action="add_item"
                data-submit="ajax"
                data-price="<?= $product['price']; ?>"
                data-success='update_cart_preview'
              >
                <i class="<?= FA_STYLE ?> fa-cart-plus"></i>
              </button>
              <button
                class="btn"
                type="button"
                data-url="wishlist"
                data-action="add_to_wishlist"
                data-submit="ajax"
              >
                <i class="<?= FA_STYLE ?> fa-plus"></i>
              </button>
            </div>
            <p class="price align-self-center product-price"><?= $product['price']; ?>€</p>
          </div>
        </div>
        <div class="card-body">
          <div class="card-text">
            <a class="product-title d-block" href="/product/<?= $product['slug'] ?>">
              <?= $product['name']; ?>
            </a>
            <a class="product-artist d-block" href="/artist/<?= $product['artist_id'] ?>">
              <?= $product['artist_name']; ?>
            </a>
          </div>
        </div>
      </form>
    </div>

  </div>

  <? endforeach; ?>

</div>
