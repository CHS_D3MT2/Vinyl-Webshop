<h1 class="mb-5">Warenkorb</h1>

<?

$total_price = 0;
$total_amount = 0;

if (isset($cart_items) && !empty($cart_items)) :

foreach ($cart_items as $product) :

$product_id = $product['product_id'];

$total_price += floatval($product['price'] * $cart[$product_id]['amount']);
$total_amount += intval($cart[$product_id]['amount']);

?>

<div class="row cart-item" data-role="base">
  <div class="col-lg-1 col-md-2 col-3">
    <a href="/product/<?= $product['slug']; ?>">
      <figure class="m-0">
        <img class="card-img-top" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
      </figure>
    </a>
  </div>
  <div class="col-lg-7 col-md-6 col-9">
    <span class="align-middle h4">
      <a href="/artist/<?= $product['artist_slug']; ?>">
        <?= $product['artist_name']; ?>
      </a> -
      <a href="/product/<?= $product['slug']; ?>">
        <?= $product['name']; ?>
      </a>
    </span>
    <div>
      <button type="button"
              class="btn btn-secondary btn-sm mt-2 mr-2"
              data-url="cart"
              data-action="delete_item"
              data-params="product_id=<?= $product['product_id']; ?>"
              data-submit="ajax"
              data-success='["remove_base", "update_cart_stats", "update_cart_view"]'>
        <i class="<?= FA_STYLE ?> fa-times mr-2"></i><span>Löschen</span>
      </button>
      <button class="btn btn-secondary btn-sm mt-2"
              type="button"
              data-url="wishlist"
              data-action="add_to_wishlist"
              data-params="product_id=<?= $product['product_id']; ?>"
              data-submit="ajax">
        <i class="<?= FA_STYLE ?> fa-plus mr-2"></i><span>In Wunschliste speichern</span>
      </button>
    </div>
  </div>

  <div class="col-lg-2 col-6">
    <span>Preis: </span>
    <span class="h4"><?= $product['price']; ?></span>
    <span class="h4"> €</span>
  </div>


  <div class="col-lg-2 col-md-3 col-6 text-right">
    <form class="no-send">

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">Menge:</span>
        </div>
        <input
          data-change-submit="ajax"
          data-url="cart"
          data-action="update_amount"
          data-params="product_id=<?= $product['product_id']; ?>"
          name="cart[item_amount]"
          type="number"
          data-price="<?= $product['price']; ?>"
          data-type="item-amount"
          class="form-control d-inline-block"
          min="1" value="<?= (isset($cart) && !empty($cart) ? $cart[$product_id]['amount'] : ''); ?>">
      </div>

    </form>

  </div>

  <div class="col-12">
    <hr />
  </div>

</div>

<? endforeach;

?>

<div id="cart-stats">
  <div>
    <div>
      Artikel im Warenkorb: <span id="cart_total_amount"><?= $total_amount; ?></span>
    </div>

    <div>
      Gesamtpreis: <span id="cart_total_price"><?= $total_price; ?></span>€
    </div>
  </div>

  <div class="d-flex justify-content-end">
    <a <?= ($is_logged_in ? 'href="/checkout/address"' : 'data-toggle="modal" data-target="#auth_modal"'); ?>
        class="btn btn-primary btn-lg mt-2">
        Zur Kasse
    </a>
  </div>
</div>

<?

else : ?>

<div class="text-center mt-5">
  <h3>Du hast noch keine Artikel zum Warenkorb hinzugefügt.</h3>
  <a class="btn btn-primary mt-3" href="<?= BASE_URL; ?>">Zurück zum Shop</a>
</div>

<? endif; ?>
