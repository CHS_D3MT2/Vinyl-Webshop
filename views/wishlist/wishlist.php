<h1 class="pb-5">
  Meine Wunschliste
</h1>

<?

$product_amount = count($wishlist_items);

if (isset($wishlist_items) && !empty($wishlist_items)) :

foreach ($wishlist_items as $key => $product) :

$product_id = $product['product_id'];

?>

<div class="row wishlist-item" data-role="base">
  <input type="hidden" name="price" value="<?= $product['price']; ?>"/>
  <div class="col-sm-1">
    <a href="/product/<?= $product['slug']; ?>">
      <figure class="m-0">
        <img class="card-img-top" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
      </figure>
    </a>
  </div>
  <div class="col-sm-6">
    <span class="align-middle h4">
      <a href="/artist/<?= $product['artist_slug']; ?>">
        <?= $product['artist_name']; ?>
      </a> -
      <a href="/product/<?= $product['slug']; ?>">
        <?= $product['name']; ?>
      </a>
    </span>

    <div>
      <button type="button"
              class="btn btn-secondary btn-sm mt-2 mr-2"
              data-url="wishlist"
              data-action="delete_product"
              data-params="product_id=<?= $product['product_id']; ?>"
              data-submit="ajax"
              data-success='["remove_base", "update_wishlist_view"]'>
        <i class="<?= FA_STYLE ?> fa-times mr-2"></i><span>Löschen</span>
      </button>
      <div class="btn-group">
        <button
          class="btn btn-secondary btn-sm mt-2"
          type="button"
          data-url="cart"
          data-action="add_item"
          data-submit="ajax"
          data-price="<?= $product['price']; ?>"
          data-success='["update_cart_preview"]'
          data-params="product_id=<?= $product['product_id']; ?>&price=<?= $product['price']; ?>">
          <i class="<?= FA_STYLE ?> fa-plus mr-2"></i><span>Zum Warenkorb hinzufügen</span>
        </button>
      </div>
    </div>
  </div>
  <div class="col-sm-2">
    <span>Hinzugefügt am: </span>
    <span class="h4"><?= DateTimeHelper::to_nice_date($product['date_added']); ?></span>
  </div>


  <div class="col-sm-3 text-right">
    <span>Preis: </span>
    <span class="h4"><?= $product['price']; ?></span>
    <span class="h4"> €</span>
  </div>

  <?= ($key !== $product_amount - 1 ? '<div class="col-12"><hr /></div>' : ''); ?>

</div>

<? endforeach;

else:

?>
<div class="text-center mt-5">
  <h3>Du hast noch keine Artikel zur Wunschliste hinzugefügt.</h3>
  <a class="btn btn-primary mt-3" href="<?= BASE_URL; ?>">Zurück zum Shop</a>
</div>
<?

endif;

?>
