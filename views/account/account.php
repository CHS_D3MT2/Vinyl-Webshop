<h1 class="pb-5">
  Mein Account
</h1>

<div class="row">

  <div class="col-md-3">

    <nav>
      <div class="nav flex-column nav-pills" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" data-toggle="pill" href="#nav-personal" role="tab">Persönliche Daten</a>
        <a class="nav-item nav-link" data-toggle="pill" href="#nav-login" role="tab">Zugangsdaten</a>
        <a class="nav-item nav-link" data-toggle="pill" href="#nav-address" role="tab">Adressen</a>
        <a class="nav-item nav-link" data-toggle="pill" href="#nav-payment" role="tab">Zahlungsinfos</a>
      </div>
    </nav>

  </div>

  <div class="col-md-9">

    <div class="tab-content">

      <div class="tab-pane fade show active" id="nav-personal" role="tabpanel">

        <h2 class="mb-4">Meine Daten</h2>

        <?

        $pf = new FormBuilder;

        $pf
          ->attributes([
            'id' => 'personal_form',
            'action' => '/account?action=change_user_info',
            'input_group' => 'user_info'
          ])
          ->options([
            'build_buttons' => true
          ])
          ->layout('2 2')
          ->inputs([
            [
              'input_type' => 'textbox',
              'type' => 'text',
              'label' => 'Vorname',
              'name' => 'first_name',
              'value' => $user_info['first_name']
            ],
            [
              'input_type' => 'textbox',
              'type' => 'text',
              'label' => 'Nachname',
              'name' => 'last_name',
              'value' => $user_info['last_name']
            ],
            [
              'input_type' => 'tick',
              'type' => 'radio',
              'label' => 'Weiblich',
              'name' => 'gender',
              'value' => 'female',
              'checked' => ($user_info['gender'] === 'female' ? true : false)
            ],
            [
              'input_type' => 'tick',
              'type' => 'radio',
              'label' => 'Männlich',
              'name' => 'gender',
              'value' => 'male',
              'checked' => ($user_info['gender'] === 'male' ? true : false)
            ]
          ])
          ->buttons([
            [
              'text' => 'Speichern',
              'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary'
              ]
            ]
          ])
          ->build();

        ?>

      </div>

      <div class="tab-pane fade" id="nav-login" role="tabpanel">

        <h2 class="mb-4">Meine Zugangsdaten</h2>

        <?

        $lf = new FormBuilder;

        $lf
          ->attributes([
            'id' => 'login_form',
            'action' => '/auth?action=update_info',
            'input_group' => 'auth_info'
          ])
          ->options([
            'build_buttons' => true
          ])
          ->layout('1 3')
          ->inputs([
            [
              'input_type' => 'textbox',
              'type' => 'email',
              'label' => 'Email',
              'name' => 'email',
              'value' => $auth_info['email']
            ],
            [
              'input_type' => 'textbox',
              'type' => 'password',
              'label' => 'Altes Passwort',
              'name' => 'old_pw'
            ],
            [
              'input_type' => 'textbox',
              'type' => 'password',
              'label' => 'Neues Passwort',
              'name' => 'new_pw'
            ],[
              'input_type' => 'textbox',
              'type' => 'password',
              'label' => 'Neues Passwort wiederholen',
              'name' => 'new_pw_repeat'
            ]
          ])
          ->buttons([
            [
              'text' => 'Speichern',
              'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary'
              ]
            ]
          ])
          ->build();

        ?>

      </div>

      <div class="tab-pane fade" id="nav-address" role="tabpanel">

        <h2 class="mb-4">Meine Adressen</h2>

        <div class="row">

        <? foreach ($addresses as $key => $address) : ?>

        <div class="col-md-6" data-role="base">

          <div class="address">

            <p><?= $address['first_name']." ".$address['last_name']; ?></p>
            <?= (isset($address['company']) ? '<p>'.$address['company'].'</p>' : ''); ?>
            <p><?= $address['country']; ?></p>
            <p><?= $address['city']." ".$address['plz']; ?></p>
            <p><?= $address['street']." ".$address['housenumber']; ?></p>
            <!-- <?= (isset($address['phone']) ? '<p>'.$address['phone'].'</p>' : ''); ?> -->

            <div class="actions">

              <a
                class="cursor-pointer"
                data-submit="ajax"
                data-url="account"
                data-action="delete_address"
                data-success="remove_base"
                data-params="address_id=<?= $address['address_id']; ?>">
                <i class="<?= FA_STYLE ?> fa-trash"></i>
              </a>

            </div>

          </div>

        </div>

        <? endforeach; ?>

        </div>

        <div class="d-flex justify-content-between">
          <button class="btn btn-secondary mt-2"  data-toggle="collapse" data-target="#new_address">
            Neue Adresse hinzufügen
          </button>
        </div>

        <div class="collapse mt-3" id="new_address">
          <div>

            <? (new FormBuilder)->load_template('address')->build(); ?>

          </div>
        </div>

      </div>

      <div class="tab-pane fade" id="nav-payment" role="tabpanel">

        <h2 class="mb-4">Meine Zahlungsinformationen</h2>

        <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-bank-tab" data-toggle="pill" href="#pills-bank" role="tab">SEPA-Lastschrift</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-credit-tab" data-toggle="pill" href="#pills-credit" role="tab">Kreditkarte</a>
          </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">

          <div class="tab-pane show active" id="pills-bank" role="tabpanel">

            <div>

            <? foreach ($bank_transfers as $key => $bt) : ?>

              <div class="address" data-role="base">

                <div class="actions">

                  <a
                    class="cursor-pointer"
                    data-submit="ajax"
                    data-url="account"
                    data-action="delete_payment"
                    data-success="remove_base"
                    data-params="payment_id=<?= $bt['payment_id']; ?>">
                    <i class="<?= FA_STYLE ?> fa-trash"></i>
                  </a>

                </div>

                <p>IBAN: <?= $bt['iban']; ?></p>
                <p>BIC: <?= $bt['bic']; ?></p>

              </div>

            <? endforeach; ?>

            </div>
            <div class="d-flex justify-content-between">
              <a class="btn btn-secondary mt-2"  data-toggle="collapse" data-target="#new_bank_transfer" role="button">
                Neue Bankverbindung hinzufügen
              </a>
            </div>

            <div class="collapse mt-3" id="new_bank_transfer">

              <? (new FormBuilder)->load_template('bank_transfer')->build(); ?>

            </div>

          </div>

          <div class="tab-pane" id="pills-credit" role="tabpanel">

            <div>

            <? foreach ($credit_cards as $key => $cc) : ?>

              <div class="address" data-role="base">

                <div class="actions">

                  <a
                    class="cursor-pointer"
                    data-submit="ajax"
                    data-url="account"
                    data-action="delete_payment"
                    data-success="remove_base"
                    data-params="payment_id=<?= $cc['payment_id']; ?>">
                    <i class="<?= FA_STYLE ?> fa-trash"></i>
                  </a>

                </div>

                <p>Anbieter: <?= $cc['provider']; ?></p>
                <p>Kartennummer: <?= $cc['card_number']; ?></p>
                <p>Ablaufdatum: <?= date("m/Y", strtotime($cc['expiration_date'])); ?></p>

              </div>

            <? endforeach; ?>

            </div>

            <div class="d-flex justify-content-between">
              <a class="btn btn-secondary mt-2" data-toggle="collapse" data-target="#new_credit_card" role="button">
                Neue Kreditkarte hinzufügen
              </a>
            </div>

            <div class="collapse mt-3" id="new_credit_card">

              <? (new FormBuilder)->load_template('credit_card')->build(); ?>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>
