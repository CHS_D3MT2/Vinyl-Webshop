<h1 class="pb-5">
  Meine Bestellungen
</h1>

<? if (empty($orders)) : ?>

<div class="text-center mt-5">
  <h3>Du hast noch keine Bestellungen getätigt.</h3>
  <a class="btn btn-primary mt-3" href="<?= BASE_URL; ?>">Zurück zum Shop</a>
</div>

<? else : ?>

<!-- <div class="row py-5">

  <? foreach ($orders as $order) : ?>

  <div class="col-sm-12">

    <div class="mb-4">
      <h3>
        Deine Bestellung am
        <span><?= DateTimeHelper::to_nice_date($order['created_at']); ?></span>
        um
        <span><?= DateTimeHelper::to_nice_time($order['created_at']); ?></span>
      </h3>

      <span>
        Bestell-ID: <span class="text-muted">#<?= $order['order_id']; ?></span>
      </span>
    </div>

    <?

    $total_price = 0;

    foreach ($order['content'] as $product) :

      $total_price += $product['price'] * $product['amount'];

      ?>

    <div class="media my-3">
      <a href="/shop/product/<?= $product['product_id']; ?>"><img class="mr-3" src="<?= PRODUCT_IMAGE.$product['url']; ?>" style="max-width: 55px;"></a>
      <div class="media-body">
        <h5 class="mt-0"><a href="/shop/product/<?= $product['product_id']; ?>"><strong><?= $product['name']; ?></strong></a></h5>
        <span>Einzelpreis: </span><span><?= $product['price']; ?>€</span>
        <span class="ml-5">Menge: <?= $product['amount']; ?></span>
        <span class="ml-5">Gesamtpreis: <?= number_format(intval($product['amount']) * floatval($product['price']), 2); ?>€</span>
      </div>
    </div>

    <? endforeach; ?>

    <div class="d-flex">
      <div class="ml-auto d-block">
        <span class="h3">Gesamtpreis: <span><?= $total_price; ?>€</span></span>
      </div>
    </div>


    <hr class="my-5"/>

  </div>

  <? endforeach; ?>

</div> -->

<div class="row">

  <div class="col-md-3">

    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

    <? foreach ($orders as $key => $order) : ?>

      <a class="nav-link <?= ($key === 0 ? 'active' : ''); ?>" data-toggle="pill" href="#order_<?= $order['order_id']; ?>" role="tab"><?= DateTimeHelper::to_nice_date($order['created_at']); ?> – <?= $order['total_price'] + $order['shipping_price']; ?>€</a>

    <? endforeach; ?>

    </div>

  </div>

  <div class="col-md-9">

    <div class="tab-content" id="v-pills-tabContent">

    <? foreach ($orders as $key => $order) : ?>

      <div class="tab-pane fade <?= ($key === 0 ? 'show active' : ''); ?>" id="order_<?= $order['order_id']; ?>" role="tabpanel">

        <div class="row">

        <? foreach ($order['content'] as $product) : ?>

          <div class="col-md-6 mb-5">

            <div class="media">
              <a href="/product/<?= $product['slug']; ?>"><img class="mr-3" src="<?= PRODUCT_IMAGE.$product['url']; ?>" style="max-width: 100px;"></a>
              <div class="media-body">
                <h5 class="mt-0 mb-2">
                  <a href="/product/<?= $product['slug']; ?>">
                    <strong><?= $product['name']; ?></strong>
                  </a>
                </h5>
                <div class="mb-2">
                  <a href="/artist/<?= $product['artist_slug']; ?>"><?= $product['artist_name']; ?></a>
                </div>
                <span>Einzelpreis: </span><span><?= $product['price']; ?>€</span>
                <span class="ml-5">Menge: <?= $product['amount']; ?></span>
                <!-- <span class="ml-5">Gesamtpreis: <?= number_format(intval($product['amount']) * floatval($product['price']), 2); ?>€</span> -->
              </div>
            </div>

          </div>

        <? endforeach; ?>

          <div class="col-sm-12">
            <hr />
          </div>

          <div class="col-sm-12 d-flex justify-content-end">
            <div>
              <p>
                Lieferung: <?= $order['company']." ".$order['shipping_name']; ?>: <?= $order['shipping_price']; ?>€
              </p>
              <h3>
                Gesamtpreis: <?= $order['total_price'] + $order['shipping_price']; ?>€
              </h3>
            </div>
          </div>

        </div>

      </div>

    <? endforeach; ?>

    </div>

  </div>

</div>

<? endif; ?>
