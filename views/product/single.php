<div class="product-single pt-5">
  <!-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb mb-5">
      <li class="breadcrumb-item"><a href="/">Home</a></li>
      <li class="breadcrumb-item"><a href="/products">Produkte</a></li>
      <li class="breadcrumb-item active"><?= $product['artist_name'] . " - " . $product['name']; ?></li>
    </ol>
  </nav> -->

  <div class="row" data-role="base">

    <form id="product-infos">
      <input type="hidden" name="price" value="<?= $product['price']; ?>"/>
      <input type="hidden" name="product_id" value="<?= $product['product_id']; ?>"/>
    </form>

    <div class="col-md-4">
      <figure>
        <img class="w-100" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
      </figure>

      <div class="d-block my-3">
        <strong>Release: <?= DateTimeHelper::to_nice_date($product['release_date']); ?></strong>
      </div>

      <p><?= nl2br($product['description']); ?></p>
    </div>
    <div class="col-md-8">

      <span>Album</span><span class="ml-2 badge badge-warning">Nur noch wenig auf Lager.</span>
      <h1 class="mb-2"><?= $product['name']; ?></h1>

      <div class="d-block mb-3">
        <a href="/artist/<?= $product['artist_slug']; ?>"><?= $product['artist_name']; ?></a>
      </div>

      <div class="my-3">
        <? Rating::display_only($product['rating']); ?>
        <span class="ml-3 font-weight-bold"><?= $product['rating']; ?></span><span> bei </span><span><?= $product['vote_amount']; ?> Votes</span>
      </div>

      <div class="d-flex align-items-center">

        <button
          class="btn btn-primary"
          type="button"
          data-url="cart"
          data-action="add_item"
          data-submit="ajax"
          data-form="#product-infos"
          data-success="update_cart_preview">
          <i class="<?= FA_STYLE ?> fa-cart-arrow-down pr-1"></i> <?= $product['price']; ?>€
        </button>

        <button class="btn btn-secondary ml-3"
                type="button"
                data-url="wishlist"
                data-action="add_to_wishlist"
                data-params="product_id=<?= $product['product_id']; ?>"
                data-submit="ajax">
          <i class="<?= FA_STYLE ?> fa-plus mr-2"></i><span>Wunschliste</span>
        </button>

      </div>

      <div id="tracklist" class="mt-5">
        <?

        $place = 1;

        foreach ($product['tracks'] as $track): ?>

            <div class="input-group mb-2" data-role="base">

              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><?= $place; ?>.</span>
              </div>

              <span class="form-control light-bg">

              <? if (isset($track['youtube_id'])) : ?>

                <a data-action="play-track" class="play-button">
                  <i class="<?= FA_STYLE ?> fa-play mr-2"></i>
                </a>

              <? endif; ?>

                <span class="track-name"><?= $track['name']; ?></span>
              </span>

              <div class="input-group-append">
                <span class="input-group-text"><?= DateTimeHelper::to_minutes($track['length']); ?></span>
              </div>

              <? if (isset($track['youtube_id'])) : ?>

              <div class="youtube-player d-none">
                <div>
                  <iframe width="300" height="300"
                    src="https://www.youtube.com/embed/<?= $track['youtube_id']; ?>">
                  </iframe>
                </div>
              </div>

              <? endif; ?>

            </div>

        <?

        $place++;
        endforeach;

        ?>
      </div>

      <h3 class="mt-5">Bewertungen</h3>

      <? if (!empty($reviews)) : ?>

      <? foreach ($reviews as $key => $review) : ?>

      <div class="py-3 my-3 review" data-review-id="<?= $review['review_id']; ?>" data-role="base">

        <? if ($review['user_id'] === $user['user_id']) : ?>

        <div class="actions">
          <a
            class="cursor-pointer"
            data-submit="ajax"
            data-url="review"
            data-action="delete"
            data-success="remove_base"
            data-params="review_id=<?= $review['review_id']; ?>">
            <i class="<?= FA_STYLE ?> fa-trash"></i>
          </a>
        </div>

        <? endif; ?>

        <h5 class=" mb-2"><?= $review['title']; ?></h5>
        <div class="mb-2">
          <span class="h6 mr-2">Bewertung: </span><? Rating::display_only($review['rating']); ?>
        </div>
        <p><?= $review['content']; ?></p>
        <div class="mt-2 d-inline-block">
          <span class="text-muted">Von <?= $review['first_name']." ".$review['last_name']; ?></span>
        </div>
      </div>

      <? endforeach; ?>

      <? else : ?>

      <p class="text-muted">
        Dieses Album hat noch niemand bewertet.
      </p>

      <? endif; ?>

      <? if ($is_logged_in) : ?>

      <div class="py-3" data-role="base">
        <form action="/review?action=create" method="POST">
          <h5 class=" mb-3">Wie ist deine Meinung?</h5>
          <div class="mb-3" data-type="review_score">
            <span class="h6">Bewertung: </span><? Rating::display_editable(); ?>
          </div>
          <input required type="text" class="form-control mb-3" name="review[title]" placeholder="Überschrift">
          <textarea required class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Bewertungstext" name="review[content]"></textarea>
          <input type="hidden" required class="rating-input" name="review[rating]"/>
          <input type="hidden" name="review[product_id]" value="<?= $product['product_id']; ?>"/>
          <button
            class="btn btn-primary d-block ml-auto mt-3" type="submit">Veröffentlichen</button>
        </form>
      </div>

      <? else : ?>

      <p class="text-muted pt-2"><a href="" class="main-color" data-toggle="modal" data-target="#auth_modal">Melde dich jetzt an</a> um eine Bewertung zu schreiben.</p>

      <? endif; ?>

      <? if (!empty($more_products_of_artist)) : ?>

      <div class="mt-5">
        <span class="h4 genre-title">Mehr von <?= $product['artist_name']; ?></span>

        <!-- Slider main container -->
        <div class="swiper-container">

          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <? foreach($more_products_of_artist as $product) : ?>

              <div class="swiper-slide product-preview">

                <div class="card" data-role="base" data-product-id="<?= $product['product_id']; ?>">
                  <form>
                    <input type="hidden" name="product_id" value="<?= $product['product_id']; ?>"/>
                    <input type="hidden" name="price" value="<?= $product['price']; ?>"/>
                    <div class="card-img-container">
                      <? if($product['stock'] < 5): ?>
                        <span class="badge badge-warning">Nur noch wenig auf Lager.</span>
                      <? endif; ?>
                      <figure>
                        <a class="product-title d-block" href="/product/<?= $product['slug']; ?>">
                          <img class="card-img-top" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
                        </a>
                      </figure>
                      <div class="card-actions">
                        <div>
                          <button
                            class="btn"
                            type="button"
                            data-url="cart"
                            data-action="add_item"
                            data-submit="ajax"
                            data-success='update_cart_preview'
                          >
                            <i class="<?= FA_STYLE ?> fa-cart-plus"></i>
                          </button>
                          <button
                            class="btn"
                            type="button"
                            data-url="wishlist"
                            data-action="add_to_wishlist"
                            data-submit="ajax"
                          >
                            <i class="<?= FA_STYLE ?> fa-plus"></i>
                          </button>
                        </div>
                        <p class="price align-self-center product-price"><?= $product['price']; ?>€</p>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="card-text">
                        <a class="product-title d-block" href="/product/<?= $product['slug'] ?>">
                          <?= $product['name']; ?>
                        </a>
                        <a class="product-artist d-block" href="/artist/<?= $product['artist_id'] ?>">
                          <?= $product['artist_name']; ?>
                        </a>
                      </div>
                    </div>
                  </form>
                </div>

              </div>

          <? endforeach; ?>

          </div>
          <!-- If we need pagination -->
          <div class="swiper-pagination"></div>

          <!-- If we need navigation buttons -->
          <div class="swiper-button-prev">
            <i class="<?= FA_STYLE ?> fa-chevron-left"></i>
          </div>
          <div class="swiper-button-next">
            <i class="<?= FA_STYLE ?> fa-chevron-right"></i>
          </div>
        </div>
      </div>

      <? endif; ?>

      <!-- <div id="accordion" class="mt-5">
        <div class="card">
        <div class="card-header" id="heading-tracklist">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-tracklist">
                Track-List
              </button>
            </h5>

          </div>

          <div class="collapse show" id="collapse-tracklist">
            <div class="card-body">
            <?

            $place = 1;
            foreach ($product['tracks'] as $track): ?>

                <div class="input-group mb-2">
                  <span class="form-control middle-bg">
                    <?= $place; ?>. <?= $track['name']; ?>
                  </span>
                  <div class="input-group-append"><span class="input-group-text"><?= DateTimeHelper::to_minutes($track['length']); ?></span></div>
                </div>

            <?

            $place++;
            endforeach;

            ?>

            </div>
          </div>
          <div class="card-header" id="heading-description">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-description">
                Produktbeschreibung
              </button>
            </h5>
          </div>
          <div class="collapse show" id="collapse-description">
            <div class="card-body">
              <p><?= nl2br($product['description']); ?></p>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>
