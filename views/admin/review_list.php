<div id="product-list" class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach User-ID sortieren">Produkt-ID</th>
        <th scope="col" title="Nach User-ID sortieren">Produkt-Name</th>
        <th scope="col" title="Nach User-ID sortieren">User-ID</th>
        <th scope="col" title="Nach User sortieren">User</th>
        <th scope="col" title="Nach Titel sortieren">Titel</th>
        <th scope="col" title="Nach Bewertung sortieren">Bewertung</th>
        <th scope="col" title="Nach Erstelldatum sortieren">Datum</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($reviews as $review) : ?>

      <tr data-role="base">
        <td><a href="/admin/review?review_id=<?= $review['review_id']; ?>&view=detail"><?= $review['review_id']; ?></a></td>
        <td><?= $review['product_id']; ?></td>
        <td><a href="/admin/product?product_id=<?= $review['product_id']; ?>&view=edit"><?= $review['name']; ?></a></td>
        <td><?= $review['user_id']; ?></td>
        <td><?= $review['first_name']." ".$review['last_name']; ?></td>
        <td><?= $review['title']; ?></td>
        <td><?= Rating::display_only($review['rating']); ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($review['created_at']); ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href="/admin/review?review_id=<?= $review['review_id']; ?>&view=detail">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Detailansicht</a>
              <a class="dropdown-item"
                 href="/admin/review?review_id=<?= $review['review_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>

</div>
