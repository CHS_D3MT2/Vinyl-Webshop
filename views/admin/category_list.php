<div id="product-list" class="col-sm-12">
    
  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach Name sortieren">Name</th>
        <th scope="col" title="Nach Anzahl der Produkte sortieren">Anzahl Produkte</th>
        <th scope="col" title="Nach Erstelldatum sortieren">Erstellt</th>
        <th scope="col"></th>
      </tr>
    </thead>
    
    <tbody>

    <? foreach($categories as $category) : ?>
    
      <tr data-role="base">
        <td><?= $category['category_id']; ?></td>
        <td><a href="/admin/category?category_id=<?= $category['category_id']; ?>&view=edit"><?= ucfirst($category['category_name']); ?></a></td>
        <td><?= ucfirst($category['product_amount']); ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($category['created_at']); ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" 
                 href="/admin/category?category_id=<?= $category['category_id']; ?>&view=edit">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Bearbeiten</a>
              <a class="dropdown-item"
                 href="/admin/category?category_id=<?= $category['category_id']; ?>&action=delete" data-category-id="<?= $category['category_id']; ?>" data-action="category_delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>
        
    </tbody>
       
  </table>
  
</div>