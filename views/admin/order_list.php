<div id="product-list" class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach User-ID sortieren">User-ID</th>
        <th scope="col" title="Nach Kunde sortieren">Kunde</th>
        <th scope="col" title="Nach Preis sortieren">Preis in €</th>
        <th scope="col" title="Nach Rabatt sortieren">Lieferung</th>
        <th scope="col" title="Nach Erstelldatum sortieren">Datum</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($orders as $order) : ?>

      <tr data-role="base">
        <td><a href="/admin/order?order_id=<?= $order['order_id']; ?>&view=detail"><?= $order['order_id']; ?></a></td>
        <td><?= $order['user_id']; ?></td>
        <td><?= $order['first_name']." ".$order['last_name']; ?></td>
        <td><?= $order['total_price']; ?></td>
        <td><?= $order['company']." ".$order['shipping_name']; ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($order['created_at']); ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href="/admin/order?order_id=<?= $order['order_id']; ?>&view=detail">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Detailansicht</a>
              <a class="dropdown-item"
                 href="/admin/order?order_id=<?= $order['order_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>

</div>
