

<form method="post"
      action="/admin/product<?= (isset($form_params) ? '?'.$form_params : ''); ?>"
      enctype="multipart/form-data"
      class="w-100"
      id="content_form">

  <div class="col-md-12">

    <div class="row">

      <div class="form-group col-sm-12">
        <div class="form-group">
        <label for="product_name">Produktname</label>
        <input type="text" class="form-control" id="product_name" placeholder="Wie heißt dein Produkt?"
               name="product[name]"
               value="<?= (isset($product) ? $product['name'] : ''); ?>">
        </div>
        <div class="form-group">
        <label for="product_description">Produktbeschreibung</label>
        <textarea class="form-control product-description-area" id="product_description" placeholder="Beschreibe dein Produkt"
                  name="product[description]" rows="5"><?= (isset($product) ? $product['description'] : '');; ?></textarea>
        </div>
      </div>

    </div>

    <div class="row">

      <div class="form-group col-sm-12">

        <div class="row">

          <div class="form-group col-sm-4">
            <label for="product_price">Produktpreis</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">€</div>
              </div>
              <input type="text" class="form-control" id="product_price" placeholder="Preis" name="product[price]" value="<?= (isset($product) ? $product['price'] : ''); ?>">
            </div>
            </div>
          <div class="form-group col-sm-4">
            <label for="product_discount">Rabatt</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">%</div>
              </div>
              <input type="number" min="0" max="100" class="form-control" id="product_discount" placeholder="Rabatt" name="product[discount]" value="<?= (isset($product) ? $product['discount'] : ''); ?>">
            </div>
          </div>
          <div class="form-group col-sm-4">
            <label for="product_amount">Stückzahl</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">X</div>
              </div>
              <input type="number" min="1" max="99999" class="form-control" id="product_amount" placeholder="Stückzahl" name="product[stock]" value="<?= (isset($product) ? $product['stock'] : '');; ?>">
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="row mb-4" data-role="base">
      <div class="form-group col-sm-4">
        <label>Genre</label>
        <select data-type="product_category" class="custom-select" name="product[category]">

          <? foreach ($categories as $category) : ?>

          <option <?= (isset($product) && $product['category_id'] == $category['category_id'] ? 'selected' : ''); ?> value="<?= $category['category_id']; ?>"><?= ucfirst($category['category_name']); ?></option>

          <? endforeach; ?>

        </select>
      </div>
      <div class="form-group col-sm-4">
        <label>Künstler</label>
        <select data-type="product_category" class="custom-select" name="product[artist]">

          <? foreach ($artists as $artist) : ?>

          <option <?= (isset($artist) && isset($product) ? ($product['artist_id'] == $artist['artist_id'] ? 'selected' : '') : ''); ?> value="<?= $artist['artist_id']; ?>"><?= ucfirst($artist['name']); ?></option>

          <? endforeach; ?>

        </select>
      </div>
      <div class="form-group col-sm-4">
        <label>Veröffentlichungsdatum</label>

        <input type="date" class="form-control" id="product_release_date" placeholder="Veröffentlichungsdatum"
               name="product[release_date]"
               value="<?= (isset($product) ? $product['release_date'] : ''); ?>">

      </div>

      <div class="form-group col">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="customFile" name="product_images[]" multiple>
          <label class="custom-file-label" for="customFile">Cover</label>
        </div>
      </div>

      <? if (isset($product) && !empty($product['image_url'])) : ?>

      <div class="col-sm-12">
        <img src="<?= PRODUCT_IMAGE.$product['image_url']; ?>" class="image-preview"/>
      </div>

      <? endif; ?>

    </div>

    <h5>
      Tracks
    </h5>

    <button type="button" class="btn btn-light btn-block mb-4" data-action="add_track">
      Neuen Track hinzufügen
    </button>

    <div id="track_container">

    <? if (isset($product) && !empty($product['tracks'])) : ?>

    <? foreach ($product['tracks'] as $track) : ?>

      <div class="form-row mb-3 track" data-role="base">

        <div class="col-sm-4">

          <div class="input-group">
            <div class="input-group-prepend">
              <button class="btn btn-outline-secondary handle" type="button"><i class="fa fa-arrows-alt handle"></i></button>
            </div>
            <input type="text" class="form-control track_name" placeholder="Trackname"
                 name="tracks[name][]"
                 value="<?= $track['name']; ?>"/>
          </div>

        </div>

        <div class="col-sm-4">

          <input type="text" class="form-control" placeholder="Youtube-ID" name="tracks[youtube_id][]" value="<?= $track['youtube_id']; ?>">

        </div>

        <div class="col-sm-4">

          <div class="input-group">

            <input type="time" class="form-control track_length" placeholder="Länge des Tracks" step='1' min="00:00:00" max="20:00:00"
                   name="tracks[length][]"
                   value="<?= $track['length']; ?>"/>

            <div class="input-group-append">
              <button class="btn btn-danger" type="button" data-action="delete_track"><i class="fa fa-times"></i></button>
            </div>
          </div>

        </div>

      </div>

    <? endforeach; ?>


    <? else : ?>

      <div class="form-row mb-3 track" data-role="base">

        <div class="col-sm-4">

          <div class="input-group">
            <div class="input-group-prepend">
              <button class="btn btn-outline-secondary handle" type="button"><i class="fa fa-arrows-alt handle"></i></button>
            </div>
            <input type="text" class="form-control" placeholder="Name des Tracks"
             name="tracks[name][]"/>
          </div>

        </div>

        <div class="col-sm-4">

          <input type="text" class="form-control" placeholder="Youtube-ID" name="tracks[youtube_id][]">

        </div>

        <div class="col-sm-4">

          <div class="input-group">

            <input type="time" class="form-control track_length" placeholder="Länge des Tracks" step='1' min="00:00:00" max="20:00:00"
                 name="tracks[length][]"/>

            <div class="input-group-append">
              <button class="btn btn-danger" type="button" data-action="delete_track"><i class="fa fa-times"></i></button>
            </div>
          </div>

        </div>

      </div>

    <? endif; ?>

    </div>

  </div>

</form>
