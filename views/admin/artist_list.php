<div id="product-list" class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach Name sortieren">Name</th>
        <th scope="col" title="Nach Beschreibung sortieren">Vorname</th>
        <th scope="col" title="Nach Kategorie sortieren">Nachname</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($artists as $artist) : ?>

      <tr data-role="base">
        <td><?= $artist['artist_id']; ?></td>
        <td><a href="/admin/artist?artist_id=<?= $artist['artist_id']; ?>&view=edit"><?= $artist['name']; ?></a></td>
        <td><?= $artist['first_name']; ?></td>
        <td><?= $artist['last_name']; ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href="/admin/artist?artist_id=<?= $artist['artist_id']; ?>&view=edit">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Bearbeiten</a>
              <a class="dropdown-item"
                 href="/admin/artist?artist_id=<?= $artist['artist_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>

</div>
