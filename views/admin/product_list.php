<div id="product-list" class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach Name sortieren">Name</th>
        <th scope="col" title="Nach Künstler sortieren">Künstler</th>
        <!-- <th scope="col" title="Nach Beschreibung sortieren">Beschreibung</th> -->
        <th scope="col" title="Nach Genre sortieren">Genre</th>
        <!-- <th scope="col" title="Nach Track Anzahl sortieren">Track Anzahl</th> -->
        <th scope="col" title="Nach Preis sortieren">Preis in €</th>
        <th scope="col" title="Nach Rabatt sortieren">Rabatt in %</th>
        <th scope="col" title="Nach Anzahl im Lager sortieren">Auf Lager</th>
        <th scope="col" title="Nach Erstelldatum sortieren">Erstellt</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($products as $product) : ?>

      <tr data-role="base">
        <td><?= $product['product_id']; ?></td>
        <td><a href="/admin/product?product_id=<?= $product['product_id']; ?>&view=edit"><?= $product['name']; ?></a></td>
        <td><a href="/admin/artist?artist_id=<?= $product['artist_id']; ?>&view=edit"><?= $product['artist_name']; ?></a></td>
        <!-- <td><?= StringHelper::cut($product['description'], 30); ?></td> -->
        <td><a href="/admin/category?category_id=<?= $product['category_id']; ?>&view=edit"><?= ucfirst($product['category_name']); ?></a></td>
        <!-- <td><?= $product['tracks']; ?></td> -->
        <td><?= $product['price']; ?></td>
        <td><?= $product['discount']; ?></td>
        <td><?= $product['stock']; ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($product['created_at']); ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href="/admin/product?product_id=<?= $product['product_id']; ?>&view=edit">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Bearbeiten</a>
              <a class="dropdown-item"
                 href="/admin/product?product_id=<?= $product['product_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>

</div>
