<div class="h-100">

  <div id="admin-nav" class="h-100">

    <? include(COMPONENT.'nav-left.php'); ?>

  </div>

  <div id="admin-content" class="<?= (!$this->show_sidebar ? 'pr-0' : ''); ?>">

    <div class="row py-4">

      <div class="col-sm-12 mb-4">
        <h2><?= (isset($title) ? $title : ''); ?></h2>
      </div>

      <?

      if (isset($this->include))
      {
        include(VIEW_ROOT.$this->directory.DIRECTORY_SEPARATOR.$this->include.'.php');
      }

      ?>

    </div>

  </div>

  <? if ($this->show_sidebar) : ?>

  <div id="admin-sidebar">

    <? include(COMPONENT.$this->sidebar.'.php'); ?>

  </div>

  <? endif; ?>

</div>

<div id="modal_area">

</div>
