<div id="product-list" class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach Vorname sortieren">Vorname</th>
        <th scope="col" title="Nach Nachname sortieren">Nachname</th>
        <th scope="col" title="Nach Rolle sortieren">Rolle</th>
        <th scope="col" title="Nach Geschlecht sortieren">Geschlecht</th>
        <th scope="col" title="Nach Telefonnummer sortieren">Telefon</th>
        <th scope="col" title="Nach Registrierungs-Zeitpunkt sortieren">Registrierung</th>
        <th scope="col" title="Nach Zeitpunkt des letzten Logins sortieren">Letzter Login</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($users as $user) : ?>

      <tr data-role="base">
        <td><?= $user['user_id']; ?></td>
        <td><?= $user['first_name']; ?></td>
        <td><?= $user['last_name']; ?></td>
        <td><?= $user['role']; ?></td>
        <td><?= $user['gender']; ?></td>
        <td><?= (isset($user['phone']) ? $user['phone'] : 'keine'); ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($user['created_at']); ?></td>
        <td><?= DateTimeHelper::to_nice_datetime($user['last_login']); ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href=""
                 data-toggle="modal" data-target="#user_role_select" data-button="update_user_role" data-user-id="<?= $user['user_id']; ?>">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Rolle bearbeiten</a>
              <a class="dropdown-item"
                 href="/admin/user?user_id=<?= $user['user_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>

</div>

<div class="modal fade" id="user_role_select" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <form action="/auth?action=update_role" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">User-Rolle ändern</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <label>Rolle wählen</label>
          <select data-type="product_category" class="custom-select" name="user_role">

            <option value="user">User</option>
            <option value="admin">Admin</option>

          </select>
          <input type="hidden" name="user_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
          <button type="submit" class="btn btn-primary">Speichern</button>
        </div>
      </form>
    </div>
  </div>
</div>
