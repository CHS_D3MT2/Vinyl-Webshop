<div class="col-md-3">

  <div class="mb-5">
    <h3>Lieferadresse</h3>
    <p><?= $shipping_address['first_name']." ".$shipping_address['last_name']; ?></p>
    <?= (isset($shipping_address['company']) ? '<p>'.$shipping_address['company'].'</p>' : ''); ?>
    <p><?= $shipping_address['street']." ".$shipping_address['housenumber']; ?></p>
    <p><?= $shipping_address['city']." ".$shipping_address['plz']; ?></p>
    <p><?= $shipping_address['country']; ?></p>
    <!-- <?= (isset($shipping_address['phone']) ? '<p>'.$shipping_address['phone'].'</p>' : ''); ?> -->
  </div>

</div>

<div class="col-md-3">

  <div class="mb-5">
    <h3>Rechnungsadresse</h3>
    <p><?= $payment_address['first_name']." ".$payment_address['last_name']; ?></p>
    <?= (isset($payment_address['company']) ? '<p>'.$payment_address['company'].'</p>' : ''); ?>
    <p><?= $payment_address['street']." ".$payment_address['housenumber']; ?></p>
    <p><?= $payment_address['city']." ".$payment_address['plz']; ?></p>
    <p><?= $payment_address['country']; ?></p>
    <!-- <?= (isset($payment_address['phone']) ? '<p>'.$payment_address['phone'].'</p>' : ''); ?> -->
  </div>

</div>

<div class="col-md-3">

  <div class="mb-5">
    <h3>Zahlung</h3>

  <? if ($payment['type'] == 'bank_transfer') : ?>

    <p>Lastschrift</p>
    <p>Zahlungsinstitut: <?= $payment['institute']; ?></p>
    <p>IBAN: <?= $payment['iban']; ?></p>
    <p>BIC: <?= $payment['bic']; ?></p>

  <? else : ?>

    <p>Kreditkarte</p>
    <p>Anbieter: <?= $payment['provider']; ?></p>
    <p>Kartennummer: <?= $payment['card_number']; ?></p>
    <p>Ablaufdatum: <?= date("m/Y", strtotime($payment['expiration_date'])); ?></p>

  <? endif; ?>
  </div>

</div>

<div class="col-md-3">

  <div class="mb-5">
    <h3>Lieferung</h3>
    <p><?= $shipping['company']." ".$shipping['name']; ?></p>
    <p>Preis: <?= $shipping['price']; ?>€</p>
  </div>

</div>

<div class="col-sm-12">

  <table class="table table-striped table-responsive-md">
    <thead>
      <tr>
        <th scope="col" title="Nach ID sortieren">ID</th>
        <th scope="col" title="Nach Name sortieren">Name</th>
        <th scope="col" title="Nach Künstler sortieren">Künstler</th>
        <!-- <th scope="col" title="Nach Beschreibung sortieren">Beschreibung</th> -->
        <th scope="col" title="Nach Genre sortieren">Genre</th>
        <!-- <th scope="col" title="Nach Track Anzahl sortieren">Track Anzahl</th> -->
        <th scope="col" title="Nach Preis sortieren">Preis in €</th>
        <th scope="col" title="Nach Anzahl im Lager sortieren">Anzahl</th>
        <th scope="col"></th>
      </tr>
    </thead>

    <tbody>

    <? foreach($order['content'] as $product) : ?>

      <tr data-role="base">
        <td><?= $product['product_id']; ?></td>
        <td><a href="/admin/product?product_id=<?= $product['product_id']; ?>&view=edit"><?= $product['name']; ?></a></td>
        <td><a href="/admin/artist?artist_id=<?= $product['artist_id']; ?>&view=edit"><?= $product['artist_name']; ?></a></td>
        <!-- <td><?= StringHelper::cut($product['description'], 30); ?></td> -->
        <td><a href="/admin/category?category_id=<?= $product['category_id']; ?>&view=edit"><?= ucfirst($product['category_name']); ?></a></td>
        <!-- <td><?= $product['tracks']; ?></td> -->
        <td><?= $product['price']; ?></td>
        <td><?= $product['amount']; ?></td>
        <td>
          <div class="dropdown">
            <a class="dropdown-toggle cursor-pointer"
               data-toggle="dropdown">
              <i class="<?= FA_STYLE ?> fa-chevron-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item"
                 href="/admin/product?product_id=<?= $product['product_id']; ?>&view=edit">
                <i class="<?= FA_STYLE ?> fa-edit mr-2"></i>Bearbeiten</a>
              <a class="dropdown-item"
                 href="/admin/product?product_id=<?= $product['product_id']; ?>&action=delete">
                <i class="<?= FA_STYLE ?> fa-times mr-2"></i>Löschen</a>
            </div>
          </div>
        </td>
      </tr>

      <? endforeach; ?>

    </tbody>

  </table>
</div>
