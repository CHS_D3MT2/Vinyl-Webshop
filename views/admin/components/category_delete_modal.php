<div class="modal fade" id="loaded_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <form action="/admin/product?action=change_category_id&category_id=<?= $category_id; ?>&redirect=<?= urlencode($redirect); ?>" method="post">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Hinweis</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          <p>
            Durch das löschen dieses Genres werden <?= $amount; ?> Produkte nicht mehr im Shop angezeigt.
          </p>
          <p>
            Um das zu verhindern, wähle hier das Genre unter welchem die betroffenen Produkte in Zukunft angezeigt werden sollen.
          </p>
          
          <p>
            Falls kein passendes Genre existiert kannst du ein neues erstellen.
          </p>
          
          <div class="form-group">
            
            <div class="custom-control custom-radio">
              <input checked type="radio" id="customRadio1" name="category_option" class="custom-control-input" value="existing">
              <label class="custom-control-label" for="customRadio1">Genre wählen</label>
            </div>
            <div class="custom-control custom-radio">
              <input type="radio" id="customRadio2" name="category_option" class="custom-control-input" value="new">
              <label class="custom-control-label" for="customRadio2">Genre erstellen</label>
            </div>
            
          </div>
          
          <div id="choose_category">
            <label>Genre wählen</label>
            <select data-type="product_category" class="custom-select" name="new_category_id">

              <? foreach ($categories as $category) : ?>

              <option value="<?= $category['category_id']; ?>"><?= ucfirst($category['category_name']); ?></option>

              <? endforeach; ?>

            </select>
          </div>
          
          <div id="create_new_category" class="hidden">
            <label>Genre erstellen</label>
            <input type="text" class="form-control" placeholder="Genre-Name" name="category_name">
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
          <button type="submit" class="btn btn-primary">Speichern</button>
        </div>
      </form>
    </div>
  </div>
</div>