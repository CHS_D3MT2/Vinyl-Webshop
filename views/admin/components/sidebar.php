<button class="btn btn-primary btn-block btn-lg"
        data-action="form_submit"
        data-form="#content_form">
  Speichern
</button>

<a class="btn btn-secondary btn-block"
   href="/admin/product?view=all">
  Abbrechen
</a>
