<div class="list-group h-100">

  <a class="list-group-item list-group-item-action" data-toggle="collapse" href="#collapse_users" role="button">
    <i class="<?= FA_STYLE ?> fa-user mr-2"></i> Users
  </a>
  <div class="collapse-show collapse show" id="collapse_users">
    <div class="list-group">
      <a href="/admin/user?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
    </div>
  </div>

  <a class="list-group-item list-group-item-action" data-toggle="collapse" href="#collapse_products" role="button">
    <i class="<?= FA_STYLE ?> fa-shopping-cart mr-2"></i> Produkte
  </a>
  <div class="collapse-show collapse show" id="collapse_products">
    <div class="list-group">
      <a href="/admin/product?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
      <a href="/admin/product?view=new" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Neues Produkt
      </a>
    </div>
  </div>

  <a class="list-group-item list-group-item-action" data-toggle="collapse" href="#collapse_category" role="button">
    <i class="<?= FA_STYLE ?> fa-list mr-2"></i> Genres
  </a>
  <div class="collapse-show collapse show" id="collapse_category">
    <div class="list-group">
      <a href="/admin/category?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
      <a href="/admin/category?view=new" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Neues Genre
      </a>
    </div>
  </div>

  <a class="list-group-item list-group-item-action " data-toggle="collapse" href="#collapse_artist" role="button">
    <i class="<?= FA_STYLE ?> fa-list mr-2"></i> Künstler
  </a>
  <div class="collapse-show collapse show" id="collapse_artist">
    <div class="list-group">
      <a href="/admin/artist?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
      <a href="/admin/artist?view=new" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Neuer Künstler
      </a>
    </div>
  </div>

  <a class="list-group-item list-group-item-action " data-toggle="collapse" href="#collapse_order" role="button">
    <i class="<?= FA_STYLE ?> fa-list mr-2"></i> Bestellungen
  </a>
  <div class="collapse-show collapse show" id="collapse_order">
    <div class="list-group">
      <a href="/admin/order?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
    </div>
  </div>

  <a class="list-group-item list-group-item-action " data-toggle="collapse" href="#collapse_review" role="button">
    <i class="<?= FA_STYLE ?> fa-list mr-2"></i> Bewertungen
  </a>
  <div class="collapse-show collapse show" id="collapse_review">
    <div class="list-group">
      <a href="/admin/review?view=all" class="list-group-item list-group-item-action">
        <i class="<?= FA_STYLE ?> fa-angle-right pl-4 mr-2"></i> Übersicht
      </a>
    </div>
  </div>

</div>
