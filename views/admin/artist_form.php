<form method="post"
      action="/admin/artist<?= (isset($form_params) ? '?'.$form_params : ''); ?>"
      enctype="multipart/form-data"
      class="w-100"
      id="content_form">

  <div class="col-md-12">

    <div class="form-row">

      <div class="col-md-4">

        <div class="form-group">

          <label for="name">Künstlername</label>
          <input type="text" class="form-control" id="name"
                 name="artist[name]"
                 value="<?= (isset($artist) ? $artist['name'] : ''); ?>">

        </div>

      </div>

      <div class="col-md-4">

        <div class="form-group">

          <label for="first_name">Vorname</label>
          <input type="text" class="form-control" id="first_name"
                 name="artist[first_name]"
                 value="<?= (isset($artist) ? $artist['first_name'] : ''); ?>">

        </div>

      </div>

      <div class="col-md-4">

        <div class="form-group">

          <label for="last_name">Nachname</label>
          <input type="text" class="form-control" id="last_name"
                 name="artist[last_name]"
                 value="<?= (isset($artist) ? $artist['last_name'] : ''); ?>">

        </div>

      </div>

      <div class="form-group col">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="customFile" name="artist_images[]" multiple>
          <label class="custom-file-label" for="customFile">Bild des Artists</label>
        </div>
      </div>

      <? if (isset($artist) && !empty($artist['image_url'])) : ?>

      <div class="col-sm-12">
        <img src="<?= ARTIST_IMAGE.$artist['image_url']; ?>" class="image-preview"/>
      </div>

      <? endif; ?>

    </div>

  </div>

</form>
