<? include(COMPONENT.'steps.php'); ?>

<form id="address_form" action="/checkout?action=set_address" method="POST">

  <div class="row">

    <div class="col-md-6">

        <h2 class="mb-4">Rechnungsadresse</h2>

        <?

        $address_is_same = false;

        foreach ($addresses as $key => $address) :

          if ($address['is_main_payment'] == 1 && $address['is_main_shipping'] == 1) {
            $address_is_same = true;
            $address_id = $address['address_id'];
          }

          ?>

          <div class="address select-radio" data-role="base">

            <div class="custom-control custom-radio big">
              <input
                type="radio"
                id="payment_address_<?= $key; ?>"
                name="address[payment_address]"
                value="<?= $address['address_id']; ?>"
                class="custom-control-input"
                <?= (isset($address['is_main_payment']) && $address['is_main_payment'] == 1 ? 'checked=""' : ''); ?>>

              <label class="custom-control-label cursor-pointer w-100" for="payment_address_<?= $key; ?>">

                <div>
                  <p><?= $address['first_name']." ".$address['last_name']; ?></p>
                  <?= (isset($address['company']) ? '<p>'.$address['company'].'</p>' : ''); ?>
                  <p><?= $address['country']; ?></p>
                  <p><?= $address['city']." ".$address['plz']; ?></p>
                  <p><?= $address['street']." ".$address['housenumber']; ?></p>
                  <!-- <?= (isset($address['phone']) ? '<p>'.$address['phone'].'</p>' : ''); ?> -->
                </div>

              </label>

            </div>

          </div>

        <? endforeach; ?>

    </div>

    <div class="col-md-6">

      <h2 class="mb-4">Lieferadresse</h2>

        <?

        if ($address_is_same) : ?>

        <div class="address select-radio" data-role="base">

          <div class="custom-control custom-radio big">
            <input
              type="radio"
              id="payment_address_same"
              name="address[shipping_address]"
              value="<?= $address_id; ?>"
              class="custom-control-input"
              checked="">

            <label class="custom-control-label cursor-pointer w-100" for="payment_address_same">Selbe wie Rechnungsadresse</label>

          </div>
        </div>

        <?

        endif;

        foreach ($addresses as $key => $address) :

          if (isset($address_id) && $address_id == $address['address_id']) {
            continue;
          }

        ?>

          <div class="address select-radio" data-role="base">

            <div class="custom-control custom-radio big">
              <input
                type="radio"
                id="shipping_address_<?= $key; ?>"
                name="address[shipping_address]"
                value="<?= $address['address_id']; ?>"
                class="custom-control-input"
                <?= (isset($address['is_main_payment']) && $address['is_main_payment'] == 1 ? 'checked=""' : ''); ?>>

                <label class="custom-control-label cursor-pointer w-100" for="shipping_address_<?= $key; ?>">

                  <div>
                    <p><?= $address['first_name']." ".$address['last_name']; ?></p>
                    <?= (isset($address['company']) ? '<p>'.$address['company'].'</p>' : ''); ?>
                    <p><?= $address['country']; ?></p>
                    <p><?= $address['city']." ".$address['plz']; ?></p>
                    <p><?= $address['street']." ".$address['housenumber']; ?></p>
                    <!-- <?= (isset($address['phone']) ? '<p>'.$address['phone'].'</p>' : ''); ?> -->
                  </div>

                </label>

            </div>

            <!-- <a
              class="cursor-pointer"
              data-submit="ajax"
              data-url="account"
              data-action="delete_address"
              data-success="remove_base"
              data-params="address_id=<?= $address['address_id']; ?>">
              <i class="<?= FA_STYLE ?> fa-trash"></i>
            </a> -->

          </div>

        <? endforeach; ?>

    </div>

  </div>

</form>

<div class="row">

  <div class="col-sm-12 mt-5">

    <div class="d-flex justify-content-between">
      <button class="btn btn-secondary mt-2"  data-toggle="collapse" data-target="#new_address">
        Neue Adresse hinzufügen
      </button>

      <a data-action="form_submit" data-form="#address_form" class="btn btn-primary mt-2">
        Weiter
      </a>
    </div>

    <div class="collapse mt-3" id="new_address">
      <div>
        <? (new FormBuilder)->load_template('address')->build(); ?>
      </div>
    </div>

  </div>

</div>
