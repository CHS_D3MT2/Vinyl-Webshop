<? include(COMPONENT.'steps.php'); ?>

<div class="row">

  <div class="col-lg-7 col-md-6 col-12">
    <h3 class="mb-4">Warenkorb</h3>

    <? foreach ($cart_items as $key => $product) :

    $product_id = $product['product_id'];

      ?>

    <div class="row cart-item" data-role="base">
      <div class="col-4">
        <a href="/product/<?= $product['slug']; ?>">
          <figure class="m-0">
            <img class="w-100" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
          </figure>
        </a>
      </div>
      <div class="col-8">

        <a class="product-title d-block h3" href="/product/<?= $product['slug'] ?>">
          <?= $product['name']; ?>
        </a>
        <a class="product-artist d-block" href="/artist/<?= $product['artist_slug'] ?>">
          <?= $product['artist_name']; ?>
        </a>

        <div class="mt-3">
          <span class="h4"><?= $product['price']; ?></span>
          <span class="h4"> €</span>
        </div>

        <div class="mt-2">
          Menge: <?= $cart[$product_id]['amount']; ?>
        </div>

      </div>

    </div>

    <?= ($key < count($cart_items) - 1 ? '<div class="my-4"><hr /></div>' : ''); ?>

    <? endforeach; ?>

  </div>

  <div class="col-lg-5 col-md-6 col-12">

    <div>

      <div class="mb-5">
        <h3>Lieferadresse</h3>
        <p><?= $shipping_address['first_name']." ".$shipping_address['last_name']; ?></p>
        <?= (isset($shipping_address['company']) ? '<p>'.$shipping_address['company'].'</p>' : ''); ?>
        <p><?= $shipping_address['street']." ".$shipping_address['housenumber']; ?></p>
        <p><?= $shipping_address['city']." ".$shipping_address['plz']; ?></p>
        <p><?= $shipping_address['country']; ?></p>
        <!-- <?= (isset($shipping_address['phone']) ? '<p>'.$shipping_address['phone'].'</p>' : ''); ?> -->
      </div>

      <div class="mb-5">
        <h3>Rechnungsadresse</h3>
        <p><?= $payment_address['first_name']." ".$payment_address['last_name']; ?></p>
        <?= (isset($payment_address['company']) ? '<p>'.$payment_address['company'].'</p>' : ''); ?>
        <p><?= $payment_address['street']." ".$payment_address['housenumber']; ?></p>
        <p><?= $payment_address['city']." ".$payment_address['plz']; ?></p>
        <p><?= $payment_address['country']; ?></p>
        <!-- <?= (isset($payment_address['phone']) ? '<p>'.$payment_address['phone'].'</p>' : ''); ?> -->
      </div>

      <div class="mb-5">
        <h3>Zahlung</h3>

      <? if ($payment['type'] == 'bank_transfer') : ?>

        <p>Lastschrift</p>
        <p>Zahlungsinstitut: <?= $payment['institute']; ?></p>
        <p>IBAN: <?= $payment['iban']; ?></p>
        <p>BIC: <?= $payment['bic']; ?></p>

      <? else : ?>

        <p>Kreditkarte</p>
        <p>Anbieter: <?= $payment['provider']; ?></p>
        <p>Kartennummer: <?= $payment['card_number']; ?></p>
        <p>Ablaufdatum: <?= date("m/Y", strtotime($payment['expiration_date'])); ?></p>

      <? endif; ?>
      </div>

      <div class="mb-5">
        <h3>Lieferung</h3>
        <p><?= $shipping['company']." ".$shipping['name']; ?></p>
        <p>Preis für deinen Standort: <?= $shipping['price']; ?>€</p>
      </div>

    </div>

    <div class="mt-5 mb-3 p-3 light-bg">
      <p>
        Lieferkosten: <?= $shipping['price']; ?>€
      </p>
      <h4 class="mt-2">Gesamtsumme: <span class="main-color"><?= $cart_stats['price'] + $shipping['price']; ?>€</span></h4>

      <a href="/checkout?action=buy" class="btn btn-primary btn-block btn-lg mt-4">
        Kaufen
      </a>
    </div>

  </div>

</div>
