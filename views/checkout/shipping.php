<div class="row">
  <div class="col-12">

    <form method="POST" action="/checkout?action=set_shipping">

<? include(COMPONENT.'steps.php'); ?>

<? foreach ($shipping_methods as $key => $sm) : ?>

  <div class="address select-radio" data-role="base">

    <div class="custom-control custom-radio big">
      <input
        type="radio"
        id="shipping_<?= $key; ?>"
        name="shipping[shipping_code]"
        value="<?= $sm['code']; ?>"
        class="custom-control-input"
        <?= ($key === 0 ? 'checked=""' : ''); ?>>

      <label class="custom-control-label cursor-pointer w-100" for="shipping_<?= $key; ?>">

        <div>
          <p><?= $sm['company']." ".$sm['name']; ?></p>
          <p>Preis für deinen Standort: <?= $sm['price']; ?>€</p>
        </div>

      </label>

    </div>

  </div>

<? endforeach; ?>

    <div class="d-flex justify-content-end">
      <button type="submit" class="btn btn-primary mt-2">
        Weiter
      </button>
    </div>

    </form>

  </div>
</div>
