<h1 class="main-color mt-4">Vielen Dank!</h1>
<h4>Deine Bestellung mit der ID #<?= $order_id; ?> war erfolgreich.</h4>
<h4>Du findest eine Bestellbestätigung in deinem Email-Postfach.</h4>

<a href="/" class="btn btn-primary btn-lg mt-4">
  Zur Startseite
</a>
