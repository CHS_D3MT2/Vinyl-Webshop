<div id="checkout-step-nav">

<div class="step <?= ($position == 1 ? 'active' : ($position > 1 ? 'visited' : '')) ?>">
  <div class="icon-container">
    <div class="icon">
      <span><?= ($position > 1 ? '<i class="'.FA_STYLE.' fa-check"></i>' : '1'); ?></span>
    </div>
  </div>
  <div class="step-name">
    <a>Anmelden</a>
  </div>
</div>

<div class="step <?= ($position == 2 ? 'active' : ($position > 2 ? 'visited' : '')) ?>">
  <a href="/checkout/address" class="icon-container">
    <div class="icon">
      <span><?= ($position > 2 ? '<i class="'.FA_STYLE.' fa-check"></i>' : '2'); ?></span>
    </div>
  </a>
  <div class="step-name">
    <a href="/checkout/address">Adresse</a>
  </div>
</div>

<div class="step <?= ($position == 3 ? 'active' : ($position > 3 ? 'visited' : '')) ?>">
  <a href="/checkout/payment" class="icon-container">
    <div class="icon">
      <span><?= ($position > 3 ? '<i class="'.FA_STYLE.' fa-check"></i>' : '3'); ?></span>
    </div>
  </a>
  <div class="step-name">
    <a href="/checkout/payment">Zahlung</a>
  </div>
</div>

<div class="step <?= ($position == 4 ? 'active' : ($position > 4 ? 'visited' : '')) ?>">
  <a href="/checkout/shipping" class="icon-container">
    <div class="icon">
      <span><?= ($position > 4 ? '<i class="'.FA_STYLE.' fa-check"></i>' : '4'); ?></span>
    </div>
  </a>
  <div class="step-name">
    <a href="/checkout/shipping">Lieferung</a>
  </div>
</div>

<div class="step <?= ($position == 5 ? 'active' : ($position > 5 ? 'visited' : '')) ?>">
  <a href="/checkout/confirm" class="icon-container">
    <div class="icon">
      <span><?= ($position > 5 ? '<i class="'.FA_STYLE.' fa-check"></i>' : '5'); ?></span>
    </div>
  </a>
  <div class="step-name">
    <a href="/checkout/confirm">Bestätigen</a>
  </div>
</div>


</div>
