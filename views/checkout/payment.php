<? include(COMPONENT.'steps.php'); ?>

<ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-bank-tab" data-toggle="pill" href="#pills-bank" role="tab">SEPA-Lastschrift</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-credit-tab" data-toggle="pill" href="#pills-credit" role="tab">Kreditkarte</a>
  </li>
</ul>

<form id="payment_form" method="POST" action="/checkout?action=set_payment">

<div class="tab-content" id="pills-tabContent">

  <div class="tab-pane show active" id="pills-bank" role="tabpanel">

    <div>

    <? foreach ($bank_transfers as $key => $bt) : ?>

      <div class="address select-radio" data-role="base">

        <div class="custom-control custom-radio big">
          <input
            type="radio"
            id="payment_bt_<?= $key; ?>"
            name="payment[payment_id]"
            value="<?= $bt['payment_id']; ?>"
            class="custom-control-input"
            <?= ($key === 0 ? 'checked=""' : ''); ?>>

          <label class="custom-control-label cursor-pointer w-100" for="payment_bt_<?= $key; ?>">

            <div>
              <p>IBAN: <?= $bt['iban']; ?></p>
              <p>BIC: <?= $bt['bic']; ?></p>
            </div>

          </label>

        </div>

      </div>

    <? endforeach; ?>

    </div>
    <div class="d-flex justify-content-between">
      <a class="btn btn-secondary mt-2"  data-toggle="collapse" data-target="#new_bank_transfer" role="button" data-action="close_others">
        Neue Bankverbindung hinzufügen
      </a>

      <button type="submit" class="btn btn-primary mt-2">
        Weiter
      </button>
    </div>

  </div>

  <div class="tab-pane" id="pills-credit" role="tabpanel">

    <div>

    <? foreach ($credit_cards as $key => $cc) : ?>

      <div class="address select-radio" data-role="base">

        <div class="custom-control custom-radio big">
          <input
            type="radio"
            id="payment_cc_<?= $key; ?>"
            name="payment[payment_id]"
            value="<?= $cc['payment_id']; ?>"
            class="custom-control-input">

          <label class="custom-control-label cursor-pointer w-100" for="payment_cc_<?= $key; ?>">

            <div>
              <p>Anbieter: <?= $cc['provider']; ?></p>
              <p>Kartennummer: <?= $cc['card_number']; ?></p>
              <p>Ablaufdatum: <?= date("m/Y", strtotime($cc['expiration_date'])); ?></p>
            </div>

          </label>

        </div>

      </div>

    <? endforeach; ?>

    </div>

    <div class="d-flex justify-content-between">
      <a class="btn btn-secondary mt-2" data-toggle="collapse" data-target="#new_credit_card" role="button" data-action="close_others">
        Neue Kreditkarte hinzufügen
      </a>

      <button type="submit" class="btn btn-primary mt-2">
        Weiter
      </button>
    </div>

  </div>

</div>

</form>

<div class="collapse mt-3" id="new_credit_card">

  <? (new FormBuilder)->load_template('credit_card')->build(); ?>

</div>

<div class="collapse mt-3" id="new_bank_transfer">

  <? (new FormBuilder)->load_template('bank_transfer')->build(); ?>

</div>
