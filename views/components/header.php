<!DOCTYPE html>
<html lang="de">
<head>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="/assets/css/main.css" rel="stylesheet">
  <title><?= (isset($page_title) ? $page_title." - " : "").SHOP_NAME; ?></title>
</head>
<body>
