<nav class="navbar navbar-expand-lg navbar-dark sticky-top">

  <div class="<?= $this->nav_container; ?>">

    <a class="navbar-brand" href="<?= BASE_URL; ?>"><?= SHOP_NAME; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>">Home</a>
        </li> -->

        <li class="nav-item" id="genre-link">
          <a data-no-link class="nav-link">Genres</a>
        </li>

        <? if ($is_admin) : ?>

        <li class="nav-item">
          <a class="nav-link" href="/admin">Admin-Panel</a>
        </li>

        <!-- <li class="nav-item">
          <a class="nav-link ml-auto" href="/debug?action=clear_session_cart">Session Cart leeren</a>
        </li> -->

        <? endif; ?>
      </ul>

      <form id="search-bar" class="form-inline mx-auto">
        <input class="form-control" type="search" placeholder="Suchen">
        <div id="search-result">

        </div>
      </form>

      <ul class="navbar-nav ml-auto">

      <? if ($is_logged_in) : ?>


        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
            <i class="<?= FA_STYLE ?> fa-user-circle fa-fw mr-2"></i><?= $user['first_name']." ".$user['last_name']; ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="/account"><i class="<?= FA_STYLE ?> fa-cog fa-fw mr-2"></i> Account</a>
            <a class="dropdown-item" href="/account/orders"><i class="<?= FA_STYLE ?> fa-truck fa-fw mr-2"></i> Bestellungen</a>
            <a class="dropdown-item" href="/wishlist"><i class="<?= FA_STYLE ?> fa-book-heart fa-fw mr-2"></i> Wunschliste</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/auth?action=logout"><i class="<?= FA_STYLE ?> fa-sign-out fa-fw mr-2"></i> Ausloggen</a>
          </div>
        </li>

        <? else : ?>

        <li class="nav-item">
          <a href="" class="nav-link ml-auto" data-toggle="modal" data-target="#auth_modal">
            Anmelden
          </a>
        </li>
      <? endif; ?>

        <li class="nav-item ml-3">
          <a href="/cart" class="nav-link" id="cart">
            <i class="<?= FA_STYLE ?> fa-shopping-cart mr-2"></i> (<span id="cart_amount"><?= (isset($cart_stats) && isset($cart_stats['amount']) ? $cart_stats['amount'] : '0'); ?></span>) <span id="cart_price"><?= (isset($cart_stats) ? number_format($cart_stats['price'], 2) : '0,00'); ?></span>€
          </a>
        </li>

      </ul>




    </div>

    <div id="genre-menu" class="container">
      <div class="row">
        <div class="col-xl-8 col-lg-7 col-md-6 content-wrapper">
          <div class="content">
            <div class="row">

            <?

            $col_count = 4;

            $genre_count = count($genres);

            $items_per_col = ceil($genre_count / $col_count);

            $col_size = 12 / $col_count;

            foreach ($genres as $key => $genre) :

              if ($key === 0 || $key % $items_per_col === 0) : ?>

              <div class="col-<?= $col_size; ?> content-col">

              <? endif; ?>

              <div class="genre-link">
                <a href="/genre/<?= $genre['slug']; ?>"><?= $genre['category_name']; ?></a>
              </div>

              <? if ($key % $items_per_col == $items_per_col - 1) : ?>

              </div>

              <? endif; ?>

            <? endforeach; ?>

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</nav>

<div class="modal fade" id="auth_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Anmelden</h5>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-login" role="tab">Anmelden</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-register" role="tab">Registrieren</a>
          </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">

          <div class="tab-pane fade show active" id="pills-login" role="tabpanel">

            <form action="/auth?action=login" method="post">

              <div class="form-group">
                <label>Email</label>
                <input type="email" required class="form-control" name="email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>

              <div class="form-group">
                <label>Passwort</label>
                <input type="password" required class="form-control" name="password">
              </div>

              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember_me" value="1">
                <label class="custom-control-label" for="customCheck1">Angemeldet bleiben</label>
              </div>

              <button type="submit" class="btn btn-primary d-block ml-auto mt-4">Anmelden</button>

            </form>

          </div>

          <div class="tab-pane fade" id="pills-register" role="tabpanel">

            <form action="/auth?action=register" method="post">

              <div class="form-row">

                <div class="col">
                  <label for="first_name">Vorname</label>
                  <input type="text" required class="form-control" id="first_name" name="first_name">
                </div>

                <div class="col">
                  <label for="last_name">Nachname</label>
                  <input type="text" required class="form-control" id="last_name" name="last_name">
                </div>

              </div>

              <div class="form-group mt-3">
                <label>Email-Adresse</label>
                <input type="email" required class="form-control" name="email">
              </div>

              <div class="form-row">

                <div class="col">
                  <label>Passwort</label>
                  <input type="password" required class="form-control" name="password">
                </div>

                <div class="col">
                  <label>Passwort bestätigen</label>
                  <input type="password" required class="form-control" name="password_repeat">
                </div>

              </div>

              <div class="form-group mt-3">

                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="gender_male" required name="gender" value="male" class="custom-control-input">
                  <label class="custom-control-label" for="gender_male">Männlich</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="gender_female" required name="gender" value="female" class="custom-control-input">
                  <label class="custom-control-label" for="gender_female">Weiblich</label>
                </div>

              </div>

              <div class="custom-control custom-checkbox mt-3">
                <input type="checkbox" class="custom-control-input" id="agb_check" required name="agb" value="1">
                <label class="custom-control-label" for="agb_check">Ich akzeptiere die AGB & Datenschutzerklärung.</label>
              </div>

              <button type="submit" class="btn btn-primary d-block ml-auto mt-4">Registrieren</button>

            </form>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
