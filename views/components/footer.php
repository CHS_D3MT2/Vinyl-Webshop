<footer id="site-footer">
  <div class="container">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link disabled" href="<?= BASE_URL; ?>">
          &copy; <?= date("Y"); ?> <?= SHOP_NAME; ?>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASE_URL; ?>page/imprint">
          Impressum
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASE_URL; ?>page/privacy-policy">
          Datenschutzerklärung
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASE_URL; ?>page/terms-of-service">
          AGB
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASE_URL; ?>page/contact">
          Kontakt
        </a>
      </li>
    </ul>
  </div>
</footer>
