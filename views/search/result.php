<div class="type">
  Alben
</div>

<div id="products" class="results">

<? if (!empty($products)) : ?>

<? foreach ($products as $product) : ?>

  <a class="product d-block" href="/product/<?= $product['slug']; ?>">
    <?= $product['name']." - ".$product['artist_name']; ?>
  </a>

<? endforeach; ?>

<? else : ?>

<div>
  Keine Ergebnisse gefunden
</div>

<? endif; ?>

</div>

<div class="type">
  Genres
</div>

<div id="grenre" class="results">

<? if (!empty($categories)) : ?>

<? foreach ($categories as $category) : ?>

  <a class="genre d-block" href="/genre/<?= $category['slug']; ?>">
    <?= $category['category_name']; ?>
  </a>

<? endforeach; ?>

<? else : ?>

<div>
  Keine Ergebnisse gefunden
</div>

<? endif; ?>

</div>

<div class="type">
  Künstler
</div>

<div id="artists" class="results">

<? if (!empty($artists)) : ?>

<? foreach ($artists as $artist) : ?>

  <a class="artist d-block" href="/artist/<?= $artist['slug']; ?>">
    <?= $artist['name']; ?>
  </a>

<? endforeach; ?>

<? else : ?>

<div>
  Keine Ergebnisse gefunden
</div>

<? endif; ?>

</div>

<div class="type">
  Tracks
</div>

<div id="tracks" class="results">

<? if (!empty($tracks)) : ?>

<? foreach ($tracks as $track) : ?>

<a class="track d-block" href="/product/<?= $track['slug']; ?>">
  <?= $track['name']; ?>
</a>

<? endforeach; ?>

<? else : ?>

<div>
  Keine Ergebnisse gefunden
</div>

<? endif; ?>

</div>
