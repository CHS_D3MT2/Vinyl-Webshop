<?php

include(GLOBAL_COMPONENT . 'header.php');
include(GLOBAL_COMPONENT . $this->nav.'.php');

include(GLOBAL_COMPONENT . 'loader.php');

?>

<div id="wrapper" class="<?= (isset($this->content_container) ? $this->content_container : $this->container); ?>">
  <?php include($template); ?>
</div>

<?php

include(GLOBAL_COMPONENT . 'notification.php');

if ($this->show_footer) {
  include(GLOBAL_COMPONENT . 'footer.php');
}

?>

  <script src="/assets/js/vendor/jquery.min.js"></script>
  <script src="/assets/js/vendor/tablesorter.min.js"></script>
  <script src="/assets/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="/assets/js/vendor/swiper.min.js"></script>
  <script src="/assets/js/vendor/dragula.min.js"></script>
  <script src="/assets/js/main.js"></script>

<? if (!is_null($notification)) : ?>

<script>

  let code = '<?= $notification['code']; ?>';
  let type = '<?= $notification['type']; ?>';

  setTimeout(function() {
    trigger_notification(code, type, 5000);
  }, 2000);

</script>

<? endif; ?>


  </body>
</html>
