<div class="row d-flex">
  <div class="col-xl-8 col-lg-7 col-md-6 col-12">
    <div class="form-row d-flex align-items-stretch">
    <div class="col-12">

    <? foreach($products_by_category as $category) : ?>

      <span class="h2 genre-title"><?= $category['category_name']; ?></span>

      <!-- Slider main container -->
      <div class="swiper-container">

        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
          <!-- Slides -->
          <? foreach($category['products'] as $product) : ?>

            <div class="swiper-slide product-preview">

              <div class="card" data-role="base" data-product-id="<?= $product['product_id']; ?>">
                <form>
                  <input type="hidden" name="product_id" value="<?= $product['product_id']; ?>"/>
                  <input type="hidden" name="price" value="<?= $product['price']; ?>"/>
                  <div class="card-img-container">
                    <? if($product['stock'] < 5): ?>
                      <span class="badge badge-warning">Nur noch wenig auf Lager.</span>
                    <? endif; ?>
                    <figure>
                      <a class="product-title d-block" href="/product/<?= $product['slug']; ?>">
                        <img class="card-img-top" src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>">
                      </a>
                    </figure>
                    <div class="card-actions">
                      <div>
                        <button
                          class="btn"
                          type="button"
                          data-url="cart"
                          data-action="add_item"
                          data-submit="ajax"
                          data-success='update_cart_preview'
                        >
                          <i class="<?= FA_STYLE ?> fa-cart-plus"></i>
                        </button>
                        <button
                          class="btn"
                          type="button"
                          data-url="wishlist"
                          data-action="add_to_wishlist"
                          data-submit="ajax"
                        >
                          <i class="<?= FA_STYLE ?> fa-plus"></i>
                        </button>
                      </div>
                      <p class="price align-self-center product-price"><?= $product['price']; ?>€</p>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="card-text">
                      <a class="product-title d-block" href="/product/<?= $product['slug'] ?>">
                        <?= $product['name']; ?>
                      </a>
                      <a class="product-artist d-block" href="/artist/<?= $product['artist_slug'] ?>">
                        <?= $product['artist_name']; ?>
                      </a>
                    </div>
                  </div>
                </form>
              </div>

            </div>

        <?
        endforeach; ?>

        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev">
          <i class="<?= FA_STYLE ?> fa-chevron-left"></i>
        </div>
        <div class="swiper-button-next">
          <i class="<?= FA_STYLE ?> fa-chevron-right"></i>
        </div>
      </div>

    <? endforeach; ?>

    </div>

    </div>
  </div>

  <div class="col-xl-4 col-lg-5 col-md-6 col-12 ranking-col">

    <div id="ranking-box">

      <h2>
        Top 10 - Verkäufe
      </h2>

    <?

    $place = 1;

    foreach($most_bought_products as $product) :

    ?>

      <div class="product">

        <div class="number">
          <span><?= $place; ?></span>
        </div>

        <div class="cover d-md-none d-lg-block">
          <a class="product-title d-block" href="/product/<?= $product['slug']; ?>">
            <img src="<?= (isset($product['image_url']) ? PRODUCT_IMAGE.$product['image_url'] : COVER_PLACEHOLD); ?>"/>
          </a>
        </div>

        <div class="product-text">

          <a class="product-title d-block" href="/product/<?= $product['slug']; ?>">
            <?= $product['name']; ?>
          </a>

          <a class="product-artist d-block" href="/artist/<?= $product['artist_id']; ?>">
            <?= $product['artist_name']; ?>
          </a>

        </div>

      </div>

    <?

    $place++;

    endforeach;

    ?>
    </div>

  </div>
</div>

<? //Debug::print($products, false); ?>
