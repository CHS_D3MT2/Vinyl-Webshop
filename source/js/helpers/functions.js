
$.fn.exists = function () {
    return this.length !== 0;
}

function is_json(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
