// (() => {

  /*
    Funktion zum Handlen der Nutzer-Benachrichtigungen
  */

  const trigger_notification = function (code, type, duration = 3000) {
    const animationEnd = 'animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd';

    const anim_in = 'bounceIn';
    const anim_out = 'bounceOut';

    let notification;

    /*
      Definiere HTTP-Request-Parameter mit den in der Methode angegebenen Argumente
    */

    const $request = {
      type: 'POST',
      url: `/notification/${code}/${type}`,
    };

    /*
      Führe HTTP-Request mittels AJAX aus.
    */

    $.ajax($request)
      .done(function(data) {
      /*
        Gebe die Benachrichtigung innerhalb des HTML aus, wenn der Request erfolgreich war.
      */

        const $container = $('#notification-container');

        $container.append(data);
        notification = $container.find('.notification:last');
        notification.addClass(anim_in);

      }).fail(function() {
      /*
        Benachrichtige Benutzer, falls der Request fehlgeschlagen ist
      */

        const error = `<span>Verbindung zum Server fehlgeschlagen!</span>`;
        $(notification).html(error);

      });

    /*
      Blende Benachrichtigung nach einem bestimmten Zeitraum aus
    */
    setTimeout(function () {

      $(notification).addClass(anim_out);

      setTimeout(function () {
        const animationProperties = {
          height: 0,
          margin: 0,
          padding: 0,
        };

        $(notification).animate(animationProperties, 500, function () {
          $(notification).remove();
        });

      }, 1000);

    }, duration);
  }

// })();
