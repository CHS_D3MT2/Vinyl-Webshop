
// (() => {

$(function() {

  const $closeLink = $('[data-action="close_others"]');

  $closeLink.on('click', function() {
    let others = $(this).attr('data-toggle');

    $(`.${others}`).removeClass('show');
  });

});

// })();
