$(function() {

  // Ermöglicht die Produkt-Tabelle zu sortieren
  $("#product-list table").tablesorter();

  dragula([document.querySelector('#track_container')], {
    moves: function (el, container, handle) {
      return handle.classList.contains('handle');
    }
  });

  // Sendet ein Form ab
  $(document).on('click', '[data-action="form_submit"]', function () {
    var form_id = $(this).attr('data-form');
    $(form_id).submit();
  });

  // Modal anzeigen wenn eine Kategorie gelöscht wird und Produkte davon betroffen sind
  $(document).on('click', '[data-action="category_delete"]', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');

    $.ajax({
      url: '/admin/product',
      data: {
        action: 'get_amount_by_category_id',
        redirect: link,
        category_id: $(this).attr('data-category-id')
      }
    })
      .done(function (data) {
        if (!$.trim(data)) {
          window.location.replace(link);
        } else {
          $('#modal_area').html(data);
          $('#loaded_modal').modal('show');
        }
      })
  });

  $(document).on('change', 'input[name="category_option"]', function () {
    $('#create_new_category').toggleClass('hidden');
    $('#choose_category').toggleClass('hidden');
  });

  $(document).on('click', '[data-button="update_user_role"]', function (event) {
    event.preventDefault();
    $('input[name="user_id"]').val($(this).attr('data-user-id'));
  });

  $(document).on('click', '[data-action="add_track"]', function(event) {
    var new_track = $('#track_container [data-role="base"]:first').clone();
    $(new_track).find('input').val("");
    $(new_track).appendTo('#track_container');
  });

  $(document).on('click', '[data-action="delete_track"]', function(event) {
    var track_amount = $('#track_container .track').length;
    if (track_amount == 1) {
      return false;
    }
    $(this).closest('[data-role="base"').remove();
  });

});
