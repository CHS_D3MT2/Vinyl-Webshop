let search_result_open = false;

$(function () {

  $('body').on('click', function (e) {

    if ($(e.target).parents("#search-bar").length == 0) {
      if (search_result_open) {
        $('#search-result').fadeOut(100);
        search_result_open = false;
      }
    }

  })

  $('#search-bar input').on('click', function () {
    if (!search_result_open) {
      $('#search-result').fadeIn(100);
      search_result_open = true;
    }
  })

  $('#search-bar input').on('keyup', (function (event) {

    let text = $(this).val();

    if (text.length <= 2) {
      $('#search-result').empty();
    }

    if (text.length > 2) {

      search_result_open = true;

      $.ajax({
        type: "POST",
        url: '/search',
        data: {
          search_text: text
        }
      })
        .done(function (response) {
          $('#search-result').html(response);
        });
    }

  }));

})
