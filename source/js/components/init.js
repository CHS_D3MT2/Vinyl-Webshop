
// (() => {

$(function() {

  /*
    Definiere den Wrapper, der den Slider beinhalten soll mittels HTML-Klasse
  */
  const swiperContainer = '.swiper-container';
  /*
    Definiere Einstellungen, die für den Slider eingesetzt werden sollen
  */
  const swiperSettings = {
    slidesPerView: 4,
    slidesPerGroup: 4,
    spaceBetween: 10,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1300: {
        slidesPerView: 5,
        slidesPerGroup: 5
      },
      1024: {
        slidesPerView: 3,
        slidesPerGroup: 3
      },
      992: {
        slidesPerView: 2,
        slidesPerGroup: 2
      },
      320: {
        slidesPerView: 1,
        slidesPerGroup: 1
      }
    }
  };

  /*
    Initialisiere den Slider mit den definierten Einstellungen
  */
  const swiper = new Swiper(swiperContainer, swiperSettings);

});

// })();
