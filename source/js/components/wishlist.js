function update_wishlist_view()
{
  if ($('.wishlist-item').length < 1) {
    $('#wrapper').append(`<div class="text-center mt-5">
                            <h3>Du hast noch keine Artikel zur Wunschliste hinzugefügt.</h3>
                            <a class="btn btn-primary mt-3" href="/">Zurück zum Shop</a>
                          </div>`);
  }
}
