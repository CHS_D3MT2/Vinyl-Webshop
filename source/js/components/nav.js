
// (() => {

$(function() {

  /*
    Setze Genre-Link Konstante
  */
  const $genreLink = $('#genre-link, #genre-menu .content');

  /*
    Handle das ein- oder ausblenden der Subnavi beim Hover auf den Genre-Link
  */
  $genreLink.hover(
    function(){ $("#genre-menu").show(); },
    function(){ $("#genre-menu").hide(); }
  );

});

// })();
