$(function() {

  $('.rating.editable .rating_star').hover(
    // Handles the mouseover
    function() {
      $(this).prevAll().addBack().addClass('fas').removeClass('far');
      $(this).nextAll().addClass('far').removeClass('fas');
    },
    // Handles the mouseout
    function() {
      $(this).prevAll().addBack().addClass('far').removeClass('fas');
    }
  );

  $('.rating.editable .rating_star').on('click',function() {
    $('.rating.editable .rating_star').unbind('mouseenter mouseleave');
    $(this).prevAll().addBack().addClass('fas').removeClass('far');
    $(this).nextAll().addClass('far').removeClass('fas');
    $(this).closest('[data-role="base"]').find('.rating-input').val($('[data-type="review_score"] .fas').length);
  });

});
