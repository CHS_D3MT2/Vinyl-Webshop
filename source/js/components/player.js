$(function() {

  $(document).on('click', '[data-action="play-track"]', function() {

    $(this).attr('data-action', 'stop-track');
    $(this).find('i').addClass('fa-stop').removeClass('fa-play');

    $(this).closest('[data-role="base"]').addClass('active-playing');

    let $iframe = $(this).closest('[data-role="base"]').find('iframe');

    $iframe.addClass('active-playing');
    var videoURL = $iframe.prop('src');
    videoURL += "?autoplay=1";
    $iframe.prop('src',videoURL);

  })

  $(document).on('click', '[data-action="stop-track"]', function() {

    $(this).attr('data-action', 'play-track');
    $(this).find('i').addClass('fa-play').removeClass('fa-stop');

    $(this).closest('[data-role="base"]').removeClass('active-playing');

    let $iframe = $(this).closest('[data-role="base"]').find('iframe');

    var videoURL = $iframe.prop('src');
    videoURL = videoURL.replace("?autoplay=1", "");
    $iframe.prop('src','');
    $iframe.prop('src',videoURL);

  })

})
