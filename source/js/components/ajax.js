let $button;
let $this;

$(function () {

  function ajaxHandler(element) {

    $button = $(element);
    const $form = $('' + $button.attr('data-form') + '').exists() ? $('' + $button.attr('data-form') + '') : $button.closest('form');
    let url = $button.attr('data-url');
    const dataAction = $button.attr('data-action');
    const dataParams = $button.attr('data-params') || '';

    const notification_code = url + '_' + dataAction;

    if (dataParams) {
      url = `/${url}?action=${dataAction}&${dataParams}`;
    } else {
      url = `/${url}?action=${dataAction}`;
    }

    const onSuccess = $button.data('success');
    const onFail = $button.data('fail');
    const onAlways = $button.data('always');


    $.ajax({
      type: "POST",
      url: url,
      data: $form.serialize(),
    })
      .always(function (response) {
        if (onAlways) {
          run_functions(onAlways);
        }
      })
      .done(function (response) {

        if (is_json(response)) {
          let data = JSON.parse(response);
          trigger_notification(data.code, data.type);

          if (data.status == 'success') {
            if (onSuccess) {
              run_functions(onSuccess);
            }
          } else {
            if (onFail) {
              run_functions(onFail);
            }
          }

        } else {
          trigger_notification(notification_code, 'success');
          if (onSuccess) {
            run_functions(onSuccess);
          }
        }
      })
      .fail(function (response) {
        trigger_notification(notification_code, 'error');
        if (onFail) {
          run_functions(onFail);
        }
      })
  }

  const $document = $(document);

  $document.on('click', '[data-submit="ajax"]', function () {
    $this = $(this);
    ajaxHandler(this);
  });

  $document.on('change', '[data-change-submit="ajax"]', function () {
    ajaxHandler(this);
  });

  function remove_base() {
    $this.closest('[data-role="base"]').remove();
  }

  function run_functions(array) {
    if (Array.isArray(array)) {
      var index;
      for (index = 0; index < array.length; ++index) {
        eval(array[index])();
      }
    } else {
      eval(array)();
    }
  }

});
