
(() => {

  /*
    Erstelle eine Funktion die entweder den Seiten-Preloader ein- oder ausblendet, abhängig davon ob direction == in oder == out ist.
  */

  const loader = function (container, direction, timeout, speed) {
    const $container = $(container);

    if (direction == 'out') {
      setTimeout(() => {
        $container.fadeOut(speed);
      }, timeout);
    } else if (direction == 'in') {
      $container.fadeIn(speed);
    }
  }

  /*
    Rufe loader-funktion bei Pageload aus
  */

  loader('.loader_container', 'out', 1500, 300);

  /*
    Führe eingehende loader-Funktion beim unloaden des Fensters der um zwischen Seiten überzublenden
  */

  window.onbeforeunload = function () {
    loader('.loader_container', 'in', 1500, 300);
  }

})();
