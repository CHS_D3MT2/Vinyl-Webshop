$(function() {
  /*
    Führe update_cart_preview-Funktion nur bei einem Change-Event auf dem Dokument aus
  */
  $(document).on('change', '[data-type="item-amount"]', function() {
    update_cart_stats();
  })

  $('.no-send').on('submit', function() {
    return false;
  })

});

/**
 * Überprüft ob noch Produkte im Warenkorb sind.
 * Blendet eine Meldung ein wenn keine mehr vorhanden sind.
 */
function update_cart_view()
{
  if ($('.cart-item').length < 1) {
    $('#cart-stats').remove();
    $('#wrapper').append(`<div class="text-center mt-5">
                            <h3>Du hast noch keine Artikel zum Warenkorb hinzugefügt.</h3>
                            <a class="btn btn-primary mt-3" href="/">Zurück zum Shop</a>
                          </div>`);
  }
}

/**
 * Aktualisiert den Preis und die Anzahl der Warenkorb-Anzeige
 */
function update_cart_preview()
{
  const total_amount = parseInt($('#cart_amount').text()) || 0;
  $('#cart_amount').text(total_amount + 1);
  const total_price = parseFloat($('#cart_price').text()) || 0.00;

  const prev = $this.closest('[data-role="base"]').find('input[name="price"]').val();

  const item_price = parseFloat(prev) || 0.00;
  $('#cart_price').text(parseFloat(total_price + item_price).toFixed(2));
}

/**
 * Aktualisiert den Preis und die Anzahl auf der Warenkorb-Seite
 */
function update_cart_stats() {

  let total_price = 0.00;
  let total_amount = 0;

  $('[data-type="item-amount"]').each(function(i, obj) {

    let price = parseFloat($(obj).attr('data-price'));
    let amount = parseInt($(obj).val());

    total_amount = total_amount + amount;
    total_price = total_price + (price * amount);
  });

  total_price = total_price.toFixed(2);

  $('#cart_total_price').text(total_price);
  $('#cart_total_amount').text(total_amount);

  $('#cart_price').text(total_price);
  $('#cart_amount').text(total_amount);

}
