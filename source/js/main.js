/*
  Importiere sämtliche Javascript-Komponente für den Compiler, welcher dann aus allen Files eine einzige Datei generiert, um die Anzahl der Requests zu minimieren.
*/

//@prepros-prepend helpers/functions.js
//@prepros-append components/init.js
//@prepros-append components/notification.js
//@prepros-append components/admin.js
//@prepros-append components/cart.js
//@prepros-append components/ajax.js
//@prepros-append components/nav.js
//@prepros-append components/loader.js
//@prepros-append components/search.js
//@prepros-append components/player.js
//@prepros-append components/actions.js
//@prepros-append components/rating.js
//@prepros-append components/wishlist.js
