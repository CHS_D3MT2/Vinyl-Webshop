<?

/**
 * Zuständig für den Mailversand.
 */
class Mail extends BaseModel {

  /**
   * Betreff.
   * @var string
   */
  protected $subject;

  /**
   * Begrüßung.
   * @var string
   */
  protected $greeting;

  /**
   * Mail-Inhalt
   * @var string
   */
  protected $message_body;

  /**
   * Wird beim Instantiieren des Models aufgerufen.
   * @return void
   */
  public function init()
  {
    if (LOCALHOST) {
      return false;
    }
    $this->greeting = 'Hallo '. $this->user_data['first_name'].',';
  }

  /**
   * Sendet eine Bestellbestätigung.
   * @param  integer $order_id Bestell-ID
   * @return void
   */
  public function send_order_confirm($order_id) {

    $this->subject = 'Bestellbestätigung';

    $this->message_body =
      $this->greeting . '
    Du hast am ' . date("d.m.Y") . ' um ' . date("H:i:s") . ' eine Bestellung aufgegeben.
    Die Bestell-ID lautet: '.$order_id;

    mail($this->user_data['email'], $this->subject, $this->message_body);

  }


}
