<?

/**
 * Konfiguration der Webapp
 */
abstract class Config
{
  protected static $pascal = false;

  /**
   * Wird aktell eine lokale Datenbank (xampp/wamp/etc.) verwendet?
   * @var boolean
   */
  protected static $local_db = false;

  /**
   * Aliase für Contoller um schönere bzw. individuelle URLs nutzen zu können
   * @var Array
   */
  protected static $alias_map = [
    'genre' => 'category'
  ];

  /**
   * Initiert die Konfiguration
   * @return void
   */
  public static function init()
  {
    //ini_set('session.cookie_domain', 'sp-webshop.exocreations.de');

    require_once(ROOT.'app/lib/Autoloader.php');

    ini_set('default_socket_timeout', 1000);
    ini_set('max_execution_time', 1000);
    ini_set('mysql.connect_timeout', 1000);

    self::define_constants();
  }

  /**
   * Definiert wichtige Konstanten für den späteren Gebrauch
   * @return void
   */
  public static function define_constants()
  {
    define('PROJECT_TITLE', 'schulprojekt_webshop');

    define('SHOP_NAME', 'RecordBay');

    define('BASE_URL', 'http://record-bay.exocreations.de/');

    if (self::$pascal) {
      define('BASE_URL', 'http://localhost/');
    } else {
      define('BASE_URL', 'http://localhost:8888/');
    }

    define('APP_ROOT', ROOT.'app/');
    define('CONTROLLER_ROOT', APP_ROOT.'controller/');
    define('LIB_ROOT', APP_ROOT.'lib/');
    define('MODEL_ROOT', APP_ROOT.'model/');
    define('VIEW_ROOT', ROOT.'views/');
    define('GLOBAL_COMPONENT', ROOT.'views/components/');

    if (self::$local_db) {
      define('DB_HOST', 'localhost:8889');
      define('DB_NAME', 'vinyl-webshop');
      define('DB_PASS', 'root');
      define('DB_USER', 'root');
    } else {
      define('DB_HOST', 'exocreations.de');
      define('DB_NAME', 'd02985cf');
      define('DB_PASS', '6qkYM6BWHzBBYw9Y');
      define('DB_USER', 'd02985cf');
    }

    define('DB_PORT', '3306');
    define('DB_CHARSET', 'utf8mb4');

    define('FA_STYLE', 'fal');

    define('DB_TABLE_AUTH', 'auth');
    define('DB_TABLE_USER', 'user');
    define('DB_TABLE_ADDRESS', 'address');
    define('DB_TABLE_PRODUCT', 'product');
    define('DB_TABLE_REVIEW', 'product_review');
    define('DB_TABLE_CATEGORY', 'product_category');
    define('DB_TABLE_TRACK', 'product_track');
    define('DB_TABLE_BT', 'bank_transfer');
    define('DB_TABLE_CC', 'credit_card');
    define('DB_TABLE_PAYMENT', 'payment');
    define('DB_TABLE_SHIPPING', 'shipping');
    define('DB_TABLE_ORDER', 'orders');
    define('DB_TABLE_ORDER_CONTENT', 'order_content');
    define('DB_TABLE_ARTIST', 'product_artist');
    define('DB_TABLE_WISHLIST', 'user_wishlist');
    define('DB_TABLE_IMAGE', 'product_image');
    define('PRODUCT_IMAGE', '/storage/app/public/uploads/images/product/');
    define('DB_TABLE_ARTIST_IMAGE', 'product_artist_image');
    define('ARTIST_IMAGE', '/storage/app/public/uploads/images/artist/');
    define('COVER_PLACEHOLD', '/assets/img/cover-placeholder.png');
  }

  /**
   * Gibt das Alias für einen Namen zurück
   * @param  string $name Name
   * @return string       Alias
   */
  public static function get_alias($name)
  {
    if (array_key_exists($name, self::$alias_map)) {
      return self::$alias_map[$name];
    } else {
      return 'index';
    }
  }


}
