<?php

/**
 * Helper-Klasse um ohne HTML ein Formular aufzubauen.
 */
class FormBuilder
{
  protected $autocomplete = true;

  protected $spellcheck = false;

  protected $csrf_token = null;

  protected $build_buttons = false;

  protected $accept_files = false;

  protected $none_required = false;

  protected $templates = [
    'address' => [
      'attributes' => [
        'id' => 'new_address_form',
        'action' => '/account?action=add_address',
        'input_group' => 'address'
      ],
      'options' => [
        'build_buttons' => true
      ],
      'layout' => '2 1 3 2 1 1 1',
      'inputs' => [
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Vorname',
          'name' => 'first_name'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Nachname',
          'name' => 'last_name'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Unternehmen',
          'name' => 'company',
          'required' => false
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Land',
          'name' => 'country'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Stadt',
          'name' => 'city'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Postleitzahl',
          'name' => 'plz'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Straße',
          'name' => 'street'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Hausnummer',
          'name' => 'housenumber'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Telefon',
          'name' => 'phone',
          'required' => false
        ],
        [
          'input_type' => 'tick',
          'type' => 'checkbox',
          'label' => 'Standard-Rechnungsadresse',
          'name' => 'is_main_payment',
          'required' => false
        ],
        [
          'input_type' => 'tick',
          'type' => 'checkbox',
          'label' => 'Standard-Lieferadresse',
          'name' => 'is_main_shipping',
          'required' => false
        ]
      ],
      'buttons' => [
        [
          'text' => 'Speichern',
          'attributes' => [
            'data-action' => 'form_submit',
            'class' => 'btn btn-primary'
          ]
        ]
      ]
    ],
    'bank_transfer' => [
      'attributes' => [
        'id' => 'new_bank_transfer_form',
        'action' => '/account?action=add_bank_transfer',
        'input_group' => 'bank_transfer'
      ],
      'options' => [
        'build_buttons' => true
      ],
      'layout' => '1 1 1',
      'inputs' => [
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Zahlungsinstitut',
          'name' => 'institute'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'IBAN',
          'name' => 'iban'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'BIC',
          'name' => 'bic'
        ]
      ],
      'buttons' => [
        [
          'text' => 'Speichern',
          'attributes' => [
            'type' => 'submit',
            'class' => 'btn btn-primary'
          ]
        ]
      ]
    ],
    'credit_card' => [
      'attributes' => [
        'id' => 'new_credit_card_form',
        'action' => '/account?action=add_credit_card',
        'input_group' => 'credit_card'
      ],
      'options' => [
        'build_buttons' => true
      ],
      'layout' => '2 2',
      'inputs' => [
        [
          'input_type' => 'select',
          'label' => 'Anbieter',
          'name' => 'provider',
          'selected' => 'mastercard',
          'options' => [
            [
              'value' => 'Mastercard',
              'text' => 'Mastercard'
            ],
            [
              'value' => 'Visa',
              'text' => 'Visa'
            ],
            [
              'value' => 'american_express',
              'text' => 'American Express'
            ]
          ]
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Kartennummer',
          'name' => 'card_number'
        ],
        [
          'input_type' => 'textbox',
          'type' => 'text',
          'label' => 'Sicherheitscode',
          'name' => 'security_code'
        ],
        [
          'input_type' => 'month',
          'label' => 'Ablaufdatum',
          'name' => 'expiration_date'
        ]
      ],
      'buttons' => [
        [
          'text' => 'Speichern',
          'attributes' => [
            'data-action' => 'form_submit',
            'class' => 'btn btn-primary'
          ]
        ]
      ]
    ]
  ];

  /**
   * Legt die Inputfelder fest.
   * @param  Array  $inputs Inputfelder
   * @return FormBuilder
   */
  public function inputs(Array $inputs)
  {
    $this->inputs = $inputs;

    return $this;
  }

  /**
   * Legt einen CSRF-Token fest.
   * @param  string $token CSRF-Token
   * @return void
   */
  public function csrf_token($token)
  {
    $this->csrf_token = $token;
  }

  /**
   * Legt das Layout des Formulars fest.
   * @param  string $layout Anzahl der Spalten pro Reihe
   * @return FormBuilder
   */
  public function layout($layout = "2 3 4")
  {
    $this->layout = explode(" ", $layout);

    return $this;
  }

  /**
   * Legt die Attribute des Formulars fest.
   * @param  array $attr Attribute des Formulars
   * @return FormBuilder
   */
  public function attributes($attr)
  {
    $this->id = $attr['id'];
    $this->action = $attr['action'];
    $this->input_group = $attr['input_group'];

    return $this;
  }

  /**
   * Legt die Optionen des Formulars fest.
   * @param  array $attr Optionen des Formulars
   * @return FormBuilder
   */
  public function options($opt)
  {
    foreach($opt as $key => $value) {
      if (isset($opt[$key])) {
        $this->$key = $value;
      }
    }

    return $this;
  }

  /**
   * Legt die Buttons des Formulars fest.
   * @param  array $attr Buttons des Formulars
   * @return FormBuilder
   */
  public function buttons($array)
  {
    $this->buttons = $array;

    return $this;
  }

  /**
   * Baut das HTML-Formular zusammen und gibt es aus.
   * @return void
   */
  public function build()
  {
    ?>

    <form action="<?= $this->action; ?>"
          method="POST" id="<?= $this->id; ?>"
          autocomplete="<?= ($this->autocomplete ? 'on' : 'off'); ?>"
          <?= ($this->accept_files == true ? 'enctype="multipart/form-data"' : ''); ?>>

      <?

      echo isset($this->csrf_token) ? '<input type="hidden" name="_csrf" value="'.$this->csrf_token.'">' : '';

      $counter = 0;

      $index = 1;

      $layout_pos = 0;

      if (!isset($this->layout)) {
        $this->layout = str_repeat("1", count($this->inputs));
      }

      foreach ($this->inputs as $input) :

        $id = (isset($input['id']) ? $input['id'] : (isset($input['name']) ? $input['name']."_".(new Generate)->secure_random_string(6)."_".$counter : (new Generate)->secure_random_string(6)."_".$counter));
        $placeholder = (isset($input['placeholder']) ? $input['placeholder'] : (isset($input['label']) ? $input['label'] : ''));
        $name = (isset($input['name']) ? $this->input_group."[".$input['name']."]" : null);
        $is_required = (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? true : false) : false);

        if (!isset($this->layout[$layout_pos])) {
          $layout_pos = 0;
        }

        ?>

      <? if ($index == 1 || !isset($this->layout)) : ?>

      <div class="form-row <?= ($input['input_type'] == 'hidden' ? 'm-0 p-0 h-0' : ''); ?>">

      <? endif; ?>

        <div class="form-group col-md-<?= (isset($this->layout) ? 12 / $this->layout[$layout_pos] : '12'); ?>">

          <? if ($input['input_type'] == 'textbox') : ?>

          <?= (isset($input['label']) ? '<label for="'.$id.'">'.$input['label'].($is_required ? '<span class="required-star">*</span>' : '').'</label>' : '' ); ?>

          <input id="<?= $id; ?>"
                 class="form-control"
                 type="<?= $input['type']; ?>"
                 placeholder="<?= $placeholder; ?>"
                 name="<?= $name; ?>"
                 spellcheck="<?= ($this->spellcheck == true ? 'true' : 'false'); ?>"
                 value="<?= (isset($input['value']) ? $input['value'] : ''); ?>"
                 <? if ($input['type'] == 'number') :

                    echo (isset($input['min']) ? 'min="'.$input['min'].'"' : '');
                    echo (isset($input['max']) ? 'max="'.$input['max'].'"' : '');
                    echo (isset($input['step']) ? 'step="'.$input['step'].'"' : '');

                  endif; ?>

            <?= (isset($input['maxlength']) ? 'maxlength="'.$input['maxlength'].'"' : ''); ?>
            <?= ($is_required ? 'required' : ''); ?>>

          <?= (isset($input['small']) ? '<small class="form-text text-muted">'.$input['small'].'</small>' : '' ); ?>

          <?= (isset($input['invalid_feedback']) ? '<div class="invalid-feedback">'.$input['invalid_feedback'].'</div>' : '' ); ?>
          <?= (isset($input['valid_feedback']) ? '<div class="valid-feedback">'.$input['valid_feedback'].'</div>' : '' ); ?>

          <? elseif ($input['input_type'] == 'textarea') : ?>

          <label for="<?= $id; ?>"><?= $input['label'].($is_required ? '<span class="required-star">*</span>' : ''); ?></label>

          <textarea id="<?= $id; ?>"
                    class="form-control"
                    placeholder="<?= $placeholder; ?>"
                    rows="<?= (isset($input['rows']) ? $input['rows'] : 3); ?>"
                    name="<?= $name; ?>"
                    spellcheck="<?= ($this->spellcheck == true ? 'true' : 'false'); ?>"
                    value="<?= (isset($input['value']) ? $input['value'] : ''); ?>"
            <?= (isset($input['maxlength']) ? 'maxlength="'.$input['maxlength'].'"' : ''); ?>
            <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>></textarea>

          <?= (isset($input['small']) ? '<small class="form-text text-muted">'.$input['small'].'</small>' : '' ); ?>

          <?= (isset($input['invalid_feedback']) ? '<div class="invalid-feedback">'.$input['invalid_feedback'].'</div>' : '' ); ?>
          <?= (isset($input['valid_feedback']) ? '<div class="valid-feedback">'.$input['valid_feedback'].'</div>' : '' ); ?>

          <? elseif ($input['input_type'] == 'tick') : ?>

          <div class="custom-control custom-<?= $input['type']; ?>">

            <input id="<?= $id; ?>"
                   type="<?= $input['type']; ?>"
                   class="custom-control-input"
                   name="<?= $name; ?>"
                   <?= (isset($input['checked']) && $input['checked'] == true ? 'checked=""' : ''); ?>
                   <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>
                   value="<?= (isset($input['value']) ? $input['value'] : '1'); ?>">

            <label class="custom-control-label" for="<?= $id; ?>"><?= $input['label'].($is_required ? '<span class="required-star">*</span>' : ''); ?></label>

            <?= (isset($input['invalid_feedback']) ? '<div class="invalid-feedback">'.$input['invalid_feedback'].'</div>' : '' ); ?>
            <?= (isset($input['valid_feedback']) ? '<div class="valid-feedback">'.$input['valid_feedback'].'</div>' : '' ); ?>

          </div>

          <?= (isset($input['small']) ? '<small class="form-text text-muted">'.$input['small'].'</small>' : '' ); ?>

          <? elseif ($input['input_type'] == 'select') : ?>

          <label for="<?= $id; ?>"><?= $input['label'].($is_required ? '<span class="required-star">*</span>' : ''); ?></label>

          <select id="<?= $id; ?>"
                  class="custom-select"
                  spellcheck="<?= ($this->spellcheck == true ? 'true' : 'false'); ?>"
                  name="<?= $name; ?>"
                  <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>>

            <?= (!isset($input['selected']) ? '<option selected value="">'.$placeholder.'</option>' : ''); ?>

            <? foreach ($input['options'] as $option) : ?>

            <option <?= (isset($input['selected']) && $option['value'] == $input['selected'] ? 'selected' : ''); ?> value="<?= $option['value']; ?>"><?= $option['text']; ?></option>

            <? endforeach; ?>

          </select>

          <?= (isset($input['small']) ? '<small class="form-text text-muted">'.$input['small'].'</small>' : '' ); ?>

          <?= (isset($input['invalid_feedback']) ? '<div class="invalid-feedback">'.$input['invalid_feedback'].'</div>' : '' ); ?>
          <?= (isset($input['valid_feedback']) ? '<div class="valid-feedback">'.$input['valid_feedback'].'</div>' : '' ); ?>

          <? elseif ($input['input_type'] == 'file') : ?>

          <div class="custom-file c_pointer">

            <input type="file"
                   class="custom-file-input c_pointer"
                   id="<?= $id; ?>"
                   name="<?= $input['name']; ?>[]"
                   <?= (isset($input['multiple']) && $input['multiple'] == true ? 'multiple="true"' : ''); ?>
                   <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>
                   <?
                   if (isset($input['custom'])) {
                      foreach($input['custom'] as $key => $value) {
                        echo $key.'="'.$value.'"';
                      }
                   }
                   ?>/>

            <label class="custom-file-label c_pointer" for="<?= $id; ?>"><?= $input['label'].($is_required ? '<span class="required-star">*</span>' : ''); ?></label>

            <?= (isset($input['small']) ? '<small class="form-text text-muted">'.$input['small'].'</small>' : '' ); ?>

            <?= (isset($input['invalid_feedback']) ? '<div class="invalid-feedback">'.$input['invalid_feedback'].'</div>' : '' ); ?>
            <?= (isset($input['valid_feedback']) ? '<div class="valid-feedback">'.$input['valid_feedback'].'</div>' : '' ); ?>

          </div>

          <? elseif ($input['input_type'] == 'date') : ?>

          <?= (isset($input['label']) ? '<label for="'.$id.'">'.$input['label'].($is_required ? '<span class="required-star">*</span>' : '').'</label>' : '' ); ?>

          <input id="<?= $id; ?>"
                 class="form-control"
                 type="date"
                 name="<?= $name; ?>"
                 <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>/>

          <? elseif ($input['input_type'] == 'month') : ?>

          <?= (isset($input['label']) ? '<label for="'.$id.'">'.$input['label'].($is_required ? '<span class="required-star">*</span>' : '').'</label>' : '' ); ?>

          <input id="<?= $id; ?>"
                class="form-control"
                type="month"
                name="<?= $name; ?>"
                <?= (!isset($this->none_required) || !$this->none_required ? (!isset($input['required']) || $input['required'] == true ? 'required' : '') : ''); ?>/>

          <? elseif ($input['input_type'] == 'hidden') : ?>

          <input id="<?= $id; ?>"
                type="hidden"
                name="<?= $name; ?>"
                value="<?= (isset($input['value']) ? $input['value'] : ''); ?>"/>

          <? elseif ($input['input_type'] == 'container') : ?>

          <div id="<?= $input['id']; ?>" class="row">
          </div>

          <? endif; ?>

        </div>

      <? if (!isset($this->layout) || $index == $this->layout[$layout_pos]) : ?>

      </div>

      <? endif; ?>

      <?

      if ($index == $this->layout[$layout_pos]) {

        $index = 1;
        $layout_pos++;

      } else {

        $index++;

      }

      $counter++;

    endforeach; ?>

    <p class="required-note">
      Mit * markierte Felder sind Pflichtfelder
    </p>

    <? if (isset($this->buttons) && $this->build_buttons) :

        $this->build_buttons();

      endif;

      ?>

    </form>


    <?
  }

  /**
   * Baut die Buttons des Formulars.
   * @return void
   */
  public function build_buttons()
  {
    foreach ($this->buttons as $button) : ?>

    <button <? foreach ($button['attributes'] as $key => $value) {
      echo $key.'="'.$value.'" ';
    } ?>>
      <?= $button['text']; ?>
    </button>

    <? endforeach;
  }

  /**
   * Lädt ein festgelegtes Template.
   * @param  string $name Name des Templates
   * @return FormBuilder
   */
  public function load_template($name)
  {
    if (array_key_exists($name, $this->templates)) {

      $this->attributes($this->templates[$name]['attributes']);
      $this->options($this->templates[$name]['options']);
      $this->layout($this->templates[$name]['layout']);
      $this->inputs($this->templates[$name]['inputs']);
      $this->buttons($this->templates[$name]['buttons']);

    }

    return $this;
  }
}
