<?

/**
 * Helper-Klasse für Arrays.
 */
abstract class ArrayHelper
{

  /**
   * Überprüft ob ein Array leere Werte oder Werte bestehend aus Leerzeichen besitzt
   *
   * @param  Array   $array  Array, welches auf leere Werte überprüft werden soll
   * @param  Array  $ignore  Felder, die ignoriert werden sollen
   * @return boolean         Array besitzt leere Werte
   */
  public static function has_empty_values(Array $array, Array $ignore = null)
  {
    foreach($array as $key => $value) {
      if ((empty($array[$key]) && !trim($array[$key]) && !in_array($key, $ignore)) || ctype_space($value)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Entfernt alle HTML-Tags aus Daten
   *
   * @param  Array/String $data Daten, die überprüft werden sollen
   * @return Array/String       Gesäuberte Daten
   */
  public static function clean_input($data)
  {
    if (is_array($data)) {

      $cleaned = [];

      foreach($data as $key => $value) {

        $cleaned[$key] = self::clean_input($value);

      }
    } else {

      $cleaned = strip_tags(html_entity_decode(trim($data)));

    }

    return $cleaned;
  }

  /**
   * Überprüft ob ein Datei-Input Dateien enthält
   *
   * @param  Array $file_input Array, das überprüft werden soll
   * @return boolean           File-Input enthält Dateien
   */
  public static function contains_files($file_input)
  {
    if ($file_input['size'][0] > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Überprüft ob ein Datei-Input mehr als eine Datei enthält
   *
   * @param  Array $file_input Array, das überprüft werden soll
   * @return boolean           File-Input enthält mehrere Dateien
   */
  public static function contains_multiple_files($array)
  {
    if (array_key_exists('name', $array)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Wandelt die komische Array-Struktur von einem Datei-Input, der das Attribut "multiple" besitzt in eine bessere Struktur um
   * @param  Array $file Datei-Array welches umgewandelt werden soll
   * @return Array       umgewandeltes Datei-Array
   */
  public static function convert_upload_array($file)
  {
    $file_ary = array();
    $file_count = count($file['name']);
    $file_key = array_keys($file);

    for($i=0;$i<$file_count;$i++)
    {
        foreach($file_key as $val)
        {
            $file_ary[$i][$val] = $file[$val][$i];
        }
    }
    return $file_ary;
  }

}
