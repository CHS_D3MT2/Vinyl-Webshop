<?

/**
 * Helper-Klasse für alles was mit Datum und Zeit zu tun hat.
 */
class DateTimeHelper
{

  /**
   * Liefert das aktuelle Datum im MYSQL-Format
   * @return string Formatierte Datums-Zeichenkette
   */
  public static function get_mysql_date()
  {
    return date("Y-m-d H:i:s");
  }

  /**
   * Konvertiert eine Datumsangabe in das MYSQL-Format
   * @param $date   Zu konvertierendes Datum
   * @return string Formatierte Datums-Zeichenkette
   */
  public static function to_mysql_date($date)
  {
    return date("Y-m-d", strtotime($date));
  }

  /**
   * Konvertiert ein Datum in ein schönes Datums-Format
   * @param  string $date Zu konvertierendes Datum
   * @return string       Formatierte Datums-Zeichenkette
   */
  public static function to_nice_date($date)
  {
    return date("d.m.Y", strtotime($date));
  }

  /**
   * Konvertiert eine Uhrzeit in ein schönes Uhrzeit-Format
   * @param  string $date Zu konvertierendes Datum
   * @return string          Formatierte Datums-Zeichenkette
   */
  public static function to_nice_time($date)
  {
    return date("H:i:s", strtotime($date));
  }

  /**
   * Konvertiert ein komplettes Datum in ein schönes Datums-Format
   * @param  string $date Zu konvertierendes Datum
   * @return string       Formatierte Datums-Zeichenkette
   */
  public static function to_nice_datetime($date)
  {
    return date("d.m.Y H:i:s", strtotime($date));
  }

  /**
   * Konvertiert ein Datum in das Format für HTML-Date-Input-Felder
   * @param  string $date Zu konvertierendes Datum
   * @return string       Formatierte Datums-Zeichenkette
   */
  public static function to_html_date($date)
  {
    return date("Y-m-d", strtotime($date));
  }

  /**
   * Konvertiert eine Zeitangabe in Minuten.
   * @param  string $date Zu konvertierende Zeit
   * @return string       Formatierte Zeit-Zeichenkette
   */
  public static function to_minutes($date)
  {
    return date("i:s", strtotime($date));
  }

}
