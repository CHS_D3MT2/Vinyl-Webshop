<?

/**
 * Ist für die Interaktion mit der Session zuständig.
 */
class Session
{

  /**
   * Startet die Session und setzt wichtige Parameter.
   * @return void
   */
  public static function init()
  {
    if (!LOCALHOST) {
      session_set_cookie_params(time() + (10 * 365 * 24 * 60 * 60), '/', 'sp-webshop.exocreations.de', true, false);
    }

    session_start();
  }

  /**
   * Schreibt einen Wert bzw. ein Array an eine Stelle in der Session.
   * @param  string $main_key An welcher Stelle die Daten gespeichert werden sollen.
   * @param  string|array|integer $data Daten, die gespeichert werden sollen.
   * @return Session Objekt um Funktionen zu verknüpfen.
   */
  public static function write($main_key, $data)
  {
    if (is_array($data)) {

      foreach ($data as $key => $value) {

        $_SESSION[$main_key][$key] = $value;

      }

    } else {

      $_SESSION[$main_key] = $data;

    }

    return new static;

  }

  /**
   * Liest den Inhalt an einer bestimmten Position in der Session.
   * @param  string $path  Pfad, von wo Daten gelesen werden sollen.
   * @param  string $token String, an welcher Position der Pfad getrennt werden soll
   * @return string|array|integer Daten an der angegebene Position in der Session
   */
  public static function read($path, $token = ".")
  {
    $current = $_SESSION;
    $p = strtok($path, $token);

    while ($p !== false) {
      if (!isset($current[$p])) {
        return null;
      }
      $current = $current[$p];
      $p = strtok($token);
    }

    return $current;
  }

  /**
   * Löscht eine bestimmt Position in der Session.
   * @param  string $main_key An welcher Stelle die Daten entfernt werden sollen.
   * @param  string $sub_key  An welcher unterposition die Daten entfernt werden sollen.
   * @return void
   */
  public static function clear($main_key, $sub_key = null)
  {
    if ($sub_key !== null) {
      unset($_SESSION[$main_key][$sub_key]);
    } else  {
      unset($_SESSION[$main_key]);
    }
  }

}
