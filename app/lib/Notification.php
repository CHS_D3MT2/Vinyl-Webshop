<?

/**
 * Bearbeitet Anfragen, die an das Benachrichtigungssystem gerichtet sind.
 */
class Notification
{
  /**
   * Typ der Meldung.
   * @var string
   */
  protected $type;

  /**
   * Standard-Code.
   * Wird genutzt falls der gesendete Code nicht existiert.
   * @var string
   */
  protected $default = 'save_data';

  /**
   * Enthält die Icons und Nachrichten für die verschiednen Meldungen.
   * @var array
   */
  protected $notification_map = [
    'cart_add_item' => [
      'success' => [
        'icon' => 'cart-arrow-down',
        'message' => 'In den Warenkorb gelegt.'
      ],
      'error' => [
        'icon' => 'times',
        'message' => 'Beim Hinzufügen des Produkts ist ein Fehler aufgetreten!'
      ]
    ],
    'cart_delete_item' => [
      'success' => [
        'icon' => 'trash',
        'message' => 'Produkt wurde aus deinem Warenkorb entfernt.'
      ],
      'error' => [
        'icon' => 'times',
        'message' => 'Beim Entfernen des Produkts ist ein Fehler aufgetreten!'
      ]
    ],
    'wishlist_delete_product' => [
      'success' => [
        'icon' => 'trash',
        'message' => 'Produkt wurde aus deiner Wunschliste entfernt.'
      ],
      'error' => [
        'icon' => 'times',
        'message' => 'Beim Entfernen des Produkts ist ein Fehler aufgetreten!'
      ]
    ],
    'wishlist_add_to_wishlist' => [
      'success' => [
        'icon' => 'heart',
        'message' => 'In deiner Wunschliste gespeichert.'
      ],
      'error' => [
        'icon' => 'times',
        'message' => 'Fehler beim Hinzufügen zu deiner Wunschliste'
      ]
    ],
    'save_data' => [
      'success' => [
        'icon' => 'check',
        'message' => 'Aktion erfolgreich!'
      ],
      'error' => [
        'icon' => 'times',
        'message' => 'Fehler beim Ausführen der Funktion!'
      ]
    ],
    'login' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Login fehlerhaft! Bitte überprüfe deine Eingaben.'
      ]
    ],
    'email_exists' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Diese Email existiert bereits.'
      ]
    ],
    'address_used_in_order' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Diese Adresse wurde bei einer Bestellung genutzt und kann nicht gelöscht werden.'
      ]
    ],
    'payment_used_in_order' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Diese Zahlungsart wurde bei einer Bestellung genutzt und kann nicht gelöscht werden.'
      ]
    ],
    'password_not_same' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Die eingegebenen Passwörter stimmen nicht überein. Bitte überprüfe deine Eingaben!'
      ]
    ],
    'not_enough_products' => [
      'error' => [
        'icon' => 'times',
        'message' => 'Diese Menge des Produktes ist leider nicht vorrätig.'
      ]
    ],
    'cart_update_amount' => [
      'success' => [
        'icon' => 'arrows-alt-v',
        'message' => 'Menge des Produkts wurde angepasst.'
      ]
    ]
  ];

  /**
   * Legt den Typ der Meldung fest.
   * @param string $type Typ der Meldung
   * @return Notification Instanz der Klasse um Funktionen zu verknüpfen
   */
  public function set_type($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Legt den Typ der Meldung fest.
   * @param string $code Code der Meldung
   */
  public function set_code($code)
  {
    if (!array_key_exists ($code, $this->notification_map)) {
      $this->code = $this->default;
    } else {
      $this->code = $code;
    }

    return $this;
  }

  /**
   * Rendert eine Meldung.
   * @return void
   */
  public function render()
  {
    ?>
    <div class="alert alert-<?= $this->type; ?> notification animated" role="alert">
      <i class="fas fa-<?= $this->notification_map[$this->code][$this->type]['icon']; ?> mr-3"></i><span><?= $this->notification_map[$this->code][$this->type]['message']; ?></span>
    </div>
  <?
  }

  /**
   * Speichert eine Meldung in der Session um sie beim nächsten Laden einer Seite anzuzeigen.
   * @param string $code Code der Meldung
   * @param string $type Typ der Meldung
   */
  public function set($code, $type)
  {
    Session::write('notification', ['code' => $code, 'type' => $type]);
  }

  /**
   * Lädt eine Meldung aus der Session und löscht sie danach.
   * @return array Daten der Meldung
   */
  public function get()
  {
    $var = Session::read('notification');
    Session::clear('notification');
    return $var;
  }

  /**
   * Sendet eine Meldung als JSON zurück, welche dann im Javascript verarbeitet wird.
   * @param  array $data Daten der Meldung
   * @return string JSON-String
   */
  public function send_back($data)
  {
    echo json_encode([
      'code' => $data['code'],
      'type' => $data['type']
    ]);
    die();
  }

}
