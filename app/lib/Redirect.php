<?

/**
 * Helper-Klasse für Weiterleitungen
 */
class Redirect {

  /**
   * Ort wohin weitergeleitet werden soll
   * @var String
   */
  protected $location = BASE_URL;

  /**
   * Setzt das Weiterleitungs-Ziel auf die Index-Seite
   * @return Redirect
   */
  public function to_index()
  {
    $this->location = BASE_URL;

    return $this;
  }

  /**
   * Setzt das Weiterleitungs-Ziel auf eine bestimmte URL
   * @param  string $page Ziel-URL
   * @return Redirect
   */
  public function to($page)
  {
    $page = str_replace(BASE_URL, '', $page);

    $this->location = BASE_URL.ltrim($page, "/");

    return $this;
  }

  /**
   * Setzt das Weiterleitungs-Ziel auf die URL von der die Anfrage kam
   * @return Redirect
   */
  public function to_origin()
  {
    if (!isset($_SERVER['HTTP_REFERER']) || is_null($_SERVER['HTTP_REFERER'])) {
      $this->to_index();
    } else {
      $path = str_replace(":8081", "", $_SERVER['HTTP_REFERER']);
      $this->location = $path;
    }

    return $this;
  }

  /**
   * Lädt die aktuelle Seite nach einer gewissen Zeit neu
   * @param  integer $timeout Dauer bis zum Reload
   * @return void
   */
  public function refresh($timeout = 0)
  {
    header("Refresh:".$timeout);
    die();
  }

  /**
   * Leitet den Nutzer nach einer gewissen Zeit an das Weiterleitungs-Ziel weiter
   * @param  integer $timeout Dauer bis zur Weiterleitung
   * @return void
   */
  public function go_after($timeout = 0)
  {
    header("refresh:".$timeout.";url=".$this->location);
    die();
  }

  /**
   * Leitet den Nutzer direkt an das Weiterleitungs-Ziel weiter
   * @return void
   */
  public function go()
  {
    header("location: ".$this->location);
    die();
  }

}
