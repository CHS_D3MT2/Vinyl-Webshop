<?

/**
 * Löst Anfragen auf und instantiiert den passenden Contoller.
 */
class Router
{
  /**
   * URL-Paramter
   * @var array
   */
  protected $params;

  /**
   * Alle empfangenen Daten.
   * @var array
   */
  protected $data = [];

  /**
   * Name der angeforderten Controller-Klasse
   * @var string
   */
  protected $controller_class_name;

  /**
   * Name des angeforderten Controllers.
   * @var string
   */
  protected $controller_name;

  /**
   * Spaltet die angefragte URL in ihre Bestandteile auf.
   * @param  string $url Angefragte URL
   * @return void
   */
  public function convert_url($url)
  {
    $url = parse_url($url);
    $url = $url['path'];
    $url = trim($url, "/");
    $this->params = explode('/', $url);
  }

  /**
   * Sammelt alle empfangenen Daten in einem Array
   * @param  array $data_array Empfangene Date
   * @return void
   */
  public function collect_data($data_array)
  {
    foreach ($data_array as $data) {

      if (isset($data) && is_array($data)) {

        $this->data = array_merge($this->data, $data);

      }

    }
  }

  /**
   * Findet den geforderten Controller.
   * @return void
   */
  public function map_controller()
  {
    $this->controller_name = (isset($this->params[0]) && $this->params[0] ? $this->params[0] : 'index');
    $this->controller_class_name = ucfirst($this->controller_name).'Controller';

    if (!class_exists($this->controller_class_name)) {

      $this->controller_name = Config::get_alias($this->controller_name);

      $this->controller_class_name = ucfirst($this->controller_name).'Controller';

    }
  }

  /**
   * Instantiiert den Controller.
   * @return mixed
   */
  public function build_controller()
  {
    $controller = $this->controller_class_name;
    return new $controller($this->params, $this->data);
  }

  /**
   * Instantiiert einen Unter-Controller.
   * @param  string $name           Name des Unter-Controllers
   * @param  BaseController $base   Instanz des Basis-Controllers
   * @param  string $prefix         Prefix für den Unter-Controller
   * @return mixed                  Controller
   */
  public function build_sub_controller($name, $base = null, $prefix = null)
  {
    $controller_name = (isset($prefix) ? ucfirst($prefix) : '').ucfirst($name);
    $controller_class = $controller_name."Controller";

    return new $controller_class($base);
  }

  /**
   * Gibt den Namen des Controllers zurück.
   * @return string Controller-Name
   */
  public function get_controller_name()
  {
    return $this->controller_name;
  }
}
