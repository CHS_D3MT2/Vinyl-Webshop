<?php

/**
 * Autoloader für alle genutzten Klassen um nicht alle genutzten Klassen includen zu müssen.
 *
 * @package Library;
 */
class Autoloader
{
  /**
   * Registriert die Autoloader-Funktionen.
   */
  public function __construct()
  {
    spl_autoload_register([$this, 'controller_autoload']);
    spl_autoload_register([$this, 'model_autoload']);
    spl_autoload_register([$this, 'lib_autoload']);
  }

  /**
   * Erstellt eine neue Instanz des Autoloaderss
   * @return Autoloader
   */
  public static function register()
  {
    new Autoloader();
  }

  /**
   * Autoloader-Funktion für Controller
   * @param  string $class Name, der zu ladenden Klasse
   * @return void
   */
  function controller_autoload($class)
  {
    $file = CONTROLLER_ROOT.$class.".php";

    if (file_exists($file)) {
      require($file);
    }
  }

  /**
   * Autoloader-Funktion für Library-Klassen und Helper
   * @param  string $class Name, der zu ladenden Klasse
   * @return void
   */
  function lib_autoload($class)
  {
    $file = LIB_ROOT.$class.".php";

    if (file_exists($file)) {
      require($file);
    }
  }

  /**
   * Autoloader-Funktion für ModelsKan
   * @param  string $class Name, der zu ladenden Klasse
   * @return void
   */
  function model_autoload($class)
  {
    $file = MODEL_ROOT.$class.".php";

    if (file_exists($file)) {
      require($file);
    }
  }

}
