<?

/**
 * Helper-Klasse für String-Funktuionen
 */
class StringHelper {

  /**
   * Schneidet einen String nach einer gewissen Anzahl an Chars ab und hängt standardmäßg "..." an
   * @param  string $string String der abgeschnitten werden soll
   * @param  integer $length Anzahl der Buchstaben nach denen geschnitten werden soll
    * @param  string $append String, der angehängt werden soll
   * @return string         Zugeschnittener String
   */
  public static function cut($string, $length, $append = '...')
  {
    $text_length = strlen($string);

    $string = substr($string, 0, $length).($text_length > $length ? $append : '');
    return $string;
  }

  /**
   * Wandelt einen String in einen "Slug" um. Das heißt es werden alle Umlaute umgewandelt und Leerzeichen bzw. nicht unterstützt Zeichen umgewandelt
   * @param  string $string    Umzuwandelnder String
   * @param  string $delimiter String der unerlaubte Zeichen ersetzt
   * @return string            Umgewandelter String
   */
  public static function to_slug($string, $delimiter = '-')
  {
    return strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $string))))), $delimiter));
  }

  /**
   * Alle HTML-Tags entfernen
   * @param  string $value zu säubernder String
   * @return string gesäuberter String
   */
  public static function escape_html($value)
  {
    echo htmlspecialchars($value, ENT_QUOTES);
  }

}
