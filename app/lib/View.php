<?

/**
 * Ist zuständig für das rendern/darstellen der views/templates/Seiten.
 */
class View {

  /**
   * CSS-Container-Klasse für den Inhaltsbereich.
   * @var string
   */
  protected $container = 'container';

  /**
   * CSS-Container-Klasse für die Navigationsleiste.
   * @var string
   */
  protected $nav_container = 'container';

  /**
   * Template-Datei welche für die Navigation genutzt wird.
   * @var string
   */
  protected $nav = 'nav';

  /**
   * Ob die Sidebar im Adminbereich angezeigt werden soll oder nicht.
   * @var boolean
   */
  public $show_sidebar = true;

  /**
   * Ob der Footer angezeigt wird.
   * @var boolean
   */
  protected $show_footer = true;

  /**
   * Template-Datei für die Sidebar im Adminbereich.
   * @var string
   */
  protected $sidebar = 'sidebar';

  /**
   * Wie das Template gerendert werden soll.
   *
   * 'basic': lädt alle Resourcen (.css, .js) und das 'base'-Layout.
   * 'partial': lädt nur das angegebene Template.
   *
   * @var string
   */
  protected $render_mode = 'basic';

  /**
   * Daten welche der Controller an die View übergeben hat und in den Templates zur Verfügung stehen.
   *
   * @var array
   */
  public $data;


  /**
   * Setzt template und directory auf den gegebenen Controller-Namen.
   *
   * @param string $controller_name Name des erzeugten Controllers.
   */
  public function __construct($controller_name)
  {
    $this->directory = $controller_name;
    $this->template = $this->directory;
  }

  /**
   * Gibt den Controllern die Möglichkeit das View-Objekt mit Daten zu befüllen.
   *
   * @param array $data Daten welche in der View zur Verfügung stehen sollen.
   */
  public function set_data($data)
  {
    foreach($data as $key => $val) {
      $this->data[$key] = $val;
    }
  }

  /**
   * Ändert den 'render_mode'.
   * @param string $name Name des Render-Typs.
   */
  public function set_render_mode($name)
  {
    $this->render_mode = $name;
  }

  /**
   * Deaktiviert die Sidebar im Adminbereich.
   * @return void
   */
  public function disable_sidebar()
  {
    $this->show_sidebar = false;
  }

  /**
   * Ändert das Template welches für die Sidebar im Adminbereich genutzt werden soll.
   * @param string $name Name des Templates.
   */
  public function set_sidebar($name)
  {
    $this->sidebar = $name;
  }

  /**
   * Deaktiviert den Footer.
   * @return void
   */
  public function disable_footer()
  {
    $this->show_footer = false;
  }

  /**
   * Ändert die CSS-Container-Klasse für den Inhaltsbereich und die Navigationsleiste.
   * @param string $container Name der CSS-Klasse welche der Inhaltsbereich und die Navigationsleiste erhalten soll.
   */
  public function set_container($container)
  {
    $this->container = $container;
  }

  /**
   * Ändert die CSS-Container-Klasse für den Inhaltsbereich.
   *
   * @param string $container Name der CSS-Klasse welche der Inhaltsbereich erhalten soll.
   */
  public function set_content_container($container)
  {
    $this->content_container = $container;
  }

  /**
   * Ändert die CSS-Container-Klasse für die Navigationsleiste
   *
   * @param string $container Name der CSS-Klasse welche die Navigationsleiste erhalten soll
   */
  public function set_nav_container($container)
  {
    $this->nav_container = $container;
  }

  /**
   * Setzt das Template welches für die Navigationsleiste verwendet werden soll.
   *
   * @param string $name Name des Templates
   */
  public function set_nav($name)
  {
    $this->nav = $name;
  }

  /**
   * Ändert das aktuelle 'view' Verzeichnis.
   * Nur die Templates im genutzten Verzeichnis können verwendet werden.
   *
   * @param string $name Pfad oder Name des Verzeichnisses.
   */
  public function set_directory($name)
  {
    $this->directory = $name;
  }

  /**
   * Setzt das Template welches gezeigt/gerendert werden soll.
   *
   *
   * @param string $name Name des Templates.
   */
  public function set_template($name)
  {
    $this->template = $name;
  }

  /**
   * Setzt das zu nutzende Template wenn ein Rahmentemplate genutzt wird.
   * Im Adminbereich ist das Template immer 'admin.php'.
   * Um die Darstellung zu ändern wird lediglich das zu ladenede Template geändert.
   * @param string $path Pfad/Name der zu nutzenden Template-Datei
   */
  public function set_include($path)
  {
    $this->include = $path;
  }

  /**
   * Rendert bzw. inkludiert das festgelegte Template um es im Browser anzuzeigen.
   * @return void
   */
  public function render()
  {
    extract($this->data);

    $template = VIEW_ROOT.$this->directory."/".$this->template.'.php';

    if ($this->render_mode == 'basic') {
      include(VIEW_ROOT.'layout'."/".'base.php');
    }
    else if ($this->render_mode == 'partial') {
      include($template);
    }
  }
}
