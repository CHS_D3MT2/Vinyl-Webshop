<?

/**
 * Enthält Funktionalitäten, welche das Debuggen des Codes erleichtern.
 */
class Debug {

  /**
   * Gibt allemöglichen Datentypen aus und stoppt standardmäßg das Ausführen des Scripts.
   * @param  mixed  $array Daten, die dargestellt werden sollen.
   * @param  boolean $die  Ausführen des Scripts nach Darstellung beenden.
   * @return mixed
   */
  public static function print($array, $die = true) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";

    if($die === true) {
      die();
    }

  }

}
