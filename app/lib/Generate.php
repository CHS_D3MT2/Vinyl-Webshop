<?

/**
 * Enthält Funktionalitäten für die Erstellung bzw. das Generieren von Daten
 */
class Generate {

  /**
   * Genereiert eine zufällige Zahlenkette mit definierter Länge
   * @param  integer $length Länge der Zahlenkette
   * @return integer         zufällige Zahlenkette
   */
  public function secure_random_numeric($length = 32)
  {
    return $this->secure_random_string($length, $chars = '0123456789');
  }

  /**
   * Generiert eine zufällige Zeichenkette mit definierter Länge und bestehend aus definierten Zeichen.
   * @param  integer $length Länge der Zeichenkette
   * @param  string  $chars  Zeichen, die genutzt werden sollen
   * @return string          zufällige Zeichenkette
   */
  public function secure_random_string($length = 32, $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
  {
    $chars = str_shuffle($chars);
    $random_string = '';
    $max = mb_strlen($chars, '8bit') - 1;

    for ($i = 0; $i < $length; ++$i) {
        $random_string .= $chars[random_int(0, $max)];
    }

    return $random_string;
  }

}
