<?

/**
 * Datenbank-Klasse für vereinfachte Datenbank-Interaktion
 */
class Database
{

  /**
   * Datenbank-Handler bzw. PDO-Objekt
   * @var PDO
   */
  private $dbh;

  /**
   * Enthält eventuelle auftretende Fehlermeldungen.
   * @var string
   */
  private $error;

  /**
   * Prepared Statements des PDO-Objekts
   * @var [type]
   */
  private $stmt;

  /**
   * Konstruktor, erstelt neues PDO-Objekt und stellt Verbindung zur Datenbank her.
   */
  public function __construct()
  {
    // PDO Konfiguration
    $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME. ';port=' . DB_PORT . ';charset=' . DB_CHARSET;

    // PDO Optionen
    $options = array(
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => true
    );

    // versucht neue PDO-Instanz zu erstellen
    try {
      $this->dbh = new PDO($dsn, DB_USER, DB_PASS, $options);
    }
    // alle Errors auffangen und ausgeben
    catch (Exception $e) {
      $this->error = $e->getMessage();
    }

  }

  /**
   * Bereitet ein SQL Statement vor.
   * @param  string $query SQL-Query
   * @return void
   */
  public function query($query){
    $this->stmt = $this->dbh->prepare($query);
  }

  /**
   * Variabeln an den jeweiligen Parameter binden.
   * Typ der Variable wird automatisch ermittelt.
   * @param  string $param Name des Parameters
   * @param  mixed $value  Wert, den der Paramter erhalten soll
   * @param  mixed $type   Typ des Werts
   * @return void
   */
  public function bind($param, $value, $type = null){
    if (is_null($type)) {
      switch (true) {
          case is_int($value):
              $type = PDO::PARAM_INT;
              break;
          case is_bool($value):
              $type = PDO::PARAM_BOOL;
              break;
          case is_null($value):
              $type = PDO::PARAM_NULL;
              break;
          default:
              $type = PDO::PARAM_STR;
      }
    }
    $this->stmt->bindValue($param, $value, $type);
  }

  /**
   * Führt das vorbereitete Statement aus.
   * @param  mixed $bind Werte, die an Parameter gebunden werden sollen
   * @return mixed Statement
   */
  public function execute($bind = null){
    return $this->stmt->execute($bind);
  }

  /**
   * Ergebnisse als assoziatives Array bekommen
   * @param array $bind Ergebnisse des SQL-Statements
   */
  public function resultset($bind = null){
    $this->execute($bind);
    return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Einzelne Zeile als assoziatives Array bekommen
   * @param  mixed $bind Werte, die an Parameter gebunden werden sollen
   * @return array Ergbnis
   */
  public function single($bind = null){
    $this->execute($bind);
    return $this->stmt->fetch(PDO::FETCH_ASSOC);
  }

  /**
   * Letzte, in der Datenbank erzeugte ID holen
   * @return [type] [description]
   */
  public function last_insert_id(){
    return $this->dbh->lastInsertId();
  }

  /**
   * Anzahl der Ergebnis-Reihen zurückgeben
   * @return integer Anzahl der Reihen
   */
  public function row_count(){
    return $this->stmt->rowCount();
  }

}
