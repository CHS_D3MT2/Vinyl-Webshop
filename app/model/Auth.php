<?

/**
 * Diese Klasse ist für die Authentifizierung der Nutzer und der Interaktion mit der 'auth'-Tabelle zuständig
 */
class Auth extends BaseModel
{
  public function init()
  {
    $this->user = new User();
  }

  /**
   * Setzt die user_id mit der die aktuelle Instanz der Klasse arbeitet.
   *
   * @param integer $id
   * @return Auth Aktuelle Instanz der Klasse um Funktionen zu verknüpfen
   */
  public function set_user_id($id)
  {
    $this->user_id = $id;

    return $this;
  }

  /**
   * Erstellt einen neuen Zungang für einen User
   */
  public function create()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_USER.' (first_name, last_name, gender, created_at) VALUES (:first_name, :last_name, :gender, :created_at)');
    $this->db->execute([
      ':first_name' => $this->data_to_process['first_name'],
      ':last_name' => $this->data_to_process['last_name'],
      ':gender' => $this->data_to_process['gender'],
      ':created_at' => DATE_NOW,
    ]);

    $this->user_id = $this->db->last_insert_id();

    $this->db->query('INSERT INTO '.DB_TABLE_AUTH.' (user_id, email, password, role) VALUES (:user_id, :email, :password, :role)');
    $this->db->execute([
      ':user_id' => $this->user_id,
      ':email' => $this->data_to_process['email'],
      ':password' => password_hash($this->data_to_process['password'], PASSWORD_BCRYPT),
      ':role' => 'user'
    ]);
  }

  /**
   * überprüft ob das eingegebene und das in der Datenbank vorhandene Passwort übereinstimmen.
   *
   * @return boolean Passwort ist gleich oder ist nicht gleich
   */
  public function verify_password()
  {
    $this->db->query('SELECT user_id, password FROM '.DB_TABLE_AUTH.' WHERE email = :email');
    $row = $this->db->single([
      ':email' => $this->data_to_process['email']
    ]);

    if (password_verify($this->data_to_process['password'], $row['password'])) {

      $this->user_id = $row['user_id'];
      return true;

    } else {
      return false;
    }
  }

  /**
   * Meldet den User an und lädt alle wichtigen Nutzer-Daten in die aktuelle Session.
   * @return void
   */
  public function login()
  {
    $this->user->set_user_id($this->get_user_id())->set_last_login();

    Session::write('user_data', (new User)->set_user_id($this->user_id)->get_by_id());
    Session::write('user_data', [
      'is_logged_in' => 1,
      'role' => $this->get_role()
    ]);
  }

  /**
   * Meldet den aktuellen User ab.
   * @return void
   */
  public function logout()
  {
    Session::write('user_data', ['is_logged_in' => 0]);

    session_unset();
    session_destroy();
  }

  /**
   * Überprüft ob eine Email bereits in der Datenbank existiert.
   *
   * @return boolean Email existiert oder Email existiert nicht
   */
  public function exists()
  {
    $this->db->query('SELECT email FROM '.DB_TABLE_AUTH.' WHERE email = :email');
    $this->db->single([
      ':email' => $this->data_to_process['email']
    ]);

    if ($this->db->row_count() > 0) {

      return true;

    } else {

      return false;

    }
  }

  /**
   * Überprüft ob der User eingeloggt ist.
   *
   * @return boolean ist eingeloggt oder ist nicht eingeloggt
   */
  public function is_logged_in()
  {
    if ($this->user_data['is_logged_in'] == 1) {

      return true;

    } else {

      return false;

    }
  }

  /**
   * Überprüft ob der User die Adminrolle besitzt.
   *
   * @return boolean hat admin rechte oder hat keine
   */
  public function is_admin()
  {
    if ($this->user_data['role'] == 'admin') {

      return true;

    } else {

      return false;

    }
  }

  /**
   * Lädt die Rolle eines Nutzers.
   *
   * @return array Nutzerrolle/-rechte
   */
  public function get_role()
  {
    $this->db->query('SELECT role FROM '.DB_TABLE_AUTH.' WHERE user_id = :user_id');
    $row = $this->db->single([
      ':user_id' => $this->get_user_id()
    ]);

    return $row['role'];
  }

  /**
   * Ändert die Rechte eines Users.
   * @return void
   */
  public function update_role()
  {
    $this->db->query('UPDATE '.DB_TABLE_AUTH.' SET role = :role WHERE user_id = :user_id');
    $this->db->execute([
      ':user_id' => $this->get_user_id(),
      ':role' => $this->data_to_process['user_role']
    ]);
  }

  /**
   * Lädt Authentifizierungsinfos über den Nutzer.
   * @return array Authentifizierungsinfos
   */
  public function get_info()
  {
    $this->db->query('SELECT email, role FROM '.DB_TABLE_AUTH.' WHERE user_id = :user_id');
    return $this->db->single([
      ':user_id' => $this->get_user_id()
    ]);
  }

  /**
   * Holt den Hashwert des Passworts eines Nutzers.
   * @return string Hashwert des Nutzerpassworts
   */
  public function get_password()
  {
    $this->db->query('SELECT password FROM '.DB_TABLE_AUTH.' WHERE user_id = :user_id');
    return $this->db->single([
      ':user_id' => $this->user_data['user_id']
    ])['password'];
  }

  /**
   * Ändert die Email-Adresse eines Nutzers.
   * @return void
   */
  public function update_email()
  {
    $this->db->query('UPDATE '.DB_TABLE_AUTH.' SET email = :email WHERE user_id = :user_id');
    $this->db->execute([
      ':user_id' => $this->get_user_id(),
      ':email' => $this->data_to_process['email']
    ]);
  }

  /**
   * Überprüft ob der Nutzer sein Password ändern darf.
   *
   * 1. Stimmt das eingebene alte Passwort mit dem Passwort in der Datenbank überein?
   * 2. Stimmt die Wiederholung des neuen Passworts mit dem neuen Passwort überein?
   *
   * @return boolean Nutzer darf Passwort ändern
   */
  public function password_change_allowed()
  {
    if (!password_verify($this->data_to_process['old_pw'], $this->get_password())) {
      return false;
    } else {
      if ($this->data_to_process['new_pw'] !== $this->data_to_process['new_pw_repeat']) {
        return false;
      } else {
        return true;
      }
    }
  }

  /**
   * Ändert das Passwort eines Nutzers.
   * @return void
   */
  public function update_password()
  {
    $this->db->query('UPDATE '.DB_TABLE_AUTH.' SET password = :password WHERE user_id = :user_id');
    $this->db->execute([
      ':user_id' => $this->get_user_id(),
      ':password' => password_hash($this->data_to_process['new_pw'], PASSWORD_BCRYPT)
    ]);
  }

}
