<?

/**
 * Diese Klasse ist für die Interaktion mit der 'user'-Tabelle zuständig.
 */
class User extends BaseModel
{

  /**
   * Datensatz eines Nutzers mithilfe der ID aus der Datenbank holen.
   * @return array Datzensatz des Nutzers
   */
  public function get_by_id()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_USER.' WHERE user_id = :user_id');
    $row = $this->db->single([
      ':user_id' => $this->get_user_id()
    ]);

    return $row;
  }

  /**
   * Alle User aus der Datenbank holen
   * @return array Datensätze aller Nutzer
   */
  public function get_all()
  {
    $this->db->query('SELECT u.*, a.role FROM '.DB_TABLE_USER.' u LEFT JOIN '.DB_TABLE_AUTH.' a ON u.user_id = a.user_id');
    return $this->db->resultset();
  }

  /**
   * Setzt die user_id mit der die aktuelle Instanz der Klasse arbeitet.
   *
   * @param integer $id
   * @return void gibt die aktuelle Instanz der Klasse zurück um Funktionen zu verknüpfen
   */
  public function set_user_id($id)
  {
    $this->user_id = $id;

    return $this;
  }

  /**
   * überschreibt das Datum und die Uhrzeit des Users mit dem letzten sich angemeldeten Login Datum.
   */
  public function set_last_login()
  {
    $this->db->query('UPDATE '.DB_TABLE_USER.' SET last_login = :last_login WHERE user_id = :user_id');
    $this->db->execute([
      ':user_id' => $this->user_id,
      ':last_login' => DateTimeHelper::get_mysql_date(DATE_NOW)
    ]);
  }

  /**
   * Löscht den Datensatz eines Nutzers.
   * @return void
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_USER.' WHERE user_id = :user_id');
    $this->db->execute([
      ':user_id' => $this->user_id
    ]);
  }

  /**
   * Aktualisiert den Datensatz eines Nutzers.
   * @return void
   */
  public function update()
  {
    if (!isset($this->user_id)) {
      $this->user_id = $this->user_data['user_id'];
    }

    $this->db->query('UPDATE '.DB_TABLE_USER.' SET first_name = :first_name, last_name = :last_name, gender = :gender WHERE user_id = :user_id');
    $this->db->execute([
      ':first_name' => $this->data_to_process['first_name'],
      ':last_name' => $this->data_to_process['last_name'],
      ':gender' => $this->data_to_process['gender'],
      ':user_id' => $this->user_id
    ]);
  }
}
