<?

/**
 * Stellt Funktionalität für den Upload der Bilder bereit.
 */
class Upload {

  /**
   * Datei, die hochgeladen werden soll.
   * @var array
   */
  protected $file_to_upload;

  /**
   * Dateien, die hochgeladen werden sollen.
   * @var array
   */
  protected $file_array;

  /**
   * Dateiformat, der aktuelle Datei.
   * @var string
   */
  protected $extension;

  /**
   * Zieldatei auf dem Server.
   * @var string
   */
  protected $target_file;

  /**
   * Dateiname auf dem Server.
   * @var string
   */
  protected $final_filename;

  /**
   * Erlaubte Dateiformate.
   * @var array
   */
  protected $allowed_file_types;

  /**
   * Maximale Dateigröße.
   * @var integer
   */
  protected $max_file_size;

  /**
   * Soll die Datei zugeschnitten werden?
   * @var boolean
   */
  protected $resize;

  /**
   * Breite nach dem Zuschneiden.
   * @var integer
   */
  protected $resize_width;

  /**
   * Höhe nach dem Zuschneiden.
   * @var integer
   */
  protected $resize_height;

  /**
   * Modus, der für das Zuschneiden genutzt werden soll.
   * @var string
   */
  protected $crop_mode = 'crop-m';

  /**
   * Daten der hochgeladenen Bilder.
   * @var array
   */
  protected $image_data = [];

  /**
   * Ausgewähltes Template.
   * @var string
   */
  protected $template;

  /**
   * Verfügbare Templates.
   * @var array
   */
  protected $templates = [
    'product_image' => [
      'target_dir' => ROOT.'storage/app/public/uploads/images/product/',
      'allowed_file_types' => ['gif','png','jpg','jpeg'],
      'max_file_size' => 5000000,
      'resize' => true,
      'resize_width' => 500,
      'resize_height' => 500
    ],
    'artist_image' => [
      'target_dir' => ROOT.'storage/app/public/uploads/images/artist/',
      'allowed_file_types' => ['gif','png','jpg','jpeg'],
      'max_file_size' => 5000000,
      'resize' => true,
      'resize_width' => 500,
      'resize_height' => 500
    ]
  ];

  /**
   * Gibt die gesammelten Daten der Bilder zurück.
   * @return array Bilderdaten
   */
  public function get_image_data()
  {
    return $this->image_data;
  }

  /**
   * Setzt das Array welches die Daten der hochzuladenden Bilder enthält.
   * @param array $file Array welches die Daten der hochzuladenden Bilder enthält
   */
  public function set_file_array($file)
  {
    if (ArrayHelper::contains_multiple_files($file)) {
      $this->file_array = ArrayHelper::convert_upload_array($file);
    } else {
      $this->file_array[0] = $file;
    }
  }

  /**
   * Setzt die aktuelle Bild-Datei.
   * @param array $file Aktuelle Bild-Datei
   */
  public function set_file($file)
  {
    $this->file_to_upload = $file;
  }

  /**
   * Setzt das aktuelle Template.
   * @param string $template Zu verwendendes template
   */
  public function set_template($template)
  {
    $this->template = $template;
    $this->apply_template();
  }

  /**
   * Übernimmt die Einstellungen des ausgewählten Templates.
   * @return void
   */
  public function apply_template()
  {
    if (isset($this->template)) {

      foreach ($this->templates[$this->template] as $key => $value) {

        $this->$key = $value;

      }

    }
  }

  /**
   * Für den Upload durch.
   * @return boolean Upload-Status
   */
  public function process_upload()
  {

    foreach ($this->file_array as $file) {

      $this->set_file($file);

      $this->get_extension();
      $this->generate_random_name();
      $this->upload_file();

      if ($this->resize == true) {
        $this->resize();
      }

    }

    return true;
  }

  /**
   * Generiert einen zufälligen Dateinamen.
   * @return void
   */
  public function generate_random_name() {
    // Zuffälligen Dateinamen generieren
    $this->final_filename = (new Generate)->secure_random_string($length = 13).".".$this->extension;
    // Zieldatei festlegen in welcher das Bild gespeichert wird
    $this->target_file = $this->target_dir . $this->final_filename;
    $this->check_file_exists();

    $this->image_data[] = $this->final_filename;
   }

  /**
   * Überprüft ob eine Datei bereits existiert.
   * @return [type] [description]
   */
  public function check_file_exists() {
    if (file_exists($this->target_file)) {
      $this->generateRandomName();
    }
  }

  /**
   * Lädt Datei hoch.
   * @return void
   */
  public function upload_file() {
    move_uploaded_file($this->file_to_upload["tmp_name"], $this->target_file);
  }

  /**
   * Holt sich die Erweiterung einer Datei.
   * @return void
   */
  protected function get_extension()
  {
    $this->extension = pathinfo($this->file_to_upload["name"], PATHINFO_EXTENSION);
  }

  /**
   * Schneidet ein Bild zu.
   * @return void
   */
  public function resize() {
    $magicianObj = new ImageMagician($this->target_file);
    $magicianObj->resizeImage($this->resize_width, $this->resize_height, $this->crop_mode);
    $magicianObj->saveImage($this->target_file, 100);
  }

}
