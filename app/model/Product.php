<?

/**
 * Diese Klasse ist für die Interaktion mit der 'product'-Tabelle zuständig.
 */
class Product extends BaseModel
{

  /**
   * SQL-String der alle nötigen Infos für ein Produkt enthält.
   * Kann um "WHERE"/"ORDER BY"/etc Statements erweitert werden.
   * @var string
   */
  protected $sql_string_full_info =
    'SELECT p.*, c.category_name, a.name AS artist_name, a.slug AS artist_slug, img.url AS image_url FROM '.DB_TABLE_PRODUCT.' AS p
     LEFT JOIN '.DB_TABLE_CATEGORY.' AS c ON p.category_id = c.category_id
     LEFT JOIN '.DB_TABLE_ARTIST.' AS a ON p.artist_id = a.artist_id
     LEFT JOIN '.DB_TABLE_IMAGE.' AS img ON p.product_id = img.product_id';


  /**
   * Setter für Artist-ID
   * @param integer $artist_id Artist-ID
   */
  public function set_artist_id($artist_id)
  {
    $this->artist_id = $artist_id;
  }

  /**
   * Setter für Produkt-Slug
   * @param integer $slug [description]
   */
  public function set_slug($slug)
  {
    $this->slug = $slug;
  }

  /**
   * Getter für Produkt-Slug
   * @return $slug
   */
  public function get_slug()
  {
    return $this->slug;
  }

  /**
   * Setter für die Bild-Daten (url, etc.) welche nach dem Upload in der Datenbank gespeichert werden sollen.
   * @param array $data
   */
  public function set_image_data($data)
  {
    $this->image_data = $data;
  }

  /**
   * Findet die Produkt-ID anhand eines Slugs
   * @param  string $slug [description]
   * @return integer       [description]
   */
  public function get_id_by_slug($slug)
  {
    $this->db->query('SELECT product_id FROM '.DB_TABLE_PRODUCT.' WHERE slug = :slug');
    return $this->db->single([
      ':slug' => $slug,
    ])['product_id'];
  }

  /**
   * Setzt die Kategorie-ID mit der die aktuelle Instanz der Klasse arbeitet
   * @param integer $id
   */
  public function set_category_id($id)
  {
    $this->category_id = $id;
  }

  /**
   * Setzt die Produkt-ID mit der die aktuelle Instanz der Klasse arbeitet
   * @param integer $id
   */
  public function set_product_id($id)
  {
    $this->product_id = $id;

    return $this;
  }

  public function get_product_id()
  {
    return $this->product_id;
  }

  /**
   * Holt sich alle Produkte.
   * @return array Prdouktdaten
   */
  public function get_all()
  {
    $this->db->query($this->sql_string_full_info);
    return $this->db->resultset();
  }

  /**
   * Gibt die Anzahl der Produkte in einer Kategorie zurück.
   * @return integer Anzahl der Produkte in der Kategorie
   */
  public function get_amount_by_category_id()
  {

    $this->db->query('SELECT * FROM '.DB_TABLE_PRODUCT.' WHERE category_id = :category_id');
    $rows = $this->db->resultset([
      ':category_id' => $this->category_id
    ]);

    return $this->db->row_count();
  }

  /**
   * Überprüft ob der erstellte Slug schon in der Datenbank existiert.
   * @return boolean Slug existiert bereits.
   */
  public function slug_exists()
  {
    $this->db->query('SELECT slug FROM '.DB_TABLE_PRODUCT.' WHERE slug = :slug');
    $row = $this->db->single([
      ':slug' => $this->slug,
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Fügt ein neues Produkt zur Datenbank hinzu.
   */
  public function insert()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_PRODUCT.' (category_id, artist_id, name, description, slug, price, release_date, stock, discount, created_at) VALUES (:category_id, :artist_id, :name, :description, :slug, :price, :release_date, :stock, :discount, :created_at)');
    $this->db->execute([
      ':category_id' => $this->data_to_process['category'],
      ':artist_id' => $this->data_to_process['artist'],
      ':name' => $this->data_to_process['name'],
      ':description' => $this->data_to_process['description'],
      ':slug' => $this->slug,
      ':price' => $this->data_to_process['price'],
      ':release_date' => $this->data_to_process['release_date'],
      ':stock' => $this->data_to_process['stock'],
      ':discount' => $this->data_to_process['discount'],
      ':created_at' => DATE_NOW
    ]);

    $this->product_id = $this->db->last_insert_id();
  }

  /**
   * Ändert den Datensatz eines Produkts.
   */
  public function update()
  {
    $this->db->query('UPDATE '.DB_TABLE_PRODUCT.' SET category_id = :category_id, artist_id = :artist_id, name = :name, description = :description, slug= :slug, price = :price, release_date = :release_date, stock = :stock, discount = :discount, updated_at = :updated_at WHERE product_id = :product_id');
    $this->db->execute([
      ':category_id' => $this->data_to_process['category'],
      ':artist_id' => $this->data_to_process['artist'],
      ':name' => $this->data_to_process['name'],
      ':description' => $this->data_to_process['description'],
      ':slug' => $this->slug,
      ':price' => $this->data_to_process['price'],
      ':release_date' => $this->data_to_process['release_date'],
      ':stock' => $this->data_to_process['stock'],
      ':discount' => $this->data_to_process['discount'],
      ':updated_at' => DATE_NOW,
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Löscht ein Produkt.
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_PRODUCT.' WHERE product_id = :product_id');
    $this->db->execute([
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Gibt den passenden Datensatz zur gegebenen ID zurück.
   */
  public function get_by_id()
  {
    $this->db->query($this->sql_string_full_info.' WHERE p.product_id = :product_id');

    $row = $this->db->single([
      ':product_id' => $this->product_id
    ]);

    $this->db->query(
      'SELECT * FROM '.DB_TABLE_TRACK.'
      WHERE product_id = :product_id'
    );

    $row['tracks'] = $this->db->resultset([
      ':product_id' => $this->product_id
    ]);

    $this->artist_id = $row['artist_id'];

    return $row;
  }

  /**
   * Ändert die Kategorie eines Produkts.
   * @param integer $new_id
   */
  public function change_category_id_to($new_id)
  {
    $this->db->query('UPDATE '.DB_TABLE_PRODUCT.' SET category_id = :new_category_id, updated_at = :updated_at WHERE category_id = :category_id');
    $this->db->execute([
      ':category_id' => $this->category_id,
      ':updated_at' => DATE_NOW,
      ':new_category_id' => $new_id
    ]);
  }

  /**
   * Speichert Bilder eines Produkts in der Datenbank.
   */
  public function save_images()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_IMAGE.' (url, product_id, is_active, created_at) VALUES (:url, :product_id, :is_active, :created_at)');

    foreach ($this->image_data as $image) {

      $this->db->execute([
        ':url' => $image,
        ':product_id' => $this->product_id,
        ':is_active' => 0,
        ':created_at' => DATE_NOW
      ]);

    }

  }

  /**
   * Löscht alle Bilder eines Produkts aus der Datenbank.
   */
  public function delete_images_by_product()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_IMAGE.' WHERE product_id = :product_id');
    $this->db->execute([
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Holt sich die 10 meiste verkauften Produkte.
   * @return array
   */
  public function get_most_bought()
  {
    $this->db->query($this->sql_string_full_info.' ORDER BY amount_sold DESC LIMIT 10');
    return $this->db->resultset();
  }

  /**
   * Holt sich alle Produkte einer Category
   * @return array
   */
  public function get_by_category()
  {
    $this->db->query($this->sql_string_full_info.' WHERE p.category_id = :category_id');

    return $this->db->resultset([
      ':category_id' => $this->category_id
    ]);
  }

  /**
   * Holt sich alle Produkte eines Artists außer des aktuellen Produkts
   * @return array
   */
  public function get_by_artist_except_current()
  {
    $this->db->query($this->sql_string_full_info.' WHERE p.artist_id = :artist_id AND p.product_id <> :product_id');

    return $this->db->resultset([
      ':artist_id' => $this->artist_id,
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Holt sich alle Produkte geordnet nach Kategorien.
   *
   * @return array
   */
  public function get_all_by_categories()
  {
    $result = (new Category)->get_all();

    foreach ($result as $key => $category) {
      $this->set_category_id($category['category_id']);
      $result[$key]['products'] = $this->get_by_category();
    }

    return $result;
  }

  /**
   * Holt sich alle Produkte eines Artists
   * @return array
   */
  public function get_by_artist()
  {
    $this->db->query($this->sql_string_full_info.' WHERE p.artist_id = :artist_id');

    return $this->db->resultset([
      ':artist_id' => $this->artist_id
    ]);
  }

  /**
   * erhöht die Anzahl der verkauften Produkte
   * @param integer $amount
   */
  public function increase_amount_sold($amount)
  {
    $this->db->query('UPDATE '.DB_TABLE_PRODUCT.' SET amount_sold = amount_sold + :amount_sold WHERE product_id = :product_id');
    $this->db->execute([
      ':amount_sold' => $amount,
      ':product_id' => $this->product_id
    ]);

    return $this;
  }

  /**
   * Senkt die Anzahl der Produkte im Lager
   * @param integer $amount
   */
  public function decrease_stock($amount)
  {
    $this->db->query('UPDATE '.DB_TABLE_PRODUCT.' SET stock = stock - :amount WHERE product_id = :product_id');
    $this->db->execute([
      ':amount' => $amount,
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Erhöht´die Anzahl, wie oft ein Produkt angesehen wurde.
   */
  public function increase_view_amount()
  {
    $this->db->query('UPDATE '.DB_TABLE_PRODUCT.' SET view_amount = view_amount + 1 WHERE product_id = :product_id');
    $this->db->execute([
      ':product_id' => $this->product_id
    ]);
  }

  /**
   * Berechnet das neue Durchschnitts-Rating eines Produkts nachdem eine neue Bewertung abgegeben wurde.
   * @param  integer $rating Abgegebene Bewertung
   * @return void
   */
  public function update_rating($rating)
  {
    $this->db->query(
      'UPDATE
         '.DB_TABLE_PRODUCT.'
       SET
        rating = ((rating * vote_amount + :rating) / IF(vote_amount > 0, vote_amount + 1, 1)),
        vote_amount = vote_amount +1
       WHERE product_id = :product_id'
    );

    $this->db->execute([
      ':product_id' => $this->product_id,
      ':rating' => $rating
    ]);
  }

  public function enough_products($amount)
  {
    $this->db->query('SELECT stock FROM '.DB_TABLE_PRODUCT.' WHERE product_id = :product_id');
    $row = $this->db->single([
      ':product_id' => $this->product_id
    ]);

    if ($row['stock'] >= $amount) {
      return true;
    } else {
      return false;
    }
  }
}
