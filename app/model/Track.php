<?

/**
 * Diese Klasse ist für die Interaktion mit der 'product_track'-Tabelle zuständig
 */
class Track extends BaseModel
{

  /**
   * Setzt die Produkt-ID mit der die aktuelle Instanz der Klasse arbeitet
   *
   * @param integer $id
   */
  public function set_product_id($id)
  {
    $this->product_id = $id;
  }

  /**
   * Speichert alle Tracks eines Produktes in der Datenbank
   * @return void
   */
  public function insert()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_TRACK.' (product_id, name, length, youtube_id) VALUES (:product_id, :name, :length, :youtube_id)');

    foreach ($this->data_to_process['name'] as $key => $trackname) {

      $date = DateTime::createFromFormat("H:i:s", $this->data_to_process['length'][$key]);
      $formated_date = $date->format('H:i:s');

      $this->db->execute([
        ':product_id' => $this->product_id,
        ':name' => $trackname,
        ':length' => $formated_date,
        ':youtube_id' => $this->data_to_process['youtube_id'][$key]
      ]);

    }
  }

  /**
   * Löscht einen Track aus der Datenbank
   * @return void
   */
  public function delete_by_product()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_TRACK.' WHERE product_id = :product_id');
    $this->db->execute([
      ':product_id' => $this->product_id
    ]);
  }
}
