<?

/**
 * Enthält die Funktionalität für eine Suche.
 */
class Search extends BaseModel
{
  /**
   * Setzt den Suchtext mit der die aktuelle Instanz der Klasse arbeitet.
   * @param string $text Text nach dem in den verschiedenen Tabellen gesucht werden soll
   */
  public function set_search_text($text)
  {
    $this->search_text = $text;
  }

  /**
   * Liefert alle Produkte, die den Suchtext im Name oder der Beschreibung enthalten.
   * @return array Produktdatensätze
   */
  public function products()
  {
    $this->db->query(
      'SELECT p.*, a.name AS artist_name
      FROM  '.DB_TABLE_PRODUCT.' p
      LEFT JOIN '.DB_TABLE_ARTIST.' a
      ON p.artist_id = a.artist_id
      WHERE p.name LIKE :search_text
        OR description LIKE :search_text
      GROUP BY p.product_id');

    return $this->db->resultset([
      ':search_text' => '%' . $this->search_text . '%'
    ]);
  }

  /**
   * Liefert alle Künstler, die den Suchtext im Künstlernamen, Vornamen oder Nachnamen enthalten.
   * @return array Artist-Datensätze
   */
  public function artists()
  {
    $this->db->query(
      'SELECT *
      FROM  '.DB_TABLE_ARTIST.' a
      WHERE name LIKE :search_text
        OR first_name LIKE :search_text
        OR last_name LIKE :search_text
      GROUP BY a.artist_id');

    return $this->db->resultset([
      ':search_text' => '%' . $this->search_text . '%'
    ]);
  }

  /**
   * Liefert alle Tracks, die den Suchtext im Namen enthalten.
   * @return array Track-Datensätze
   */
  public function tracks()
  {
    $this->db->query(
      'SELECT t.*, p.slug
      FROM  '.DB_TABLE_TRACK.' t
      LEFT JOIN '.DB_TABLE_PRODUCT.' p
      ON t.product_id = p.product_id
      WHERE t.name LIKE :search_text
      GROUP BY t.track_id');

    return $this->db->resultset([
      ':search_text' => '%' . $this->search_text . '%'
    ]);
  }

  /**
   * Liefert alle Kategorien, die den Suchtext im Namen enthalten.
   * @return array Kategorie-Datensätze
   */
  public function categories()
  {
    $this->db->query(
      'SELECT * FROM '.DB_TABLE_CATEGORY.'
      WHERE category_name LIKE :search_text');

    return $this->db->resultset([
      ':search_text' => '%' . $this->search_text . '%'
    ]);
  }
}
