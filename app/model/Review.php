<?

/**
 * Diese Klasse beschreibt die Funktionalität einer Bewertung eines Produktes durch einen Nutzer.
 */
class Review extends BaseModel
{

  /**
   * Setzt die Bewertungs-ID mit der die aktuelle Instanz der Klasse arbeitet
   * @param integer $id
   */
  public function set_review_id($id)
  {
    $this->review_id = $id;

    return $this;
  }

  /**
   * Überprüft ob eine Wertung bzw. ein Rating valide ist.
   * @return boolean Wertung ist valide.
   */
  public function is_valid()
  {
    if (!isset($this->data_to_process['rating'])) {
      return false;
    } else {
      if ($this->data_to_process['rating'] == 0 || $this->data_to_process['rating'] == "0") {
        return false;
      } else {
        return true;
      }
    }
  }

  /**
   * Speichert eine neue Bewertung in die Datenbank.
   * @return void
   */
  public function create()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_REVIEW.' (product_id, user_id, rating, title, content, created_at) VALUES (:product_id, :user_id, :rating, :title, :content, :created_at)');

    $this->db->execute([
      ':product_id' => $this->data_to_process['product_id'],
      ':user_id' => $this->get_user_id(),
      ':rating' => $this->data_to_process['rating'],
      ':title' => $this->data_to_process['title'],
      ':content' => $this->data_to_process['content'],
      ':created_at' => DATE_NOW
    ]);
    $this->review_id = $this->db->last_insert_id();
  }

  /**
   * Lädt alle Bewertungen eines Produktes
   * @param  integer $product_id ID des Produktes
   * @return array               Datensätze der passenden Bewertungen
   */
  public function get_by_product_id($product_id)
  {
    $this->db->query(
      'SELECT
        sr.*,
        u.user_id,
        u.first_name,
        u.last_name
      FROM '.DB_TABLE_REVIEW.' AS sr
      LEFT JOIN '.DB_TABLE_USER.' AS u
      ON sr.user_id = u.user_id
      WHERE sr.product_id = :product_id');

    $this->db->bind(':product_id', $product_id);
    return $this->db->resultset();
  }

  /**
   * Lädt eine Bewertung anhand einer ID
   * @param  integer $review_id ID der Bewertung
   * @return array              Datensatz der Bewertung
   */
  public function get_by_id($review_id)
  {
    $this->db->query(
      'SELECT
        r.*,
        u.user_id,
        u.first_name,
        u.last_name
      FROM '.DB_TABLE_REVIEW.' AS r
      LEFT JOIN '.DB_TABLE_USER.' AS u
      ON r.user_id = u.user_id
      WHERE r.review_id = :review_id');

    $this->db->bind(':review_id', $review_id);
    return $this->db->single();
  }

  /**
   * Lädt alle Bewertungen
   * @return array Datensätze der Bewertungen
   */
  public function get_all()
  {
    $this->db->query(
      'SELECT
        r.*,
        u.user_id,
        u.first_name,
        u.last_name,
        p.*
      FROM '.DB_TABLE_REVIEW.' AS r
      LEFT JOIN '.DB_TABLE_USER.' AS u
      ON r.user_id = u.user_id
      LEFT JOIN '.DB_TABLE_PRODUCT.' AS p
      ON r.product_id = p.product_id');

    return $this->db->resultset();
  }

  /**
   * Löscht eine Bewertung.
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_REVIEW.' WHERE review_id = :review_id');
    $this->db->execute([
      ':review_id' => $this->review_id
    ]);
  }

  /**
   * Überprüft, ob die gegebene Bewertungs-ID zu dem aktuellen User gehört
   * @param  integer $id Bewertungs-ID, welche überprüft werden soll
   * @return boolean     Bewertung gehört zu einem User
   */
  public function is_owned_by_user()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_REVIEW.' WHERE user_id = :user_id AND review_id = :review_id');
    $row = $this->db->single([
      ':user_id' => $this->get_user_id(),
      ':review_id' => $this->review_id
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

}
