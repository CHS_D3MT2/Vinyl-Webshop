<?

/**
 * Beschreibt die Funktionalität einer Bestellung
 */
class Order extends BaseModel
{
  /**
   * ID der Lieferadresse.
   * @var integer
   */
  public $shipping_address_id;

  /**
   * ID der Rechnungsadresse.
   * @var integer
   */
  public $payment_address_id;

  /**
   * ID der Zahlung.
   * @var integer
   */
  public $payment_id;

  /**
   * Code der Liefermethode.
   * @var string
   */
  public $shipping;

  /**
   * Setzt die Bestell-ID mit der die aktuelle Instanz der Klasse arbeitet
   * @param integer $id
   */
  public function set_order_id($id)
  {
    $this->order_id = $id;

    return $this;
  }

  /**
   * Gibt den kompletten Datensatz einer Bestellung zurück.
   * @return array Datensatz der Bestellung
   */
  public function get_by_id()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ORDER.' WHERE order_id = :order_id');

    $order = $this->db->single([
      ':order_id' => $this->order_id
    ]);

    $this->db->query(
      'SELECT oc.*, p.*, c.category_name, pi.url, a.slug AS artist_slug, a.name AS artist_name
      FROM '.DB_TABLE_ORDER_CONTENT.' oc
      LEFT JOIN '.DB_TABLE_PRODUCT.' p
      ON oc.product_id = p.product_id
      LEFT JOIN '.DB_TABLE_CATEGORY.' AS c
      ON p.category_id = c.category_id
      LEFT JOIN '.DB_TABLE_IMAGE.' pi
      ON p.product_id = pi.product_id
      LEFT JOIN '.DB_TABLE_ARTIST.' AS a
      ON p.artist_id = a.artist_id
      WHERE oc.order_id = :order_id
      GROUP BY oc.product_id
      ORDER BY oc.content_id'
    );

    $order['content'] = $this->db->resultset([
      ':order_id' => $this->order_id
    ]);

    return $order;
  }

  /**
   * Liefert alle Bestellungen eines User und detailierte Infos zur Bestellung.
   * @return array Bestellungen und detailierte Infos.
   */
  public function get_by_user_detailed()
  {
    $this->db->query(
      'SELECT o.*, s.code, s.company, s.name AS shipping_name, s.price AS shipping_price FROM '.DB_TABLE_ORDER.' o
      LEFT JOIN shipping s
      ON o.shipping_code = s.code
      WHERE o.user_id = :user_id
      ORDER BY o.created_at DESC'
    );

    $orders = $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);

    foreach ($orders as $field => $order) {

      $this->db->query(
        'SELECT oc.*, p.name, pi.url, a.slug AS artist_slug, a.name AS artist_name
        FROM '.DB_TABLE_ORDER_CONTENT.' oc
        LEFT JOIN '.DB_TABLE_PRODUCT.' p
        ON oc.product_id = p.product_id
        LEFT JOIN '.DB_TABLE_IMAGE.' pi
        ON p.product_id = pi.product_id
        LEFT JOIN '.DB_TABLE_ARTIST.' AS a
        ON p.artist_id = a.artist_id
        WHERE oc.order_id = :order_id
        GROUP BY oc.product_id
        ORDER BY oc.content_id'
      );

      $orders[$field]['content'] = $this->db->resultset([
        ':order_id' => $order['order_id']
      ]);

    }

    return $orders;
  }

  /**
   * Setzt ID der Lieferadresse für die aktuelle Bestellung.
   * @param integer $id ID der Lieferadresse
   */
  public function set_shipping_address_id($id)
  {
    $this->delivery_address_id = $id;
    Session::write('order', ['shipping_address_id' => $id]);
  }

  /**
   * Setzt ID der Rechnungsadresse für die aktuelle Bestellung.
   * @param integer $id ID der Rechnungsadresse
   */
  public function set_payment_address_id($id)
  {
    $this->payment_address_id = $id;
    Session::write('order', ['payment_address_id' => $id]);
  }

  /**
   * Setzt ID der Zahlung für die aktuelle Bestellung.
   * @param integer $id ID der Zahlung
   */
  public function set_payment_id($var)
  {
    $this->payment = $var;
    Session::write('order', ['payment_id' => $var]);
  }

  /**
   * Setzt Code der Liefermethode für die aktuelle Bestellung.
   * @param string $id Code der Liefermethode
   */
  public function set_shipping_code($var)
  {
    $this->shipping = $var;
    Session::write('order', ['shipping' => $var]);
  }

  /**
   * Importiert den aktuellen Warenkorb in die aktuelle Bestellung.
   * @param array $cart Warenkorb-Inhalte
   */
  public function set_cart($cart)
  {
    $this->cart = $cart;
    Session::write('order', ['cart' => $cart]);
  }

  /**
   * Setzt einen Schritt der Bestellung auf abgeschlossen.
   * @param integer $step Schritt der Bestellung
   */
  public function set_complete($step)
  {
    $this->step = $step;
    Session::write('order', ['step_completed' => $step]);
  }

  /**
   * Setzt den Bestllungsfortschritt zurück.
   */
  public function reset_completed()
  {
    Session::clear('order', 'step_completed');
  }

  /**
   * Überprüft ob der vorherige Schritt der Bestellung erfolreich abgeschlossen wurde.
   * @param  integer  $step Aktuller Schritt der Bestellung
   * @return boolean        Vorheriger Schritt der Bestellung ist komplett.
   */
  public function is_previous_complete($step)
  {
    if (Session::read('order.step_completed') + 1 >= $step) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Gibt die ID der Lieferadresse der aktuellen Bestellung zurück.
   * @return integer ID der Lieferadresse
   */
  public function get_shipping_address_id()
  {
    return Session::read('order.shipping_address_id');
  }

  /**
   * Gibt die ID der Rechnungsadresse der aktuellen Bestellung zurück.
   * @return integer ID der Rechnungsadresse
   */
  public function get_payment_address_id()
  {
    return Session::read('order.payment_address_id');
  }

  /**
   * Gibt die ID der Zahlung der aktuellen Bestellung zurück.
   * @return integer ID der Zahlung
   */
  public function get_payment_id()
  {
    return Session::read('order.payment_id');
  }

  /**
   * Gibt den Code der Liefermethode der aktuellen Bestellung zurück.
   * @return string Code der Liefermethode
   */
  public function get_shipping_code()
  {
    return Session::read('order.shipping');
  }

  /**
   * Gibt den Warenkorb der aktuellen Bestellung zurück.
   * @return array Warenkorb-Inhalte
   */
  public function get_cart()
  {
    return Session::read('order.cart');
  }

  /**
   * Gibt die ID der aktuellen Bestellung zurück.
   * @return integer ID der aktuellen Bestellung
   */
  public function get_order_id()
  {
    return Session::read('order.order_id');
  }

  /**
   * Löscht die aktuelle Bestellung aus der Session.
   * @return void
   */
  public function clear()
  {
    Session::clear('order');
  }

  /**
   * Überprüft, ob eine Order-ID in der Datenbank existiert.
   * @return boolean Order existiert
   */
  public function exists()
  {
    $this->db->query('SELECT order_id FROM '.DB_TABLE_ORDER.' WHERE order_id = :order_id');
    $this->db->single([
      ':order_id' => $this->order_id
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Erstellt eine einzigartige Order-ID.
   * @return integer generierte Order-ID
   */
  public function create_order_id()
  {
    $this->order_id = (new Generate)->secure_random_numeric(19);

    if ($this->exists()) {
      $this->create_order_id();
    } else {
      Session::write('order', ['order_id' => $this->order_id]);
    }
  }

  /**
   * Aktuelle Bestellung abschließen/kaufen.
   * @return void
   */
  public function buy()
  {
    $this->create_order_id();

    $this->db->query('INSERT INTO '.DB_TABLE_ORDER.' (order_id, user_id, is_paid, payment_address_id, shipping_address_id, payment_id, shipping_code, created_at) VALUES (:order_id, :user_id, :is_paid, :payment_address_id, :shipping_address_id, :payment_id, :shipping_code, :created_at)');
    $this->db->execute([
      'order_id' => $this->order_id,
      ':user_id' => $this->user_data['user_id'],
      ':is_paid' => 1,
      ':payment_address_id' => $this->get_payment_address_id(),
      ':shipping_address_id' => $this->get_shipping_address_id(),
      ':payment_id' => $this->get_payment_id(),
      ':shipping_code' => $this->get_shipping_code(),
      ':created_at' => DATE_NOW
    ]);

    $product_model = new Product;

    $total_price = 0;

    foreach ($this->get_cart() as $product_id => $product) {

      $price = $product_model->set_product_id($product_id)->get_by_id()['price'];

      $total_price += $price * $product['amount'];

      $this->db->query('INSERT INTO '.DB_TABLE_ORDER_CONTENT.' (order_id, product_id, amount, price) VALUES (:order_id, :product_id, :amount, :price)');
      $this->db->execute([
        ':order_id' => $this->order_id,
        ':product_id' => $product_id,
        ':price' => $price,
        ':amount' => $product['amount']
      ]);

      $product_model->set_product_id($product_id)->increase_amount_sold($product['amount'])->decrease_stock($product['amount']);
    }

    $this->db->query('UPDATE '.DB_TABLE_ORDER.' SET total_price = :total_price WHERE order_id = :order_id');
    $this->db->execute([
      ':total_price' => $total_price,
      ':order_id' => $this->order_id
    ]);

  }

  /**
   * Überprüft, ob eine Adresse in einer Bestellung verwendet wurde.
   * @param  integer  $address_id
   * @return boolean  Adresse wurde benutzt
   */
  public function is_address_used($address_id)
  {
    $this->db->query('SELECT order_id FROM '.DB_TABLE_ORDER.' WHERE (payment_address_id = :payment_address_id OR shipping_address_id = :shipping_address_id) AND user_id = :user_id');
    $this->db->single([
      ':payment_address_id' => $address_id,
      ':shipping_address_id' => $address_id,
      ':user_id' => $this->get_user_id()
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Überprüft, ob eine Zahlungsart in einer Bestellung verwendet wurde.
   * @param  integer  $payment_id
   * @return boolean  Zahlungsart wurde benutzt
   */
  public function is_payment_used($payment_id)
  {
    $this->db->query('SELECT order_id FROM '.DB_TABLE_ORDER.' WHERE (payment_id = :payment_id) AND user_id = :user_id');
    $this->db->single([
      ':payment_id' => $payment_id,
      ':user_id' => $this->get_user_id()
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Gibt alle Datensätze der Künstler-Tabelle zurück.
   * @return array Datensätze der Künstler
   */
  public function get_all()
  {
    $this->db->query('SELECT o.*, u.first_name, u.last_name, s.code, s.company, s.name AS shipping_name, s.price AS shipping_price FROM '.DB_TABLE_ORDER.' o
    LEFT JOIN shipping s
    ON o.shipping_code = s.code
    LEFT JOIN '.DB_TABLE_USER.' u
    ON o.user_id = u.user_id
    ORDER BY o.created_at DESC');
    return $this->db->resultset();
  }

  /**
   * Löscht eine Bestellung.
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_ORDER.' WHERE order_id = :order_id');
    $this->db->execute([
      ':order_id' => $this->order_id
    ]);
  }


}
