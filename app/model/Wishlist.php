<?

/**
 * Diese Klasse beschreibt die Funktionalität der Wunschliste.
 * Das heißt sie ist für die Interaktion mit der 'user_wishlist'-Tabelle zuständig.
 */
class Wishlist extends BaseModel
{
    /**
     * Setzt die Produkt-ID mit der die aktuelle Instanz der Klasse arbeitet.
     *
     * @param integer $id
     */
    public function set_product_id($id)
    {
        $this->product_id = $id;
    }

    /**
     * Holt sich alle Produkte, die von einem bestimmten Nutzer zur Wunschliste hinzugefügt wurden.
     * @return array Produkte in der Wunschliste
     */
    public function get_by_user()
    {
      $this->db->query(
        'SELECT w.created_at AS date_added, p.*, c.category_name, a.name AS artist_name, a.slug AS artist_slug, img.url AS image_url FROM '.DB_TABLE_WISHLIST.' AS w
        LEFT JOIN '.DB_TABLE_PRODUCT.' AS p ON w.product_id = p.product_id
        LEFT JOIN '.DB_TABLE_CATEGORY.' AS c ON p.category_id = c.category_id
        LEFT JOIN '.DB_TABLE_ARTIST.' AS a ON p.artist_id = a.artist_id
        LEFT JOIN '.DB_TABLE_IMAGE.' AS img ON p.product_id = img.product_id
        WHERE user_id = :user_id ORDER BY date_added DESC'
      );

      return $this->db->resultset([':user_id' => $this->user_data['user_id']]);
    }

    /**
     * Fügt ein neues Produkt zur Wunschliste hinzu.
     * @return void
     */
    public function insert()
    {
      $this->db->query('INSERT INTO '.DB_TABLE_WISHLIST.' (user_id, product_id, created_at)
      VALUES (:user_id, :product_id, :created_at)');
      $this->db->execute([
        ':user_id' => $this->user_data['user_id'],
        ':product_id' => $this->product_id,
        ':created_at' => DATE_NOW
      ]);
    }
    /**
     * Überprüft ob ein Produkt bereits in der Wunschliste ist
     *
     * @return true falls das Produkt bereits in der Wunschliste vorhanden ist
     */
    public function exists()
    {
      $this->db->query('SELECT * FROM '.DB_TABLE_WISHLIST.' WHERE product_id = :product_id AND user_id = :user_id');
      $this->db->single([
        ':product_id' => $this->product_id,
        ':user_id' => $this->user_data['user_id']
      ]);
      if ($this->db->row_count() > 0) {
        return true;
      } else {
        return false;
      }
    }
    /**
     * löscht ein Produkt aus der Wunschliste
     */
    public function delete()
    {
      $this->db->query('DELETE FROM '.DB_TABLE_WISHLIST.' WHERE product_id = :product_id');
      $this->db->execute([
        ':product_id' => $this->product_id
      ]);
    }
}
