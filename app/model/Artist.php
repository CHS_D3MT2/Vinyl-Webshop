<?

/**
 * Diese Klasse ist für die Interaktion mit der 'product_artist'-Tabelle zuständig
 */
class Artist extends BaseModel
{
  /**
   * SQL-String der alle nötigen Infos für ein Artist enthält.
   * Kann um "WHERE"/"ORDER BY"/etc Statements erweitert werden.
   * @var string
   */
  protected $sql_string_full_info =
    'SELECT a.*, img.url AS image_url FROM '.DB_TABLE_ARTIST.' AS a
     LEFT JOIN '.DB_TABLE_ARTIST_IMAGE.' AS img ON a.artist_id = img.artist_id';

  /**
   * setzt die artist_id mit der die aktuelle Instanz der Klasse arbeitet
   * @param integer $artist_id
   */
  public function set_artist_id($artist_id)
  {
    $this->artist_id = $artist_id;
  }

  /**
   * Findet die artist-id anhand eines Slugs
   * @param  string $slug [description]
   * @return array       ???
   */
  public function get_id_by_slug($slug)
  {
    $this->db->query('SELECT artist_id FROM '.DB_TABLE_ARTIST.' WHERE slug = :slug');
    return $this->db->single([
      ':slug' => $slug,
    ])['artist_id'];
  }

  /**
   * Fügt einen neuen Künstler zur Datenbank hinzu.
   * @return void
   */
  public function insert()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_ARTIST.' (name, first_name, last_name, slug) VALUES (:name, :first_name, :last_name, :slug)');

    $this->db->execute([
      ':name' => $this->data_to_process['name'],
      ':first_name' => $this->data_to_process['first_name'],
      ':last_name' => $this->data_to_process['last_name'],
      ':slug' => StringHelper::to_slug($this->data_to_process['name'])
    ]);

    $this->artist_id = $this->db->last_insert_id();
  }

  /**
   * Aktualisiert den Datensatz eines Künstlers in der Datenbank.
   * @return void
   */
  public function update()
  {
    $this->db->query('UPDATE '.DB_TABLE_ARTIST.' SET name = :name, first_name = :first_name, last_name = :last_name, slug = :slug WHERE artist_id = :artist_id');

    $this->db->execute([
      ':artist_id' => $this->artist_id,
      ':name' => $this->data_to_process['name'],
      ':first_name' => $this->data_to_process['first_name'],
      ':last_name' => $this->data_to_process['last_name'],
      ':slug' => StringHelper::to_slug($this->data_to_process['name'])
    ]);
  }

  /**
   * Gibt alle Datensätze der Künstler-Tabelle zurück.
   * @return array Datensätze der Künstler
   */
  public function get_all()
  {
    $this->db->query($this->sql_string_full_info);
    return $this->db->resultset();
  }

  /**
   * Löscht den Datensatz eines Künstlers aus der Datenbank.
   * @return void
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_ARTIST.' WHERE artist_id = :artist_id');
    $this->db->execute([
      ':artist_id' => $this->artist_id
    ]);
  }

  /**
   * Lädt den Datensatz eines Künstlers anhand der 'artist_id' aus der Datenbank.
   * @return array Datensatz des Künstlers
   */
  public function get_by_id()
  {
    $this->db->query($this->sql_string_full_info.' WHERE a.artist_id = :artist_id');
    return $this->db->single([
      ':artist_id' => $this->artist_id
    ]);
  }

  /**
   * Setter für die Bild-Daten (url, etc.) welche nach dem Upload in der Datenbank gespeichert werden sollen.
   * @param array $data
   */
  public function set_image_data($data)
  {
    $this->image_data = $data;
  }

  /**
   * Speichert Bilder eines Produkts in der Datenbank.
   */
  public function save_images()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_ARTIST_IMAGE.' (url, artist_id, is_active, created_at) VALUES (:url, :artist_id, :is_active, :created_at)');

    foreach ($this->image_data as $image) {
      $this->db->execute([
        ':url' => $image,
        ':artist_id' => $this->artist_id,
        ':is_active' => 0,
        ':created_at' => DATE_NOW
      ]);
      
    }
  }

  /**
   * Löscht alle Bilder eines Produkts aus der Datenbank.
   */
  public function delete_images_by_artist()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_ARTIST_IMAGE.' WHERE artist_id = :artist_id');
    $this->db->execute([
      ':artist_id' => $this->artist_id
    ]);
  }
}
