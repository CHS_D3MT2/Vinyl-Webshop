<?

/**
 * Diese Klasse stellt die Funktionalitäten für die Lieferoptionen bereit.
 */
class Shipping extends BaseModel
{
  /**
   * Holt alle aktiven bzw. sichtbaren Liefermethoden.
   * @return [type] [description]
   */
  public function get_all_visible()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_SHIPPING.' WHERE visible = 1');
    return $this->db->resultset();
  }

  /**
   * Lädt den Datensatz einer Liefermethode anhand des Codes.
   * @param  string $code Code der Liefermethode
   * @return array        Datensatz der Liefermethode
   */
  public function get_by_code($code)
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_SHIPPING.' WHERE visible = 1 AND code = :code');
    return $this->db->single([
      ':code' => $code
    ]);
  }
}
