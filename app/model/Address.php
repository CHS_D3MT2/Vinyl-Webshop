<?

/**
 * Diese Klasse ist für die Interaktion mit der 'address'-Tabelle zuständig
 */
class Address extends BaseModel
{
  /**
   * Anzahl der Adressen, die jeder Nutzer speichern kann.
   * Momentan darf jeder User nur 6 Adressen eintragen.
   * @var integer
   */
  protected $address_amount_allowed = 6;

  /**
   * Setter für address_id
   * @param integer $id
   */
  public function set_address_id($id)
  {
    $this->address_id = $id;

    return $this;
  }

  /**
   * Speichert eine Adresse in die Datenbank.
   * @return void
   */
  public function insert()
  {
    $this->data_to_process['user_id'] = $this->user_data['user_id'];
    $this->data_to_process['created_at'] = DATE_NOW;

    if (!isset($this->data_to_process['is_main_shipping'])) {
      $this->data_to_process['is_main_shipping'] = 0;
    }

    if (!isset($this->data_to_process['is_main_payment'])) {
      $this->data_to_process['is_main_payment'] = 0;
    }

    $this->db->query('INSERT INTO '.DB_TABLE_ADDRESS.' (user_id, company, first_name, last_name, country, city, plz, street, housenumber, phone, is_main_shipping, is_main_payment, created_at) VALUES (:user_id, :company, :first_name, :last_name, :country, :city, :plz, :street, :housenumber, :phone, :is_main_shipping, :is_main_payment, :created_at)');
    $this->db->execute($this->convert_data_to_process());
  }

  /**
   * Prüft, ob der User schon 6 Adressen gespeichert hat oder er noch Adressen speichern darf.
   * @return boolean
   */
  public function can_add_address()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ADDRESS.' WHERE user_id = :user_id');
    $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);

    if ($this->db->row_count() >= $this->address_amount_allowed) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Prüft ob ein User schon Adressen hinzugefügt hat.
   * @return boolean
   */
  public function has_addresses()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ADDRESS.' WHERE user_id = :user_id');
    $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Holt sich alle Adressen eines Users.
   * @return array Alle Adressen, die der User bereits gespeichert hat
   */
  public function get_by_user()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ADDRESS.' WHERE user_id = :user_id');
    return $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);
  }

  /**
   * Holt sich eine Adresse anhand einer ID.
   *
   * @param integer $id
   * @return array Adressdaten
   */
  public function get_by_id($id)
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ADDRESS.' WHERE user_id = :user_id AND address_id = :address_id');
    return $this->db->single([
      ':user_id' => $this->user_data['user_id'],
      ':address_id' => $id
    ]);
  }

  /**
   * Löscht eine Adresse.
   * @return void
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_ADDRESS.' WHERE address_id = :address_id AND user_id = :user_id');
    $this->db->execute([
      ':address_id' => $this->address_id,
      ':user_id' => $this->get_user_id()
    ]);
  }

  /**
   * Überprüft, ob die gegebene Adress-ID zu dem aktuellen User gehört
   * @param  integer $address_id Adress-ID, welche überprüft werden soll
   * @return boolean             Adresse gehört zu enem User
   */
  public function owned_by_user($address_id)
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_ADDRESS.' WHERE user_id = :user_id AND address_id = :address_id');
    $row = $this->db->single([
      ':user_id' => $this->user_data['user_id'],
      ':address_id' => $address_id
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Setzt die Standardmäßige Verwendung der anderen Adressen eines Nutzers zurück.
   */
  public function reset_others()
  {
    if (isset($this->data_to_process['is_main_payment']) && $this->data_to_process['is_main_payment'] == 1) {
      $this->db->query('UPDATE '.DB_TABLE_ADDRESS.' SET is_main_payment = 0 WHERE user_id = :user_id');
      $this->db->execute([
        ':user_id' => $this->get_user_id()
      ]);
    }
    if (isset($this->data_to_process['is_main_shipping']) && $this->data_to_process['is_main_shipping'] == 1) {
      $this->db->query('UPDATE '.DB_TABLE_ADDRESS.' SET is_main_shipping = 0 WHERE user_id = :user_id');
      $this->db->execute([
        ':user_id' => $this->get_user_id()
      ]);
    }
  }
}
