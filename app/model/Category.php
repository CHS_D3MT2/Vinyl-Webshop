<?

/**
 * Beschreibt die Funktionalität einer Produkt-Kategorie
 */
class Category extends BaseModel
{

  /**
   * setzt die category_id mit der die aktuelle Instanz der Klasse arbeitet
   *
   * @param integer $id
   */
  public function set_category_id($id)
  {
    $this->category_id = $id;
  }

  /**
   * gibt die category_id zurück
   *
   * @return category_id
   */
  public function get_category_id()
  {
    return $this->category_id;
  }

  /**
   * gibt alle Daten der Tabelle Category zurück
   */
  public function get_all()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_CATEGORY.' WHERE visible = 1 ORDER BY category_name ASC');
    return $this->db->resultset();
  }

  /**
   * Findet die category_id anhand eines Slugs
   * @param  string $slug [description]
   * @return integer       [description]
   */
  public function get_id_by_slug($slug)
  {
    $this->db->query('SELECT category_id FROM '.DB_TABLE_CATEGORY.' WHERE slug = :slug');
    return $this->db->single([
      ':slug' => $slug,
    ])['category_id'];
  }

  /**
   * gibt die Anzahl der Produkte in einer Category zurück
   */
  public function get_with_product_amount()
  {
    $this->db->query(
      'SELECT pc.*, COUNT(p.category_id) AS product_amount
      FROM '.DB_TABLE_CATEGORY.' pc
      LEFT JOIN '.DB_TABLE_PRODUCT.' p ON p.category_id = pc.category_id
      GROUP BY pc.category_id;'
    );
    return $this->db->resultset();
  }

  /**
   * gibt die category mit der category_id zurück
   */
  public function get_by_id()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_CATEGORY.' WHERE category_id = :category_id');
    return $this->db->single([
      ':category_id' => $this->category_id
    ]);
  }

  /**
   * speichert die Daten für eine category in die Datenbank
   */
  public function insert()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_CATEGORY.' (slug, category_name, created_at) VALUES (:slug, :category_name, :created_at)');
    $this->db->execute([
      ':slug' => StringHelper::to_slug($this->data_to_process['category_name']),
      ':category_name' => $this->data_to_process['category_name'],
      ':created_at' => DATE_NOW
    ]);

    $this->category_id = $this->db->last_insert_id();
  }

  /**
   * ändert die Daten eine bestimmten category
   */
  public function update()
  {
    $this->db->query('UPDATE '.DB_TABLE_CATEGORY.' SET slug = :slug, category_name = :category_name, updated_at = :updated_at WHERE category_id = :category_id');
    $this->db->execute([
      ':slug' => StringHelper::to_slug($this->data_to_process['category_name']),
      ':category_id' => $this->category_id,
      ':category_name' => $this->data_to_process['category_name'],
      ':updated_at' => DATE_NOW
    ]);
  }

  /**
   * löscht eine category
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_CATEGORY.' WHERE category_id = :category_id');
    $this->db->execute([
      ':category_id' => $this->category_id
    ]);
  }

  /**
   * holt sich alle Kategorien außer einer festgelegten
   */
  public function get_all_except()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_CATEGORY.' WHERE category_id <> :category_id');
    return $this->db->resultset([
      ':category_id' => $this->category_id
    ]);
  }
}
