<?

/**
 * Beschreibt die Funktionalität der Basis-Klasse aller Models.
 * Alle Model-Klassen erben von dieser Basis-Klasse.
 */
class BaseModel
{
  /**
   * Instanz der Datenbank-Interaktions-Klasse.
   * @var Database
   */
  protected $db;

  public function __construct()
  {
    $this->db = new Database();

    $this->user_data = Session::read('user_data');

    $this->init();
  }

  /**
   * Wird immer beim Erstellen einer Instanz einer Klasse aufgerufen.
   *
   * @return void
   */
  protected function init()
  {

  }

  /**
   * Gibt dem Controller die Möglichkeit Daten an die Model-Klasse zu übergeben.
   *
   * @param array $data Daten, welche dem Model zur Verfügung stehen sollen.
   */
  public function set_data_to_process($data)
  {
    $this->data_to_process = $data;
  }

  /**
   * Konvertiert die übergegeben Daten um sie direkt an die Datenbank-Instanz als Datenarray zu übergeben.
   *
   * Beispiel:
   *
   *   Vorher:
   *   $this->data_to_process = [
   *     'key' => 'value'
   *   ];
   *
   *   Nachher:
   *   $this->data_to_process = [
   *     ':key' => 'value'
   *   ];
   *
   * @return array Konvertiertes Array
   */
  public function convert_data_to_process()
  {
    foreach ($this->data_to_process as $key => $value) {
      $this->data_to_process[':'.$key] = $value;
      unset($this->data_to_process[$key]);
    }

    return $this->data_to_process;
  }

  /**
   * Gibt die aktuelle User-ID zurück.
   *
   * Wenn die User-ID nicht manuell gesetzt wurde, wird immer die User-ID, welche in der aktuellen Session vorhanden ist, zurückgegeben.
   *
   * @return int User-ID
   */
  public function get_user_id()
  {
    if (isset($this->user_id)) {
      return $this->user_id;
    } else {
      return $this->user_data['user_id'];
    }
  }

}
