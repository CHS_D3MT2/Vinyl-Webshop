<?

/**
 * Enthält Funktionalitäten im Zusammenhang mit der Bezahlung.
 */
class Payment extends BaseModel
{

  /**
   * Setter für Payment-ID
   * @param integer $id
   */
  public function set_payment_id($id)
  {
    $this->payment_id = $id;
  }

  /**
   * Fügt eine Bankverbindung zur 'bank_transfer'-Tabelle hinzu.
   */
  public function add_bank_transfer()
  {
    $this->db->query('INSERT INTO '.DB_TABLE_BT.' (institute, iban, bic) VALUES (:institute, :iban, :bic)');
    $this->db->execute($this->convert_data_to_process());

    $this->bt_id = $this->db->last_insert_id();

    $this->db->query('INSERT INTO '.DB_TABLE_PAYMENT.' (bank_transfer_id, user_id, created_at) VALUES (:bank_transfer_id, :user_id, :created_at)');
    $this->db->execute([
      ':bank_transfer_id' => $this->bt_id,
      ':user_id' => $this->user_data['user_id'],
      ':created_at' => DATE_NOW
    ]);
  }

  /**
   * Fügt eine Kreditkarte zur 'credit_card'-Tabelle hinzu.
   */
  public function add_credit_card()
  {
    $this->data_to_process['expiration_date'] = DateTimeHelper::to_mysql_date($this->data_to_process['expiration_date']);

    $this->db->query('INSERT INTO '.DB_TABLE_CC.'  (provider, card_number, security_code, expiration_date) VALUES (:provider, :card_number, :security_code, :expiration_date)');
    $this->db->execute($this->convert_data_to_process());

    $this->cc_id = $this->db->last_insert_id();

    $this->db->query('INSERT INTO '.DB_TABLE_PAYMENT.' (credit_card_id, user_id, created_at) VALUES (:credit_card_id, :user_id, :created_at)');
    $this->db->execute([
      ':credit_card_id' => $this->cc_id,
      ':user_id' => $this->user_data['user_id'],
      ':created_at' => DATE_NOW
    ]);
  }

  /**
   * Holt sich alle Bankverbindungen eines Nutzers.
   * @return array Bankverbindungen
   */
  public function get_bt_by_user()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_PAYMENT.' AS pay INNER JOIN '.DB_TABLE_BT.' AS bt ON pay.bank_transfer_id = bt.bank_transfer_id WHERE user_id = :user_id');
    return $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);
  }

  /**
   * Holt sich alle Kreditkarten eines Nutzers.
   * @return array Kreditkartendaten
   */
  public function get_cc_by_user()
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_PAYMENT.' AS pay INNER JOIN '.DB_TABLE_CC.' AS cc ON pay.credit_card_id = cc.credit_card_id WHERE user_id = :user_id');
    return $this->db->resultset([
      ':user_id' => $this->user_data['user_id']
    ]);
  }

  /**
   * Holt sich eine Zahlung mithilfe einer ID.
   * @param  integer $id ID einer Zahlung
   * @return array Zahlungsdaten
   */
  public function get_by_id($id)
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_PAYMENT.' AS pay INNER JOIN '.DB_TABLE_BT.' AS bt ON pay.bank_transfer_id = bt.bank_transfer_id WHERE user_id = :user_id AND payment_id = :payment_id');
    $row = $this->db->single([
      ':user_id' => $this->user_data['user_id'],
      ':payment_id' => $id
    ]);

    if ($this->db->row_count() <= 0) {
      $this->db->query('SELECT * FROM '.DB_TABLE_PAYMENT.' AS pay INNER JOIN '.DB_TABLE_CC.' AS cc ON pay.credit_card_id = cc.credit_card_id WHERE user_id = :user_id AND payment_id = :payment_id');
      $row = $this->db->single([
        ':user_id' => $this->user_data['user_id'],
        ':payment_id' => $id
      ]);
      $row['type'] = 'credit_card';
    } else {
      $row['type'] = 'bank_transfer';
    }

    return $row;
  }


  /**
   * Überprüft, ob eine Zahlung zu dem aktuellen User gehört.
   * @param  integer $payment_id Zahlungs-ID, welche überprüft werden soll
   * @return boolean             Zahlung gehört dem Nutzer
   */
  public function owned_by_user($payment_id)
  {
    $this->db->query('SELECT * FROM '.DB_TABLE_PAYMENT.' WHERE user_id = :user_id AND payment_id = :payment_id');
    $row = $this->db->single([
      ':user_id' => $this->user_data['user_id'],
      ':payment_id' => $payment_id
    ]);

    if ($this->db->row_count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Löscht eine Zahlungsart.
   * @return void
   */
  public function delete()
  {
    $this->db->query('DELETE FROM '.DB_TABLE_PAYMENT.' WHERE payment_id = :payment_id AND user_id = :user_id');
    $this->db->execute([
      ':payment_id' => $this->payment_id,
      ':user_id' => $this->get_user_id()
    ]);
  }
}
