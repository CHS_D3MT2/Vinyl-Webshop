<?

/**
 * Bearbietet alle Anfragen, die die Kategorien gerichtet sind.
 */
class CategoryController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    if(isset($this->params[1]))
    {
      $this->action = 'view_single';
    }

    $this->category = new Category();
    $this->product = new Product();
  }

  /**
   * Fehlerseite, falls eine Kategorie nicht vorhanden ist.
   * @return void
   */
  public function view_index()
  {
    $this->view->set_template('404');

    $this->view->set_data([
      'page_title' => 'Fehler - Kategorie nicht gefunden',
    ]);
  }

  /**
   * Zeigt eine Katgeorie und die darin enthaltenen Produkte.
   * @return void
   */
  public function view_single()
  {
    $this->view->set_template('category');

    $category_id = $this->category->get_id_by_slug($this->params[1]);

    $this->category->set_category_id($category_id);
    $this->product->set_category_id($category_id);

    $this->view->set_data([
      'page_title' => 'Genre',
      'category' => $this->category->get_by_id(),
      'products' => $this->product->get_by_category(),
    ]);
  }
}
