<?php

/**
 * Diese Klasse ist die Grundlage für alle Controller. Alle Haupt-Controller erben von dieser Klasse.
 * Sie enthält alle Grundfunktionen und befüllt die View mit dem Grundinhalt (eingeloggt, user-Daten, etc.).
 */
class BaseController
{

  /**
   * Konstruktor.
   * @param array $params URL-Parameter
   * @param array $data   Alle empfangenen Daten
   * @param View $view    View-Objekt
   */
  public function __construct($params = null, $data = null, $view = null)
  {
    // initiert die Session
    Session::init();

    // speichert übergebene Parameter
    $this->params = $params;
    $this->data = $data;

    // Instanziiert wichtige Klassen
    $this->auth = new Auth();
    $this->redirect = new Redirect();

    // speichert die angefragte Aktion
    if (isset($this->data['action'])) {
      $this->action = $this->data['action'];
    }
    else if (isset($this->data['view'])) {
      $this->action = 'view_'.$this->data['view'];
    }
    else {
      $this->action = 'view_index';
    }


    // speichert das aktuelle Datum
    define('DATE_NOW', DateTimeHelper::get_mysql_date());

    // definiert die aktuellen Pfade zum View- und Componenten-Ordner der aktuellen Ansicht
    define('VIEW', VIEW_ROOT.$this->params[0].'/');
    define('COMPONENT', VIEW_ROOT.$this->params[0].'/components/');

    // führt Grundfunktionen in Child-Controllern aus
    $this->init();
  }

  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {

  }

  /**
   * Sammelt Daten bei jeder Anzeige benötigt werden.
   * @return void
   */
  public function get_data()
  {

  }

  /**
   * Speichert das View-Objekt für die aktuelle Instanz, welches dann befüllt wird und die gewünschste Seite darstellt.
   * @param View $view View-Objekt
   */
  public function set_view(View $view)
  {
    $this->view = $view;
  }

  /**
   * View-Objekt mit Grunddaten befüllen, welche immer zur Verfügung stehen sollen.
   * @return void
   */
  public function get_base_data()
  {
    $this->view->set_data([
      'is_logged_in' => $this->auth->is_logged_in(),
      'is_admin' => $this->auth->is_admin(),
      'user' => Session::read('user_data'),
      'cart_stats' => (new Cart)->get_total_stats(),
      'genres' => (new Category)->get_all(),
      'notification' => (new Notification)->get()
    ]);
  }

  /**
   * Anfrage bearbeiten und entscheiden was ausgeführt werden soll/muss.
   * @return void
   */
  public function process_request()
  {
    if (isset($this->action)) {

      $action = $this->action;
      $this->$action();

    }
  }

  /**
   * Sammelt alle Variablen in diesem Object und exportiert diese.
   * @return array Alle Daten dieser Instanz
   */
  public function get_object_data()
  {
    return get_object_vars($this);
  }

  /**
   * Allgemeine View-Optionen setzen.
   */
  public function set_view_options()
  {

  }
}
