<?

/**
 * Diese Klasse bearbeitet alle Anfragen, die an Kategoriedaten im Adminbereich gerichtet sind
 */
class AdminCategoryController extends SubController
{

  /**
   * Initialisiert notwendige Model-Klassen und führt notwendige Funktionen aus bzw. Prüfungen durch
   * @return void
   */
  public function init()
  {
    $this->category = new Category();
    $this->product = new Product();
  }

  /**
   * Zeigt die Seite für die Erstellung einer neuen Kategorie
   * @return void
   */
  public function view_new()
  {
    $this->view->set_data([
      'title' => 'Neues Genre anlegen',
      'form_params' => 'action=create'
    ]);

    $this->view->set_include('category_form');
  }

  /**
   * Zeigt die Seite für die Bearbeitung einer vorhandenen Kategorie
   * @return void
   */
  public function view_edit()
  {
    $this->category->set_category_id($this->data['category_id']);

    $this->view->set_data([
      'category' => $this->category->get_by_id(),
      'title' => 'Genre bearbeiten',
      'form_params' => 'category_id='.$this->data['category_id'].'&action=update'
    ]);

    $this->view->set_include('category_form');
  }

  /**
   * Zeigt die Seite welche alle vorhandenen Kategorien darstellt
   * @return void
   */
  public function view_all()
  {
    $this->view->set_data([
      'categories' => $this->category->get_with_product_amount(),
      'title' => 'Genre-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('category_list');
  }

  /**
   * Erstellt eine neue Kategorie
   * @return void
   */
  public function create()
  {
    $this->category->set_data_to_process($this->data);
    $this->category->insert();
    $this->redirect->to('admin/category?view=new')->go();
  }

  /**
   * Aktualisiert eine vorhandene Kategorie
   * @return void
   */
  public function update()
  {
    $this->category->set_data_to_process($this->data);
    $this->category->set_category_id($this->data['category_id']);
    $this->category->update();
    $this->redirect->to('admin/category?view=all')->go();
  }

  /**
   * Löscht eine vorhandene Kategorie
   * @return void
   */
  public function delete()
  {
    $this->category->set_category_id($this->data['category_id']);
    $this->category->delete();
    $this->redirect->to('admin/category?view=all')->go();
  }
}
