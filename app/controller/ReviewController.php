<?

/**
 * Bearbeitet alle Anfragen, die mit dem Bewertungssystem zutun haben.
 */
class ReviewController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    $this->review = new Review;
  }

  /**
   * Speichert eine Bewertung in der Datenbank.
   * @return void
   */
  public function create()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    $this->review->set_data_to_process($this->data['review']);

    if ($this->review->is_valid()) {

      $this->review->create();

      (new Product)->set_product_id($this->data['review']['product_id'])->update_rating($this->data['review']['rating']);

    }

    $this->redirect->to_origin()->go();
  }

  /**
   * Löscht eine Bewertung aus der Datenbank.
   * @return void
   */
  public function delete()
  {
    $this->review->set_review_id($this->data['review_id']);

    if ($this->review->is_owned_by_user()) {
      $this->review->delete();
    }

    die();
  }
}
