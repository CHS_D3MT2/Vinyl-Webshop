<?

/**
 * Bearbeitet alle Anfragen, die an den Warenkorb gerichtet sind.
 */
class CartController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    $this->cart = new Cart();
  }

  /**
   * Fügt ein Produkt zum Warenkorb hinzu.
   * @return void
   */
  public function add_item()
  {
    $this->cart->set_product_id($this->data['product_id']);
    $this->cart->set_price($this->data['price']);

    if (isset($this->data['amount'])) {
      $this->cart->set_amount($this->data['amount']);
    }

    if ($this->cart->exists_in_session()) {
      $this->cart->add_to_session();
    } else {
      $this->cart->create_in_session();
    }
    die();
  }

  /**
   * Zeigt die Standard-Ansicht des Warenkorbs.
   * @return void
   */
  public function view_index()
  {
    $this->view->set_template('cart');

    $this->view->set_data([
      'page_title' => 'Warenkorb',
      'cart_items' => $this->cart->get_all(),
      'cart' => $this->cart->get_session_cart()
    ]);
  }

  /**
   * Aktualisiert die Menge eines Produktes im Warenkorb.
   * @return void
   */
  public function update_amount()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    if (!(new Product)->set_product_id($this->data['product_id'])->enough_products($this->data['cart']['item_amount'])) {
      (new Notification)->send_back([
        'code' => 'not_enough_products',
        'type' => 'error'
      ]);
    }

    $this->cart->set_product_id($this->data['product_id']);
    $this->cart->set_amount($this->data['cart']['item_amount']);
    $this->cart->update_amount();
    die();
  }

  /**
   * Löscht ein Produkt aus dem Warenkorb.
   * @return void
   */
  public function delete_item()
  {
    $this->cart->set_product_id($this->data['product_id']);
    $this->cart->delete();
    die();
  }
}
