<?

/**
 * Diese Klasse bearbeitet alle Anfragen, die an Artistdaten im Adminbereich gerichtet sind.
 */
class AdminArtistController extends SubController
{
  /**
   * Initialisiert notwendige Model-Klassen.
   * @return void
   */
  public function init()
  {
    $this->artist = new Artist();
  }

  /**
   * Zeigt die Seite für die Erstellung eines neuen Artists.
   * @return void
   */
  public function view_new()
  {
    $this->view->set_data([
      'title' => 'Neuen Künstler anlegen',
      'form_params' => 'action=create'
    ]);

    $this->view->set_include('artist_form');
  }

  /**
   * Zeigt die Seite für die Bearbeitung eines vorhandenen Artsts.
   * @return void
   */
  public function view_edit()
  {
    $this->artist->set_artist_id($this->data['artist_id']);

    $this->view->set_data([
      'title' => 'Künstler bearbeiten',
      'artist' => $this->artist->get_by_id(),
      'form_params' => 'artist_id='.$this->data['artist_id'].'&action=update',
    ]);

    $this->view->set_include('artist_form');
  }

  /**
   * Zeigt die Seite welche alle vorhandenen Artists darstellt.
   * @return void
   */
  public function view_all()
  {
    $this->view->set_data([
      'artists' => $this->artist->get_all(),
      'title' => 'Artist-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('artist_list');
  }

  /**
   * Erstellt einen neuen Artist.
   * @return void
   */
  public function create()
  {
    $this->artist->set_data_to_process($this->data['artist']);
    $this->artist->insert();

    if (ArrayHelper::contains_files($this->data['artist_images'])) {

      $upload = new Upload();
      $upload->set_file_array($this->data['artist_images']);
      $upload->set_template('artist_image');

      if (!$upload->process_upload()) {
        Debug::print($this);
      }

      $this->artist->delete_images_by_artist();
      $this->artist->set_image_data($upload->get_image_data());
      $this->artist->save_images();
    }

    $this->redirect->to_origin()->go();
  }

  /**
   * Aktualisiert einen vorhandenen Artist.
   * @return void
   */
  public function update()
  {
    $this->artist->set_data_to_process($this->data['artist']);
    $this->artist->set_artist_id($this->data['artist_id']);

    if (ArrayHelper::contains_files($this->data['artist_images'])) {

      $upload = new Upload();
      $upload->set_file_array($this->data['artist_images']);
      $upload->set_template('artist_image');

      if (!$upload->process_upload()) {
        Debug::print($this);
      }

      $this->artist->delete_images_by_artist();
      $this->artist->set_image_data($upload->get_image_data());
      $this->artist->save_images();
    }

    $this->artist->update();

    $this->redirect->to('admin/artist?view=all')->go();
  }

  /**
   * Löscht einen vorhandenen Artist.
   * @return void
   */
  public function delete()
  {
    $this->artist->set_artist_id($this->data['artist_id']);
    $this->artist->delete();
    $this->redirect->to('admin/artist?view=all')->go();
  }

}
