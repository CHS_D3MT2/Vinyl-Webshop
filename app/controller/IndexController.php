<?

/**
 * Bearbeitet Anfragen an die Startseite des Shops.
 */
class IndexController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    $this->product = new Product();
  }

  /**
   * Zeigt die Startseite des Shops.
   * @return void
   */
  public function view_index()
  {
    $this->view->set_data([
      'products' => $this->product->get_all(),
      'most_bought_products' => $this->product->get_most_bought(),
      'products_by_category' => $this->product->get_all_by_categories()
    ]);
  }

}
