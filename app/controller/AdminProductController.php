<?

/*
Diese Klasse bearbeitet alle Anfragen, die keine Parameter enthalten
*/
class AdminProductController extends SubController
{
  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    $this->category = new Category();
    $this->product = new Product();
    $this->track = new Track();
    $this->artist = new Artist();
  }

  /**
   * Zeigt die Ansicht um ein neues Produkt zu erstellen.
   * @return void
   */
  public function view_new()
  {
    $this->view->set_data([
      'categories' => $this->category->get_all(),
      'title' => 'Neues Produkt anlegen',
      'form_params' => 'action=create',
      'artists' => $this->artist->get_all()
    ]);

    $this->view->set_include('product_form');
  }

  /**
   * Zeigt die Bearbeitungsansicht eines Produkts.
   * @return void
   */
  public function view_edit()
  {
    $this->product->set_product_id($this->data['product_id']);

    $this->view->set_data([
      'product' => $this->product->get_by_id(),
      'categories' => $this->category->get_all(),
      'title' => 'Produkt bearbeiten',
      'form_params' => 'product_id='.$this->data['product_id'].'&action=update',
      'artists' => $this->artist->get_all()
    ]);

    $this->view->set_include('product_form');
  }

  /**
   * Listet alle vorhandenen Produkte auf.
   * @return void
   */
  public function view_all()
  {
    $this->view->set_data([
      'products' => $this->product->get_all(),
      'title' => 'Produkt-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('product_list');
  }

  /**
   * Erstellt ein neues Produkt.
   * @return [type] [description]
   */
  public function create()
  {
    $this->product->set_data_to_process($this->data['product']);

    $this->artist->set_artist_id($this->data['product']['artist']);
    $artist_name = $this->artist->get_by_id()['name'];

    $this->product->set_slug(StringHelper::to_slug($artist_name."-".$this->data['product']['name']));

    $this->product->insert();

    if (ArrayHelper::contains_files($this->data['product_images'])) {

      $upload = new Upload();
      $upload->set_file_array($this->data['product_images']);
      $upload->set_template('product_image');

      if (!$upload->process_upload()) {
        Debug::print($this);
      }

      $this->product->delete_images_by_product();
      $this->product->set_image_data($upload->get_image_data());
      $this->product->save_images();
    }

    $this->track->set_product_id($this->product->get_product_id());
    $this->track->set_data_to_process($this->data['tracks']);
    $this->track->insert();

    $this->redirect->to('admin/product?view=new')->go();
  }

  /**
   * Aktualisiert ein Produkt.
   * @return void
   */
  public function update()
  {
    $this->product->set_data_to_process($this->data['product']);
    $this->product->set_product_id($this->data['product_id']);

    $this->artist->set_artist_id($this->data['product']['artist']);
    $artist_name = $this->artist->get_by_id()['name'];

    $this->product->set_slug(StringHelper::to_slug($artist_name."-".$this->data['product']['name']));

    $this->product->update();

    if (ArrayHelper::contains_files($this->data['product_images'])) {

      $upload = new Upload();
      $upload->set_file_array($this->data['product_images']);
      $upload->set_template('product_image');

      if (!$upload->process_upload()) {
        Debug::print($this);
      }

      $this->product->delete_images_by_product();
      $this->product->set_image_data($upload->get_image_data());
      $this->product->save_images();
    }

    $this->track->set_product_id($this->product->get_product_id());
    $this->track->delete_by_product();
    $this->track->set_data_to_process($this->data['tracks']);
    $this->track->insert();

    $this->redirect->to('admin/product?view=all')->go();
  }

  /**
   * Löscht ein Produkt.
   * @return [type] [description]
   */
  public function delete()
  {
    $this->product->set_product_id($this->data['product_id']);
    $this->product->delete();
    $this->redirect->to('admin/product?view=all')->go();
  }

  /**
   * Ansicht für das Löschen einer Kategorie wenn noch Produkte enthalten sind.
   * @return void
   */
  public function get_amount_by_category_id()
  {
    $this->product->set_category_id($this->data['category_id']);
    $amount = $this->product->get_amount_by_category_id();

    $this->category->set_category_id($this->data['category_id']);

    if ($amount > 0) {

      $this->view->set_data([
        'category_id' => $this->data['category_id'],
        'redirect' => $this->data['redirect'],
        'categories' => $this->category->get_all_except(),
        'amount' => $amount
      ]);

      $this->view->set_render_mode('partial');
      $this->view->set_template('components/category_delete_modal');

    } else {
      die();
    }
  }

  /**
   * Ändert die Kategorie eines Produkts.
   * @return void
   */
  public function change_category_id()
  {
    $this->product->set_category_id($this->data['category_id']);

    if ($this->data['category_option'] == 'new') {

      $this->category->set_data_to_process($this->data);
      $this->category->insert();
      $this->product->change_category_id_to($this->category->get_category_id());

    } else {

      $this->product->change_category_id_to($this->data['new_category_id']);

    }

    $this->redirect->to(urldecode($this->data['redirect']))->go();
  }
}
