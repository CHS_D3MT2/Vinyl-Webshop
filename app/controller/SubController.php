<?

/**
 * Stellt die Basis für alle Unter-Controller des Admin-Controllers dar.
 */
class SubController
{
  /**
   * Konstruktor des Unter-Controllers.
   * @param BaseController] $base_controller Basis-Controller, dessen Daten dann extrahiert werden können.
   */
  public function __construct($base_controller)
  {
    $this->base_controller = $base_controller;
    $this->base_data = $this->base_controller->get_object_data();

    foreach ($this->base_data as $key => $var) {

      $this->$key = $var;

    }

    $this->init();

    $this->process_request();
  }

  /**
   * Anfrage bearbeiten.
   * @return void
   */
  public function process_request()
  {
    if (isset($this->action)) {

      $action = $this->action;
      $this->$action();

    }
  }
}
