<?

/**
 * Bearbeitet alle Anfragen die an die Nutzerdaten im Adminbereich gerichtet sind.
 */
class AdminUserController extends SubController
{
  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    $this->user = new User();
  }

  public function view_all()
  {
    $this->view->set_data([
      'users' => $this->user->get_all(),
      'title' => 'User-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('user_list');
  }

  public function delete()
  {
    $this->user->set_user_id($this->data['user_id']);
    $this->user->delete();
    $this->redirect->to('admin/user?view=all')->go();
  }

}
