<?

/**
 * Nimmt alle Anfragen entgegen die an den Adminbereich gerichtet sind.
 */
class AdminController extends BaseController
{

  /**
   * Initialisiert notwendige Model-Klassen und führt notwendige Funktionen aus bzw. Prüfungen durch
   * @return void
   */
  public function init()
  {
    if (!$this->auth->is_admin()) {

      $this->redirect->to_index()->go();

    }
  }

  /**
   * Setter für View-Objekt
   * @return void
   */
  public function set_view(View $view)
  {
    $this->view = $view;

    if (isset($this->data['view'])) {

      $this->view->set_include($this->data['view']);
      $this->action = 'view_'.$this->data['view'];
    }
  }

  public function set_view_options()
  {
    $this->view->disable_footer();
  }

  /**
   * Befüllt das View-Objekt mit den Grunddaten
   * @return void
   */
  public function get_data()
  {
    $this->view->set_data([
      'page_title' => 'Admin'
    ]);

    $this->view->set_container('container container-full');
    $this->view->set_nav_container('container-fluid');
  }

  /**
   * Zeigt die Standard-Seite des AdminControllers. Das Dashboard.
   * @return void
   */
  public function view_index()
  {
    $this->redirect->to('admin/user?view=all')->go();
  }

  /**
   * Bearbeitet die aktuelle Anfrage
   * @return void
   */
  public function process_request()
  {
    $this->view->set_directory('admin');
    $this->view->set_template('admin');

    if (isset($this->params[1])) {

      (new Router)->build_sub_controller($this->params[1], $this, 'admin');

    } else {
      $action = $this->action;
      $this->$action();
    }
  }

}
