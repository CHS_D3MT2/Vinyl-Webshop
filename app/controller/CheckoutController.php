<?

/**
 * Bearbeitet Anfragen die an das Bestellsystem gerichtet sind.
 */
class CheckoutController extends BaseController
{
  /**
   * Beinhaltet die Schritte des Bestellvorgangs und deren Position.
   * @var array
   */
  protected $position = [
    'login', 'address', 'payment', 'shipping', 'confirm', 'done'
  ];

  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    if (!$this->auth->is_logged_in()) {
      $this->redirect->to_origin()->go();
    }

    if (!isset($this->data['action'])) {
      $this->action = 'view_'.$this->params[1];
    }

    $this->order = new Order;

    if (is_null(Session::read('order.step_completed'))) {
      $this->order->set_complete(1);
    }
  }

  /**
   * Zeigt Adress-Auswahl.
   * @return void
   */
  public function view_address()
  {
    if (!$this->order->is_previous_complete($this->get_step_num())) {
      $this->redirect->to_origin()->go();
    }

    $this->view->set_template('address');

    $this->view->set_data([
      'page_title' => 'Adresse',
      'addresses' => (new Address)->get_by_user()
    ]);
  }

  /**
   * Zeigt Zahlungsart-Auswahl.
   * @return void
   */
  public function view_payment()
  {
    if (!$this->order->is_previous_complete($this->get_step_num())) {
      $this->redirect->to_origin()->go();
    }

    $payment = new Payment;

    $this->view->set_template('payment');

    $this->view->set_data([
      'page_title' => 'Zahlungsart',
      'addresses' => (new Address)->get_by_user(),
      'bank_transfers' => $payment->get_bt_by_user(),
      'credit_cards' => $payment->get_cc_by_user()
    ]);
  }

  /**
   * Zeigt Lieferart-Auswahl.
   * @return void
   */
  public function view_shipping()
  {
    if (!$this->order->is_previous_complete($this->get_step_num())) {
      $this->redirect->to_origin()->go();
    }

    $this->view->set_template('shipping');

    $this->view->set_data([
      'page_title' => 'Lieferung',
      'shipping_methods' => (new Shipping)->get_all_visible()
    ]);
  }

  /**
   * Zeigt Bestätigungsseite.
   * @return void
   */
  public function view_confirm()
  {
    if (!$this->order->is_previous_complete($this->get_step_num())) {
      $this->redirect->to_origin()->go();
    }

    $this->order->set_cart((new Cart)->get_session_cart());

    $address = new Address;
    $cart = new Cart;

    $this->view->set_template('confirm');

    $this->view->set_data([
      'page_title' => 'Bestätigen',
      'payment_address' => $address->get_by_id($this->order->get_payment_address_id()),
      'shipping_address' => $address->get_by_id($this->order->get_shipping_address_id()),
      'payment' => (new Payment)->get_by_id($this->order->get_payment_id()),
      'shipping' => (new Shipping)->get_by_code($this->order->get_shipping_code()),
      'cart_items' => $cart->get_all(),
      'cart' => $cart->get_session_cart(),
      'cart_stats' => $cart->get_total_stats()
    ]);
  }

  /**
   * Zeigt die Bestätigungsseite nachdem die Bestellung erfolgreich abgeschlossen wurde.
   * @return void
   */
  public function view_done()
  {
    if (!$this->order->is_previous_complete($this->get_step_num())) {
      $this->redirect->to_index()->go();
    }

    $this->view->set_template('done');

    $this->view->set_data([
      'page_title' => 'Finito',
      'order_id' => $this->order->get_order_id()
    ]);

    $this->order->clear();
  }

  /**
   * Legt die Adressen der Bestellung fest.
   * @return void
   */
  public function set_address()
  {
    if (!isset($this->data['address']['payment_address']) || !isset($this->data['address']['shipping_address'])) {
      $this->redirect->to_origin()-go();
    }

    $address = new Address;

    if (!$address->owned_by_user($this->data['address']['payment_address']) || !$address->owned_by_user($this->data['address']['shipping_address'])) {
      $this->redirect->to_origin()-go();
    }

    $this->order->set_payment_address_id($this->data['address']['payment_address']);
    $this->order->set_shipping_address_id($this->data['address']['shipping_address']);

    $this->order->set_complete($this->get_step_num('address'));

    $this->redirect->to('checkout/payment')->go();
  }

  /**
   * Legt die Zahlungsart der Bestellung fest.
   * @return void
   */
  public function set_payment()
  {
    if (!(new Payment)->owned_by_user($this->data['payment']['payment_id'])) {
      $this->redirect->to_origin()-go();
    }

    $this->order->set_payment_id($this->data['payment']['payment_id']);

    $this->order->set_complete($this->get_step_num('payment'));

    $this->redirect->to('checkout/shipping')->go();
  }

  /**
   * Legt die Liefermethode der Bestellung fest.
   * @return void
   */
  public function set_shipping()
  {
    $this->order->set_shipping_code($this->data['shipping']['shipping_code']);

    $this->order->set_complete($this->get_step_num('shipping'));

    $this->redirect->to('checkout/confirm')->go();
  }

  /**
   * Schließt eine Bestellung ab.
   * @return void
   */
  public function buy()
  {
    $this->order->buy();

    $this->order->set_complete($this->get_step_num('confirm'));

    (new Cart)->clear();

    $this->redirect->to('checkout/done')->go();
  }

  /**
   * Legt allgemeine Einstellung für die View fest.
   */
  public function set_view_options()
  {
    $this->view->set_content_container('container-small');
  }

  /**
   * Sammelt Daten bei jeder Anzeige benötigt werden.
   * @return void
   */
  public function get_data()
  {
    $this->view->set_data([
      'position' => $this->get_step_num()
    ]);
  }

  /**
   * Gibt die Position eines Schrittes im Checkout-Vorgang zurück.
   * @param  string $var Name des Schittes
   * @return integer     Position des Schrittes
   */
  public function get_step_num($var = null)
  {
    return array_search((is_null($var) ? (isset($this->params[1]) ? $this->params[1] : null) : $var), $this->position) + 1;
  }


}

?>
