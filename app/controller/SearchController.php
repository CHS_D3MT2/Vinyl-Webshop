<?

/**
 * Bearbeitet Anfragen der Suchfunktion.
 */
class SearchController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    $this->search = new Search;
    $this->action = 'search';

  }

  /**
   * Für eine Suche in allen relevanten Tabellen durch.
   * @return void
   */
  public function search()
  {
    $this->search->set_search_text(ArrayHelper::clean_input($this->data['search_text']));

    $this->view->set_data([
      'products' => $this->search->products(),
      'categories' => $this->search->categories(),
      'artists' => $this->search->artists(),
      'tracks' => $this->search->tracks()
    ]);

    $this->view->set_render_mode('partial');
    $this->view->set_template('result');
  }
}
