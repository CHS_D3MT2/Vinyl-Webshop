<?

/**
 * Behandelt Anfragen, welche an die Wunschliste gerichtet sind
 */
class WishlistController extends BaseController
{

    /**
     * Initialisiert notwendige Model-Klassen und führt notwendige Funktionen aus bzw. Prüfungen durch
     * @return void
     */
    public function init()
    {
      $this->wishlist = new Wishlist();
    }

    /**
     * Fügt ein Produkt zur Wunschliste hinzu
     */
    public function add_to_wishlist()
    {
      $this->wishlist->set_product_id($this->data['product_id']);
      if(!$this->wishlist->exists())
      {
        $this->wishlist->insert();
      }
      die();
    }

    /**
     * Löscht ein Produkt aus der Wunschliste
     * @return void
     */
    public function delete_product()
    {
      $this->wishlist->set_product_id($this->data['product_id']);
      $this->wishlist->delete();
      die();
    }

    /**
     * Zeigt Ansicht der Wunschliste
     * @return void
     */
    public function view_index()
    {
      $this->view->set_template('wishlist');

      $this->view->set_data([
        'page_title' => 'Wunschliste',
        'wishlist_items' => $this->wishlist->get_by_user(),
      ]);
    }
}
