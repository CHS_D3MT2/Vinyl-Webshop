<?

/**
 * Bearbeitet anfragen, welche erweiterte Debug-Funktionalitäten betreffen.
 */
class DebugController extends BaseController
{
  /**
   * Leert den kompletten Warenkorb des akutellen Nutzers.
   * @return void
   */
  public function clear_session_cart()
  {
    $_SESSION['cart'] = [];
    $this->redirect->to_index()->go();
  }
}
