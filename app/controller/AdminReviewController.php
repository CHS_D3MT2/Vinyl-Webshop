<?

/**
 * Diese Klasse bearbeitet alle Anfragen, die an Bewertungsdaten im Adminbereich gerichtet sind
 */
class AdminReviewController extends SubController
{
  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    $this->review = new Review;
  }

  /**
   * Zeigt die Seite welche alle vorhandenen Bestellungen darstellt
   * @return void
   */
  public function view_all()
  {
    $this->view->set_data([
      'reviews' => $this->review->get_all(),
      'title' => 'Bewertungs-Übersicht'
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('review_list');
  }

  /**
   * Zeigt die Seite welche eine Bestellung im Detail darstellt.
   * @return void
   */
  public function view_detail()
  {
    $review = $this->review->set_review_id($this->data['review_id'])->get_by_id();

    $payment_address = (new Address)->get_by_id($review['payment_address_id']);
    $shipping_address = (new Address)->get_by_id($review['payment_address_id']);
    $payment = (new Payment)->get_by_id($review['payment_id']);
    $shipping = (new Shipping)->get_by_code($review['shipping_code']);

    $this->view->set_data([
      'review' => $review,
      'payment_address' => $payment_address,
      'shipping_address' => $shipping_address,
      'payment' => $payment,
      'shipping' => $shipping,
      'title' => 'Detailansicht der Bestellung #'.$this->data['review_id']
    ]);

    $this->view->disable_sidebar();
    $this->view->set_include('review_detail');
  }

  /**
   * Löscht eine vorhandene Bestellung
   * @return void
   */
  public function delete()
  {
    $this->review->set_review_id($this->data['review_id']);
    $this->review->delete();
    $this->redirect->to('admin/review?view=all')->go();
  }

}
