<?

/**
 * Bearbeitet alle Anfragen die an die Künstlerdaten gerichtet sind.
 */
class ArtistController extends BaseController
{
  /**
   * Initialisiert notwendige Model-Klassen
   * @return void
   */
  public function init()
  {
    if(isset($this->params[1]))
    {
      $this->action = 'view_single';
    }

    $this->artist = new Artist();
    $this->product = new Product();
  }

  public function view_index()
  {
    $this->view->set_template('404');

    $this->view->set_data([
      'page_title' => 'Fehler - Künstler nicht gefunden',
    ]);
  }

  public function view_single()
  {
    $this->view->set_template('single');

    if (is_numeric($this->params[1])) {
      $this->artist->set_artist_id($this->params[1]);
      $this->product->set_artist_id($this->params[1]);
    } else {
      $this->artist->set_artist_id($this->artist->get_id_by_slug($this->params[1]));
      $this->product->set_artist_id($this->artist->get_id_by_slug($this->params[1]));
    }

    $this->view->set_data([
      'page_title' => 'Künstler',
      'artist' => $this->artist->get_by_id(),
      'products' => $this->product->get_by_artist()
    ]);
  }

}
