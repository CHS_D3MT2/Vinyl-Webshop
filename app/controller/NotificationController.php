<?

/**
 * Nimmt Anfragen entgegen, die an das Benachrichtigungssystem gerichtet sind.
 */
class NotificationController
{
  /**
   * Konstruktor
   * @param array $params URL-Parameter
   */
  public function __construct($params)
  {
    (new Notification)->set_code($params[1])->set_type($params[2])->render();
    die();
  }

}
