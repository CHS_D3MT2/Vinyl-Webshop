<?

/**
 * Bearbeitet alle Anfragen, die an Produkte gerichtet sind.
 */
class ProductController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    if(isset($this->params[1]))
    {
      $this->action = 'view_single';
    }

    $this->category = new Category();
    $this->product = new Product();
  }

  /**
   * Fehlerseite, falls ein Produkt nicht vorhanden ist.
   * @return [type] [description]
   */
  public function view_index()
  {
    $this->view->set_template('404');

    $this->view->set_data([
      'page_title' => 'Fehler - Produkt nicht gefunden',
    ]);
  }

  /**
   * Zeigt die Detailseite eines Produkts.
   * @return void
   */
  public function view_single()
  {
    $this->view->set_template('single');

    $product_id = $this->product->get_id_by_slug($this->params[1]);

    $this->product->set_product_id($product_id)->increase_view_amount();

    $this->view->set_data([
      'page_title' => 'Produkt',
      'product' => $this->product->get_by_id(),
      'more_products_of_artist' => $this->product->get_by_artist_except_current(),
      'reviews' => (new Review)->get_by_product_id($product_id)
    ]);
  }

}
