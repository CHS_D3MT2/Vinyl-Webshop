<?

/**
 * Behandelt Anfragen, welche an den Nutzeraccount gerichtet sind
 */
class AccountController extends BaseController
{
  /**
   * Initialisierung
   * @return void
   */
  public function init()
  {
    $this->address = new Address;
    $this->payment = new Payment;

    if (!isset($this->data['action']) && isset($this->params[1])) {
      $this->action = 'view_'.$this->params[1];
    }
  }

  /**
   * Listet alle Bestellungen eines Nutzers auf.
   * @return void
   */
  public function view_orders()
  {
    $this->view->set_template('orders');

    $this->view->set_data([
      'page_title' => 'Bestellungen',
      'orders' => (new Order)->get_by_user_detailed()
    ]);
  }

  /**
   * Zeigt die Standard-Seite des AccountControllers.
   * @return void
   */
  public function view_index()
  {
    $this->view->set_template('account');

    $this->view->set_data([
      'page_title' => 'Bestellungen',
      'user_info' => (new User)->get_by_id(),
      'auth_info' => (new Auth)->get_info(),
      'addresses' => $this->address->get_by_user(),
      'bank_transfers' => $this->payment->get_bt_by_user(),
      'credit_cards' => $this->payment->get_cc_by_user()
    ]);
  }


  /**
   * Fügt eine Adresse hinzu
   */
  public function add_address()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    if (ArrayHelper::has_empty_values($this->data['address'], $ignore = ['company', 'phone'])) {
      $this->redirect->to_origin()->go();
    }

    if (!$this->address->can_add_address()) {
      $this->redirect->to_origin()->go();
    }

    if (!$this->address->has_addresses()) {
      $this->data['address']['is_main_payment'] = 1;
      $this->data['address']['is_main_shipping'] = 1;
    }

    $this->address = new Address;
    $this->address->set_data_to_process(ArrayHelper::clean_input($this->data['address']));
    $this->address->reset_others();
    $this->address->insert();

    $this->redirect->to_origin()->go();
  }

  /**
   * Löscht eine Adresse
   */
  public function delete_address()
  {
    if ((new Order)->is_address_used($this->data['address_id']))  {
      (new Notification)->send_back([
        'code' => 'address_used_in_order',
        'type' => 'error'
      ]);
    }

    $this->address->set_address_id($this->data['address_id']);
    $this->address->delete();
    die();
  }

  /**
   * Fügt eine Bankverbindung hinzu.
   */
  public function add_bank_transfer()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    $this->payment->set_data_to_process($this->data['bank_transfer']);
    $this->payment->add_bank_transfer();

    $this->redirect->to_origin()->go();
  }

  /**
   * Fügt eine Kreditkarte hinzu.
   */
  public function add_credit_card()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    $this->payment->set_data_to_process($this->data['credit_card']);
    $this->payment->add_credit_card();

    $this->redirect->to_origin()->go();
  }

  /**
   * Löscht eine Zahlungsart
   */
  public function delete_payment()
  {
    if ((new Order)->is_payment_used($this->data['payment_id']))  {
      (new Notification)->send_back([
        'code' => 'payment_used_in_order',
        'type' => 'error'
      ]);
    }

    $this->payment->set_payment_id($this->data['payment_id']);
    $this->payment->delete();
    die();
  }

  /**
   * Aktualisiert die persönlichen Daten eines Nutzers.
   * @return void
   */
  public function change_user_info()
  {
    $this->data = ArrayHelper::clean_input($this->data);

    $user = new User;
    $user->set_data_to_process($this->data['user_info']);
    $user->update();

    Session::write('user_data', $user->get_by_id());

    $this->redirect->to('/account')->go();
  }
}
