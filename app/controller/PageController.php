<?

/**
 * Bearbeitet Anfragen an statische Seiten.
 */
class PageController extends BaseController
{
  /**
   * Wird beim Instantiieren des Controllers aufgerufen.
   * @return void
   */
  public function init()
  {
    $this->action = 'view';
  }

  /**
   * Lädt ein Template anhand eines Teils der URL.
   * @param  string $slug  Name des Templates
   * @param  string $title Titel der Seite
   * @return void
   */
  public function get_page_from_slug($slug, $title)
  {
    $this->view->set_template($slug);

    $this->view->set_data([
      'page_title' => $title,
    ]);
  }

  /**
   * Behandelt sie verschiedenen Anfragen an die statischen Seiten.
   * @return void
   */
  public function view()
  {
    $page = $this->params[1];

    switch($page) {
      case 'imprint':
        $this->get_page_from_slug('imprint', 'Impressum');
        break;
      case 'privacy-policy':
        $this->get_page_from_slug('privacy-policy', 'Datenschutzerklärung');
        break;
      case 'terms-of-service':
        $this->get_page_from_slug('terms-of-service', 'Allgemeine Geschäftsbedingungen');
        break;
      case 'contact':
        $this->get_page_from_slug('contact', 'Kontakt');
        break;
    }
  }
}
