<?php

define('LOCALHOST', false);

if (LOCALHOST) {
  define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');
  // error_reporting(E_ALL & E_STRICT);
  // ini_set('display_errors', '1');
  // ini_set('log_errors', '0');
  // ini_set('error_log', './');
} else {
  define('ROOT', $_SERVER['DOCUMENT_ROOT']);
}

// Config-Klasse laden
require_once(ROOT.'app/lib/Config.php');

// Config initialisieren
Config::init();

// Autoloader registrieren
Autoloader::register();

$router = new Router();

$router->convert_url($_SERVER['REQUEST_URI']);
$router->collect_data([$_POST, $_GET, $_FILES]);

$router->map_controller();
$controller = $router->build_controller();

$view = new View($router->get_controller_name());

$controller->set_view($view);
$controller->set_view_options();
$controller->get_base_data();
$controller->get_data();
$controller->process_request();

$view->render();
